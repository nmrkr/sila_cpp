/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   test_fully_qualified_identifiers.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.01.2021
/// \brief  Unit tests for the Fully Qualified Identifiers implementations
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FullyQualifiedCommandID.h>
#include <sila_cpp/common/FullyQualifiedFeatureID.h>
#include <sila_cpp/common/FullyQualifiedPropertyID.h>

#include <catch2/catch.hpp>

using namespace std;
using namespace SiLA2;

//============================================================================
SCENARIO("Construct a Fully Qualified Feature Identifier", "[fqid][feature]")
{
    GIVEN("An Originator, Category, Feature Identifier and Version")
    {
        const QString Originator{"org.silastandard"};
        const QString Category{"test.something"};
        const QString Identifier{"SomeFeature"};
        const QString Version{"v1"};

        WHEN("a Fully Qualified Feature ID is constructed from these")
        {
            const auto FullyQualifiedFeatureID = CFullyQualifiedFeatureID{
                Originator, Category, Identifier, Version};

            THEN("the getters to return the correct values")
            {
                REQUIRE(FullyQualifiedFeatureID.originator() == Originator);
                REQUIRE(FullyQualifiedFeatureID.category() == Category);
                REQUIRE(FullyQualifiedFeatureID.identifier() == Identifier);
                REQUIRE(FullyQualifiedFeatureID.version() == Version);
            }
            AND_WHEN("the Fully Qualified Feature ID is converted into a string")
            {
                const auto String = FullyQualifiedFeatureID.toStdString();
                THEN("it conforms to the SiLA 2 standard definition of a Fully "
                     "Qualified Feature Identifier")
                {
                    const auto ConformingString =
                        QString{Originator + "/" + Category + "/" + Identifier
                                + "/" + Version}
                            .toStdString();
                    REQUIRE(String == ConformingString);

                    AND_WHEN("a Fully Qualified Feature Identifier is "
                             "constructed from a SiLA 2 conforming string")
                    {
                        const auto IdentifierFromString =
                            CFullyQualifiedFeatureID::fromStdString(
                                ConformingString);
                        THEN("the two Fully Qualified Feature Identifiers are "
                             "the same")
                        {
                            CAPTURE(IdentifierFromString);
                            REQUIRE(IdentifierFromString.isValid());
                            REQUIRE(FullyQualifiedFeatureID.originator()
                                    == IdentifierFromString.originator());
                            REQUIRE(FullyQualifiedFeatureID.category()
                                    == IdentifierFromString.category());
                            REQUIRE(FullyQualifiedFeatureID.identifier()
                                    == IdentifierFromString.identifier());
                            REQUIRE(FullyQualifiedFeatureID.version()
                                    == IdentifierFromString.version());
                        }
                    }
                }
            }
            AND_WHEN("a Fully Qualified Feature Identifier is constructed "
                     "from an invalid string")
            {
                const auto InvalidString = GENERATE(
                    ""s, "///"s, "this/is/not/a/fully/qualified/identifier"s);
                const auto InvalidIdentifier =
                    CFullyQualifiedFeatureID::fromStdString(InvalidString);
                THEN("the Identifier is invalid")
                {
                    REQUIRE_FALSE(InvalidIdentifier.isValid());
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Construct a Fully Qualified Command Identifier", "[fqid][command]")
{
    GIVEN("An Originator, Category, Feature Identifier, Version and a Command "
          "Identifier")
    {
        const QString Originator{"org.silastandard"};
        const QString Category{"test.something"};
        const QString Identifier{"SomeFeature"};
        const QString Version{"v1"};
        const QString Command{"SayHello"};

        WHEN("a Fully Qualified Command ID is constructed from these")
        {
            const auto FullyQualifiedCommandID = CFullyQualifiedCommandID{
                {Originator, Category, Identifier, Version}, Command};

            THEN("the getters to return the correct values")
            {
                REQUIRE(FullyQualifiedCommandID.featureIdentifier().originator()
                        == Originator);
                REQUIRE(FullyQualifiedCommandID.featureIdentifier().category()
                        == Category);
                REQUIRE(FullyQualifiedCommandID.featureIdentifier().identifier()
                        == Identifier);
                REQUIRE(FullyQualifiedCommandID.featureIdentifier().version()
                        == Version);
                REQUIRE(FullyQualifiedCommandID.identifier() == Command);
            }
            AND_WHEN("the Fully Qualified Command ID is converted into a string")
            {
                const auto String = FullyQualifiedCommandID.toStdString();
                THEN("it conforms to the SiLA 2 standard definition of a Fully "
                     "Qualified Command Identifier")
                {
                    const auto ConformingString =
                        QString{Originator + "/" + Category + "/" + Identifier
                                + "/" + Version + "/Command/" + Command}
                            .toStdString();
                    REQUIRE(String == ConformingString);

                    AND_WHEN("a Fully Qualified Command Identifier is "
                             "constructed from a SiLA 2 conforming string")
                    {
                        const auto IdentifierFromString =
                            CFullyQualifiedCommandID::fromStdString(
                                ConformingString);
                        THEN("the two Fully Qualified Command Identifiers are "
                             "the same")
                        {
                            CAPTURE(IdentifierFromString);
                            REQUIRE(IdentifierFromString.isValid());
                            REQUIRE(FullyQualifiedCommandID.featureIdentifier()
                                        .originator()
                                    == IdentifierFromString.featureIdentifier()
                                           .originator());
                            REQUIRE(FullyQualifiedCommandID.featureIdentifier()
                                        .category()
                                    == IdentifierFromString.featureIdentifier()
                                           .category());
                            REQUIRE(FullyQualifiedCommandID.featureIdentifier()
                                        .identifier()
                                    == IdentifierFromString.featureIdentifier()
                                           .identifier());
                            REQUIRE(FullyQualifiedCommandID.featureIdentifier()
                                        .version()
                                    == IdentifierFromString.featureIdentifier()
                                           .version());
                            REQUIRE(FullyQualifiedCommandID.identifier()
                                    == IdentifierFromString.identifier());
                        }
                    }
                }
            }
            AND_WHEN("a Fully Qualified Command Identifier is constructed "
                     "from an invalid string")
            {
                const auto InvalidString =
                    GENERATE(""s, "////Command/"s,
                             "this/is/not/a/fully/qualified/identifier"s);
                const auto InvalidIdentifier =
                    CFullyQualifiedCommandID::fromStdString(InvalidString);
                THEN("the Identifier is invalid")
                {
                    REQUIRE_FALSE(InvalidIdentifier.isValid());
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Construct a Fully Qualified Command Parameter Identifier",
         "[fqid][command]")
{
    GIVEN("An Originator, Category, Feature Identifier, Version, Command "
          "Identifier and a Parameter")
    {
        const QString Originator{"org.silastandard"};
        const QString Category{"test.something"};
        const QString Identifier{"SomeFeature"};
        const QString Version{"v1"};
        const QString Command{"SayHello"};
        const QString Parameter{"Name"};

        WHEN("a Fully Qualified Command Parameter ID is constructed from these")
        {
            const auto FullyQualifiedCommandParameterID =
                CFullyQualifiedCommandParameterID{
                    {{Originator, Category, Identifier, Version}, Command},
                    Parameter};

            THEN("the getters to return the correct values")
            {
                REQUIRE(FullyQualifiedCommandParameterID.featureIdentifier()
                            .originator()
                        == Originator);
                REQUIRE(
                    FullyQualifiedCommandParameterID.featureIdentifier().category()
                    == Category);
                REQUIRE(FullyQualifiedCommandParameterID.featureIdentifier()
                            .identifier()
                        == Identifier);
                REQUIRE(
                    FullyQualifiedCommandParameterID.featureIdentifier().version()
                    == Version);
                REQUIRE(FullyQualifiedCommandParameterID.commandIdentifier()
                            .identifier()
                        == Command);
                REQUIRE(FullyQualifiedCommandParameterID.identifier()
                        == Parameter);
            }
            AND_WHEN("the Fully Qualified Command Parameter ID is converted into "
                     "a string")
            {
                const auto String =
                    FullyQualifiedCommandParameterID.toStdString();
                THEN("it conforms to the SiLA 2 standard definition of a Fully "
                     "Qualified Command Parameter Identifier")
                {
                    const auto ConformingString =
                        QString{Originator + "/" + Category + "/" + Identifier
                                + "/" + Version + "/Command/" + Command
                                + "/Parameter/" + Parameter}
                            .toStdString();
                    REQUIRE(String == ConformingString);

                    AND_WHEN("a Fully Qualified Command Parameter Identifier is "
                             "constructed from a SiLA 2 conforming string")
                    {
                        const auto IdentifierFromString =
                            CFullyQualifiedCommandParameterID::fromStdString(
                                ConformingString);
                        THEN("the two Fully Qualified Command Identifiers are "
                             "the same")
                        {
                            CAPTURE(IdentifierFromString);
                            REQUIRE(IdentifierFromString.isValid());
                            REQUIRE(FullyQualifiedCommandParameterID
                                        .featureIdentifier()
                                        .originator()
                                    == IdentifierFromString.featureIdentifier()
                                           .originator());
                            REQUIRE(FullyQualifiedCommandParameterID
                                        .featureIdentifier()
                                        .category()
                                    == IdentifierFromString.featureIdentifier()
                                           .category());
                            REQUIRE(FullyQualifiedCommandParameterID
                                        .featureIdentifier()
                                        .identifier()
                                    == IdentifierFromString.featureIdentifier()
                                           .identifier());
                            REQUIRE(FullyQualifiedCommandParameterID
                                        .featureIdentifier()
                                        .version()
                                    == IdentifierFromString.featureIdentifier()
                                           .version());
                            REQUIRE(FullyQualifiedCommandParameterID
                                        .commandIdentifier()
                                        .identifier()
                                    == IdentifierFromString.commandIdentifier()
                                           .identifier());
                            REQUIRE(FullyQualifiedCommandParameterID.identifier()
                                    == IdentifierFromString.identifier());
                        }
                    }
                }
            }
            AND_WHEN(
                "a Fully Qualified Command Parameter Identifier is constructed "
                "from an invalid string")
            {
                const auto InvalidString =
                    GENERATE(""s, "////Command//Parameter/"s,
                             "this/is/not/a/fully/qualified/identifier"s);
                const auto InvalidIdentifier =
                    CFullyQualifiedCommandID::fromStdString(InvalidString);
                THEN("the Identifier is invalid")
                {
                    REQUIRE_FALSE(InvalidIdentifier.isValid());
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Construct a Fully Qualified Command Response Identifier",
         "[fqid][command]")
{
    GIVEN("An Originator, Category, Feature Identifier, Version, Command "
          "Identifier and a Response")
    {
        const QString Originator{"org.silastandard"};
        const QString Category{"test.something"};
        const QString Identifier{"SomeFeature"};
        const QString Version{"v1"};
        const QString Command{"SayHello"};
        const QString Response{"Name"};

        WHEN("a Fully Qualified Command Response ID is constructed from these")
        {
            const auto FullyQualifiedCommandResponseID =
                CFullyQualifiedCommandResponseID{
                    {{Originator, Category, Identifier, Version}, Command},
                    Response};

            THEN("the getters to return the correct values")
            {
                REQUIRE(FullyQualifiedCommandResponseID.featureIdentifier()
                            .originator()
                        == Originator);
                REQUIRE(
                    FullyQualifiedCommandResponseID.featureIdentifier().category()
                    == Category);
                REQUIRE(FullyQualifiedCommandResponseID.featureIdentifier()
                            .identifier()
                        == Identifier);
                REQUIRE(
                    FullyQualifiedCommandResponseID.featureIdentifier().version()
                    == Version);
                REQUIRE(FullyQualifiedCommandResponseID.commandIdentifier()
                            .identifier()
                        == Command);
                REQUIRE(FullyQualifiedCommandResponseID.identifier() == Response);
            }
            AND_WHEN("the Fully Qualified Command Response ID is converted into "
                     "a string")
            {
                const auto String = FullyQualifiedCommandResponseID.toStdString();
                THEN("it conforms to the SiLA 2 standard definition of a Fully "
                     "Qualified Command Response Identifier")
                {
                    const auto ConformingString =
                        QString{Originator + "/" + Category + "/" + Identifier
                                + "/" + Version + "/Command/" + Command
                                + "/Response/" + Response}
                            .toStdString();
                    REQUIRE(String == ConformingString);

                    AND_WHEN("a Fully Qualified Command Response Identifier is "
                             "constructed from a SiLA 2 conforming string")
                    {
                        const auto IdentifierFromString =
                            CFullyQualifiedCommandResponseID::fromStdString(
                                ConformingString);
                        THEN("the two Fully Qualified Command Identifiers are "
                             "the same")
                        {
                            CAPTURE(IdentifierFromString);
                            REQUIRE(IdentifierFromString.isValid());
                            REQUIRE(FullyQualifiedCommandResponseID
                                        .featureIdentifier()
                                        .originator()
                                    == IdentifierFromString.featureIdentifier()
                                           .originator());
                            REQUIRE(FullyQualifiedCommandResponseID
                                        .featureIdentifier()
                                        .category()
                                    == IdentifierFromString.featureIdentifier()
                                           .category());
                            REQUIRE(FullyQualifiedCommandResponseID
                                        .featureIdentifier()
                                        .identifier()
                                    == IdentifierFromString.featureIdentifier()
                                           .identifier());
                            REQUIRE(FullyQualifiedCommandResponseID
                                        .featureIdentifier()
                                        .version()
                                    == IdentifierFromString.featureIdentifier()
                                           .version());
                            REQUIRE(FullyQualifiedCommandResponseID
                                        .commandIdentifier()
                                        .identifier()
                                    == IdentifierFromString.commandIdentifier()
                                           .identifier());
                            REQUIRE(FullyQualifiedCommandResponseID.identifier()
                                    == IdentifierFromString.identifier());
                        }
                    }
                }
            }
            AND_WHEN(
                "a Fully Qualified Command Response Identifier is constructed "
                "from an invalid string")
            {
                const auto InvalidString =
                    GENERATE(""s, "////Command//Response/"s,
                             "this/is/not/a/fully/qualified/identifier"s);
                const auto InvalidIdentifier =
                    CFullyQualifiedCommandID::fromStdString(InvalidString);
                THEN("the Identifier is invalid")
                {
                    REQUIRE_FALSE(InvalidIdentifier.isValid());
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Construct a Fully Qualified Command Intermediate Response Identifier",
         "[fqid][command]")
{
    GIVEN("An Originator, Category, Feature Identifier, Version, Command "
          "Identifier and an Intermediate Response")
    {
        const QString Originator{"org.silastandard"};
        const QString Category{"test.something"};
        const QString Identifier{"SomeFeature"};
        const QString Version{"v1"};
        const QString Command{"SayHello"};
        const QString IntermediateResponse{"IntermediateGreeting"};

        WHEN("a Fully Qualified Command Intermediate Response ID is constructed "
             "from these")
        {
            const auto FullyQualifiedCommandIntermediateResponseID =
                CFullyQualifiedCommandIntermediateResponseID{
                    {{Originator, Category, Identifier, Version}, Command},
                    IntermediateResponse};

            THEN("the getters to return the correct values")
            {
                REQUIRE(FullyQualifiedCommandIntermediateResponseID
                            .featureIdentifier()
                            .originator()
                        == Originator);
                REQUIRE(FullyQualifiedCommandIntermediateResponseID
                            .featureIdentifier()
                            .category()
                        == Category);
                REQUIRE(FullyQualifiedCommandIntermediateResponseID
                            .featureIdentifier()
                            .identifier()
                        == Identifier);
                REQUIRE(FullyQualifiedCommandIntermediateResponseID
                            .featureIdentifier()
                            .version()
                        == Version);
                REQUIRE(FullyQualifiedCommandIntermediateResponseID
                            .commandIdentifier()
                            .identifier()
                        == Command);
                REQUIRE(FullyQualifiedCommandIntermediateResponseID.identifier()
                        == IntermediateResponse);
            }
            AND_WHEN("the Fully Qualified Command Intermediate Response ID is "
                     "converted into a string")
            {
                const auto String =
                    FullyQualifiedCommandIntermediateResponseID.toStdString();
                THEN("it conforms to the SiLA 2 standard definition of a Fully "
                     "Qualified Command Intermediate Response Identifier")
                {
                    const auto ConformingString =
                        QString{Originator + "/" + Category + "/" + Identifier
                                + "/" + Version + "/Command/" + Command
                                + "/IntermediateResponse/" + IntermediateResponse}
                            .toStdString();
                    REQUIRE(String == ConformingString);

                    AND_WHEN("a Fully Qualified Command Intermediate Response "
                             "Identifier is constructed from a SiLA 2 conforming "
                             "string")
                    {
                        const auto IdentifierFromString =
                            CFullyQualifiedCommandIntermediateResponseID::
                                fromStdString(ConformingString);
                        THEN("the two Fully Qualified Command Identifiers are "
                             "the same")
                        {
                            CAPTURE(IdentifierFromString);
                            REQUIRE(IdentifierFromString.isValid());
                            REQUIRE(FullyQualifiedCommandIntermediateResponseID
                                        .featureIdentifier()
                                        .originator()
                                    == IdentifierFromString.featureIdentifier()
                                           .originator());
                            REQUIRE(FullyQualifiedCommandIntermediateResponseID
                                        .featureIdentifier()
                                        .category()
                                    == IdentifierFromString.featureIdentifier()
                                           .category());
                            REQUIRE(FullyQualifiedCommandIntermediateResponseID
                                        .featureIdentifier()
                                        .identifier()
                                    == IdentifierFromString.featureIdentifier()
                                           .identifier());
                            REQUIRE(FullyQualifiedCommandIntermediateResponseID
                                        .featureIdentifier()
                                        .version()
                                    == IdentifierFromString.featureIdentifier()
                                           .version());
                            REQUIRE(FullyQualifiedCommandIntermediateResponseID
                                        .commandIdentifier()
                                        .identifier()
                                    == IdentifierFromString.commandIdentifier()
                                           .identifier());
                            REQUIRE(FullyQualifiedCommandIntermediateResponseID
                                        .identifier()
                                    == IdentifierFromString.identifier());
                        }
                    }
                }
            }
            AND_WHEN("a Fully Qualified Command Intermediate Response Identifier "
                     "is constructed from an invalid string")
            {
                const auto InvalidString =
                    GENERATE(""s, "////Command//IntermediateResponse/"s,
                             "this/is/not/a/fully/qualified/identifier"s);
                const auto InvalidIdentifier =
                    CFullyQualifiedCommandID::fromStdString(InvalidString);
                THEN("the Identifier is invalid")
                {
                    REQUIRE_FALSE(InvalidIdentifier.isValid());
                }
            }
        }
    }
}

//============================================================================
SCENARIO("Construct a Fully Qualified Property Identifier", "[fqid][command]")
{
    GIVEN("An Originator, Category, Feature Identifier, Version and a Property "
          "Identifier")
    {
        const QString Originator{"org.silastandard"};
        const QString Category{"test.something"};
        const QString Identifier{"SomeFeature"};
        const QString Version{"v1"};
        const QString Property{"StartYear"};

        WHEN("a Fully Qualified Property ID is constructed from these")
        {
            const auto FullyQualifiedPropertyID = CFullyQualifiedPropertyID{
                {Originator, Category, Identifier, Version}, Property};

            THEN("the getters to return the correct values")
            {
                REQUIRE(FullyQualifiedPropertyID.featureIdentifier().originator()
                        == Originator);
                REQUIRE(FullyQualifiedPropertyID.featureIdentifier().category()
                        == Category);
                REQUIRE(FullyQualifiedPropertyID.featureIdentifier().identifier()
                        == Identifier);
                REQUIRE(FullyQualifiedPropertyID.featureIdentifier().version()
                        == Version);
                REQUIRE(FullyQualifiedPropertyID.identifier() == Property);
            }
            AND_WHEN("the Fully Qualified Property ID is converted into a string")
            {
                const auto String = FullyQualifiedPropertyID.toStdString();
                THEN("it conforms to the SiLA 2 standard definition of a Fully "
                     "Qualified Property Identifier")
                {
                    const auto ConformingString =
                        QString{Originator + "/" + Category + "/" + Identifier
                                + "/" + Version + "/Property/" + Property}
                            .toStdString();
                    REQUIRE(String == ConformingString);

                    AND_WHEN("a Fully Qualified Property Identifier is "
                             "constructed from a SiLA 2 conforming string")
                    {
                        const auto IdentifierFromString =
                            CFullyQualifiedPropertyID::fromStdString(
                                ConformingString);
                        THEN("the two Fully Qualified Property Identifiers are "
                             "the same")
                        {
                            CAPTURE(IdentifierFromString);
                            REQUIRE(IdentifierFromString.isValid());
                            REQUIRE(FullyQualifiedPropertyID.featureIdentifier()
                                        .originator()
                                    == IdentifierFromString.featureIdentifier()
                                           .originator());
                            REQUIRE(FullyQualifiedPropertyID.featureIdentifier()
                                        .category()
                                    == IdentifierFromString.featureIdentifier()
                                           .category());
                            REQUIRE(FullyQualifiedPropertyID.featureIdentifier()
                                        .identifier()
                                    == IdentifierFromString.featureIdentifier()
                                           .identifier());
                            REQUIRE(FullyQualifiedPropertyID.featureIdentifier()
                                        .version()
                                    == IdentifierFromString.featureIdentifier()
                                           .version());
                            REQUIRE(FullyQualifiedPropertyID.identifier()
                                    == IdentifierFromString.identifier());
                        }
                    }
                }
            }
            AND_WHEN("a Fully Qualified Property Identifier is constructed "
                     "from an invalid string")
            {
                const auto InvalidString =
                    GENERATE(""s, "////Property/"s,
                             "this/is/not/a/fully/qualified/identifier"s);
                const auto InvalidIdentifier =
                    CFullyQualifiedPropertyID::fromStdString(InvalidString);
                THEN("the Identifier is invalid")
                {
                    REQUIRE_FALSE(InvalidIdentifier.isValid());
                }
            }
        }
    }
}