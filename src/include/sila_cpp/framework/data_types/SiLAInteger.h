/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAInteger.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   17.01.2020
/// \brief  Declaration of the CInteger class
//============================================================================
#ifndef SILAINTEGER_H
#define SILAINTEGER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include "DataType.h"
#include "utils.h"

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace sila2::org::silastandard
{
class Integer;
}

namespace SiLA2
{
/**
 * @brief The CInteger class provides a convenience wrapper around SiLAFramework's
 * Integer class.
 *
 * @details The <b>SiLA Integer Type</b> represents an integer number within a
 * range from the minimum value of -2^63 up to the maximum value of 2^63 - 1.
 */
class SILA_CPP_EXPORT CInteger : public CDataType<int64_t>
{
    using Base = CDataType<int64_t>;

public:
    /**
     * @brief C'tor
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CInteger(int64_t Value = 0);

    /**
     * @brief Converting copy c'tor from sila2::org::silastandard::Integer
     */
    // NOLINTNEXTLINE(google-explicit-constructor)
    CInteger(const sila2::org::silastandard::Integer& rhs);

    SILA_CPP_CREATE_SPECIAL_MEMBER_FUNCTIONS(CInteger, Base)

    /**
     * @brief Construct a new @c CInteger from the given protobuf @c Message
     * @a from
     *
     * This is just for convenience if you happen to have a pointer or reference
     * to @c google::protobuf::Message and want to convert that to a @c CInteger
     * without having to cast it to a @c sila2::org::silastandard::Integer before.
     * The conversion will be performed for you here but might fail if you pass a
     * @c Message that is not an @c Integer.
     */
    [[nodiscard]] static CInteger fromProtoMessage(
        const google::protobuf::Message& from);

    /**
     * @brief Pre-increment operator
     */
    CInteger& operator++();

    /**
     * @brief Post-increment operator
     */
    const CInteger operator++(int);

    /**
     * @brief Pre-decrement operator
     */
    CInteger& operator--();

    /**
     * @brief Post-decrement operator
     */
    const CInteger operator--(int);

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     */
    [[nodiscard]] sila2::org::silastandard::Integer toProtoMessage() const;

    /**
     * @brief Convert this convenience type to a SiLAFramework type, i.e. the
     * Protobuf Message
     *
     * @return The SiLAFramework equivalent of this type as a Protobuf Message
     * pointer
     */
    [[nodiscard]] sila2::org::silastandard::Integer* toProtoMessagePtr() const;
};

/**
 * @brief Overload for debugging CIntegers
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CInteger& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os, const CInteger& rhs);
}  // namespace SiLA2

#endif  // SILAINTEGER_H
