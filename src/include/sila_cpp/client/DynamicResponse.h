/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicResponse.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   16.03.2021
/// \brief  Declaration of the CDynamicResponse and CDynamicResponseList classes
//============================================================================
#ifndef DYNAMICRESPONSE_H
#define DYNAMICRESPONSE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicStructure.h>
#include <sila_cpp/client/DynamicValue.h>
#include <sila_cpp/codegen/fdl/Response.h>
#include <sila_cpp/common/logging.h>

#include <polymorphic_value.h>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
namespace SiLA2
{
class CDynamicCommand;
}  // namespace SiLA2

namespace SiLA2
{
/**
 * @brief The CDynamicResponse class represents a response that is returned when
 * calling a @c CDynamicCommand that is not known at compile time. It simply
 * consists of an Identifier (as it is returned by
 * @c CDynamicCommand::responses(), i.e. not Fully Qualified since the Command
 * knows its Fully Qualified Identifier an can infer the Response's from it) and a
 * Value.
 */
class SILA_CPP_EXPORT CDynamicResponse : public CDynamicValue
{
    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;

public:
    /**
     * @brief Copy c'tor
     */
    CDynamicResponse(const CDynamicResponse& rhs);

    /**
     * @brief Move c'tor
     */
    CDynamicResponse(CDynamicResponse&& rhs) noexcept = default;

    /**
     * @brief Copy assignment operator
     */
    CDynamicResponse& operator=(const CDynamicResponse& rhs) = default;

    /**
     * @brief Move assignment operator
     */
    CDynamicResponse& operator=(CDynamicResponse&& rhs) noexcept = default;

    /**
     * @brief Get the Fully Qualified Identifier of this Response
     *
     * @return The Response's Fully Qualified Identifier
     */
    [[nodiscard]] CFullyQualifiedCommandResponseID identifier() const;

    /**
     * @brief Get the Display Name of this Response
     *
     * @return The Response's Display Name
     */
    [[nodiscard]] QString displayName() const;

    /**
     * @brief Get the Description of this Response
     *
     * @return The Response's Description
     */
    [[nodiscard]] QString description() const;

    /**
     * @brief Get a pointer to the Feature that this Response belongs to
     *
     * @note No ownership is transferred here!
     *
     * @return A pointer to the Feature of this Response
     */
    [[nodiscard]] CDynamicFeatureStub* feature() const;

    /**
     * @brief Get a default constructed (i.e. empty) @c CDynamicStructure object
     * for this Value
     *
     * This object can be filled with the necessary Structure Element values
     * and then set as this Value by using @a setValue() or added to a List by
     * using @a addValue()
     *
     * @note This function should only be called when
     * <code>dataTypeType() == eDataTypeType::STRUCTURE</code> or
     * <code>underlyingDataTypeType() == eDataTypeType::STRUCTURE</code>.
     * Otherwise the returned @c CDynamicStructure will have no Elements (since
     * this Value has a different Data Type Type)
     *
     * @return An empty @c CDynamicStructure
     */
    [[nodiscard]] CDynamicStructure structure() const override;

    /**
     * @brief Returns the string representation of this Response that can be
     * displayed in a UI
     *
     * @return The Response's string representation
     */
    [[nodiscard]] QString prettyString() const override;

private:
    friend class CDynamicCommand;
    friend class CIntermediateResponseSubscription;
    /**
     * @internal Only to be used by @c CDynamicCommand and
     * @c CIntermediateResponseSubscription
     * @brief C'tor
     *
     * @param Value The value of the Response (already having the correct type)
     * @param ResponseFDL The parsed FDL description of the Response
     * @param CommandID The Fully Qualified Command Identifier of the Command that
     * this Response belongs to
     * @param DMF The Dynamic Message Factory to use to create the protobuf
     * Messages for any Custom Data Types or Structures that this Response might
     * hold
     * @param Feature The SiLA Feature Stub that this Response belongs to
     */
    CDynamicResponse(google::protobuf::Message* Value,
                     const codegen::fdl::CResponse& ResponseFDL,
                     CFullyQualifiedCommandID CommandID,
                     std::shared_ptr<codegen::proto::CDynamicMessageFactory> DMF,
                     CDynamicFeatureStub* Feature);

    /**
     * @brief Hide all @c setValue variants since the user shouldn't be able to
     * change the value of a Response
     */
    using CDynamicValue::setValue;

    /**
     * @brief Hide all @c operator= variants since the user shouldn't be able to
     * change the value of a Response
     */
    using CDynamicValue::operator=;

    PIMPL_DECLARE_PRIVATE(CDynamicResponse)
};

/**
 * @brief The CDynamicResponseList class is a simple wrapper around
 * @c QList<CDynamicResponse> that provides a few more convenience methods to
 * access the Response values in the list
 */
class SILA_CPP_EXPORT CDynamicResponseList : public QList<CDynamicResponse>
{
public:
    using QList::QList;

    /**
     * @brief Converting copy c'tor
     */
    explicit CDynamicResponseList(const QList<CDynamicResponse>& rhs);

    /**
     * @brief Get the identifiers of all @c CDynamicResponses in this list
     *
     * @return A list of all Response Identifiers
     */
    [[nodiscard]] QList<CFullyQualifiedCommandResponseID> identifiers() const;

    /**
     * @brief Returns the @c CDynamicResponse for the given @a Identifier
     *
     * @param Identifier The Identifier of the Response to get
     * @return A const reference to the @c CDynamicResponse
     *
     * @throws std::out_of_range if there is no @c CDynamicResponse with the
     * given name @a Identifier
     */
    [[nodiscard]] const CDynamicResponse& at(std::string_view Identifier) const;

    /**
     * @brief Returns the @c CDynamicResponse for the given @a Identifier
     *
     * @param Identifier The Identifier of the Response to get
     * @return A const reference to the @c CDynamicResponse
     *
     * @throws std::out_of_range if there is no @c CDynamicResponse with the
     * given name @a Identifier
     */
    [[nodiscard]] const CDynamicResponse& at(QStringView Identifier) const
    {
        return at(Identifier.toString().toStdString());
    }

    /**
     * @brief Returns the @c CDynamicResponse for the given @a Identifier
     *
     * @param Identifier The Fully Qualified Identifier of the Response to get
     * @return A const reference to the @c CDynamicResponse
     *
     * @throws std::out_of_range if there is no @c CDynamicResponse with the
     * given name @a Identifier
     */
    [[nodiscard]] const CDynamicResponse& at(
        const CFullyQualifiedCommandResponseID& Identifier) const;

    /**
     * @brief Returns the @c CDynamicResponse for the given @a Identifier
     *
     * @param Identifier The Identifier of the Response to get
     * @return A reference to the @c CDynamicResponse
     *
     * @throws std::out_of_range if there is no @c CDynamicResponse with the
     * given name @a Identifier
     */
    [[nodiscard]] CDynamicResponse& operator[](std::string_view Identifier);

    /**
     * @brief Returns the @c CDynamicResponse for the given @a Identifier
     *
     * @param Identifier The Identifier of the Response to get
     * @return A reference to the @c CDynamicResponse
     *
     * @throws std::out_of_range if there is no @c CDynamicResponse with the
     * given name @a Identifier
     */
    [[nodiscard]] CDynamicResponse& operator[](QStringView Identifier)
    {
        return operator[](Identifier.toString().toStdString());
    }

    /**
     * @brief Returns the @c CDynamicResponse for the given @a Identifier
     *
     * @param Identifier The Fully Qualified Identifier of the Response to get
     * @return A reference to the @c CDynamicResponse
     *
     * @throws std::out_of_range if there is no @c CDynamicResponse with the
     * given name @a Identifier
     */
    [[nodiscard]] CDynamicResponse& operator[](
        const CFullyQualifiedCommandResponseID& Identifier);

    using QList::at;
    using QList::operator[];
};

/**
 * @brief Overload for debugging @c CDynamicResponse
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CDynamicResponse& rhs);

/**
 * @brief Overload for printing @c CDynamicResponses
 */
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CDynamicResponse& rhs);
}  // namespace SiLA2
#endif  // DYNAMICRESPONSE_H
