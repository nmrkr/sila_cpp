/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAClient.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   08.01.2020
/// \brief  Declaration of the CSiLAClient class
//============================================================================
#ifndef SILACLIENT_H
#define SILACLIENT_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/SSLCredentials.h>
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/common/ServerInformation.h>
#include <sila_cpp/config.h>
#include <sila_cpp/data_types.h>
#include <sila_cpp/global.h>

#include <polymorphic_value.h>

//=============================================================================
//                            FORWARD DECLARATIONS
//=============================================================================
#if GRPC_VERSION >= GRPC_VERSION_CHECK(1, 32, 0)
namespace grpc
{
class Channel;
}  // namespace grpc
#else
namespace grpc_impl
{
class Channel;
}  // namespace grpc_impl
namespace grpc
{
typedef grpc_impl::Channel Channel;
}  // namespace grpc
#endif

class QCommandLineParser;
class QSslCertificate;

namespace SiLA2
{
class CFullyQualifiedFeatureID;

/**
 * @brief The CSiLAClient class provides a base for implementing custom SiLA2
 * clients
 */
class SILA_CPP_EXPORT CSiLAClient
{
protected:
    class PrivateImpl;
    using PrivateImplPtr =
        isocpp_p0201::polymorphic_value<CSiLAClient::PrivateImpl>;

public:
    /**
     * @brief C'tor
     *
     * @param Address The IP address and port of the server to connect to
     * These can be overwritten by passing command line arguments to SiLA client
     * applications.
     *
     * @sa CSiLAClient::connect
     */
    explicit CSiLAClient(const CServerAddress& Address);

    /**
     * @brief D'tor
     */
    virtual ~CSiLAClient() = default;

    /**
     * @brief Connects to the Server and retrieves some general information about
     * the server (like name, description, ...). Emits @c connected() upon
     * successful connection to the Server.
     * Calling this method multiple times will do nothing if
     * the connection has already been established.
     *
     * @param Credentials Optional SSL certificate and key for secure
     * communication; if left empty, the credentials are tried to be obtained from
     * the server during @c connect(). If these are set @em and at the same time
     * credentials are given through command line arguments, then the latter take
     * precedence (see @a CSiLAClient::commandLineParser).
     *
     * @note This function may be overridden by a subclass to add some additional
     * behaviour after the base @c CSiLAClient has connected to the Server (e.g.
     * initializing your own Feature's stubs). So be sure to always call the base
     * class implementation before you do anything else because otherwise you will
     * most certainly run into errors.
     *
     * @throws std::runtime_error if the connection to the Server cannot be
     * established
     *
     * @sa CSiLAClient::connectInsecure
     */
    virtual void connect();
    void connect(const CSSLCredentials& Credentials);

    /**
     * @brief Connects to the Server using unencrypted communication and retrieves
     * some general information about the server (like name, description, ...).
     * Emits @c connected() upon successful connection to the Server.
     * Calling this method multiple times will do nothing if the connection has
     * already been established.
     *
     * @note The SiLA 2 standard requires all connections to be encrypted. Only
     * use this function for testing purposes.
     *
     * @note This function calls @c connect(), so if you want to add some
     * additional behaviour after the base @c CSiLAClient has connected to the
     * Server (e.g. initializing your own Feature's stubs) you still need to
     * override @c connect().
     *
     * @throws std::runtime_error if the connection to the Server cannot be
     * established
     *
     * @sa CSiLAClient::connect
     */
    void connectInsecure();

    /**
     * @brief Get the gRPC channel to the connected Server
     *
     * @details This method can be used in a subclass to construct a new stub
     * for a SiLA feature, e.g.:
     * \code{.cpp}
     * auto GreetingProviderStub = GreetingProvider::NewStub(channel());
     * \endcode
     *
     * @return std::shared_ptr<grpc::Channel> The gRPC channel to the Server
     */
    [[nodiscard]] std::shared_ptr<grpc::Channel> channel() const;

    /**
     * @brief Get the IP address and port of the server that this Client is
     * connected to
     *
     * @return The IP address and port of the connected Server
     */
    [[nodiscard]] const CServerAddress& serverAddress() const;

    /**
     * @brief Get the Server Information (name, description, ...) of the Server
     * that this client is connected to
     *
     * @return The Server Information of the connected Server
     */
    [[nodiscard]] const CServerInformation& serverInformation() const;

    /**
     * @brief Get the SSL credentials used by this client to connect to the Server
     *
     * In the returned object will usually only contain the @b rootCA since SiLA
     * Clients are not required to have a certificate themselves.
     *
     * @return The SSL credentials used to connect to the Server
     */
    [[nodiscard]] const CSSLCredentials& credentials() const;

    /**
     * @brief Whether the Client should accept the untrusted @a Certificate. Every
     * subclass should implement this method and handle certificates that are
     * self-signed or otherwise untrusted (e.g. if the root CA could not be looked
     * up to be a trusted CA). If this function returns @c false then all
     * communication with the Server will be immediately stopped. If it returns
     * @c true then communication continues.
     * The default implementation always returns @c false (i.e. the @a Certificate
     * will not be accepted).
     *
     * @param Certificate The certificate that could not be verified to be trusted
     *
     * @returns true, if the @a Certificate should be accepted, false otherwise
     */
    [[nodiscard]] virtual bool acceptUntrustedServerCertificate(
        const QSslCertificate& Certificate) const;

    /**
     * @brief Get the client's command line parser
     * This can be used to add additional command line options to the parser.
     * Per default, the client command line parser contains the following options:
     * @li -?, -h, --help
     * @li -v, --version
     * @li -s, --server-host <ip-or-hostname>
     * @li -p, --server-port <port>
     * @li -r, --root-ca <filename>
     * @li -c, --encryption-cert <filename>
     * @li -k, --encryption-key <filename>
     * @li -i, --force-insecure
     *
     * @note CSiLAClient will parse these options automatically and overwrite the
     * options given in the c'tor if necessary. So, if you extend the parser with
     * your custom options you'll only have to parse those and not all of the
     * options.
     * @note If you don't set SSL credentials via command line arguments
     * @b CSiLAClient tries to obtain the server's certificate for secure
     * communication.
     * However, if you specify credentials via both the command line @em and the
     * c'tor argument, then the values read from the command line will take
     * precedence!
     *
     * @return The client's command line parser
     *
     * @sa CSiLAClient::CSiLAClient
     */
    [[nodiscard]] static std::shared_ptr<QCommandLineParser> commandLineParser();

    /// ================== SiLA Service Stub implementation ==================
    /**
     * @brief Get the Fully Qualified Feature Identifier for the SiLAService
     * Feature
     *
     * @return The SiLAService Feature's Fully Qualified Identifier
     */
    [[nodiscard]] static CFullyQualifiedFeatureID silaServiceFeatureID();

    /**
     * @brief Call the Unobservable Command "Get Feature Definition" on the server
     *
     * @param FeatureID The Fully Qualified Feature Identifier for which
     * the FeatureDefinition should be retrieved
     *
     * @return CString The FeatureDefinition in XML format
     *
     * @throws @c CSiLAError if the execution of the Command failed
     */
    CString GetFeatureDefinition(const CFullyQualifiedFeatureID& FeatureID);

    /**
     * @brief Call the Unobservable Command "Set Server Name" on the server
     *
     * @param ServerName The human readable name to assign to the SiLA Server
     *
     * @throws @c CSiLAError if the execution of the Command failed
     */
    void SetServerName(const CString& ServerName);

    /**
     * @brief Request the Unobservable Property "Server Name" on the server
     *
     * @return The Server's Name
     *
     * @throws @c CSiLAError if the reading of the Property failed
     */
    CString Get_ServerName();

    /**
     * @brief Request the Unobservable Property "Server Type" on the server
     *
     * @return The Server's Type
     *
     * @throws @c CSiLAError if the reading of the Property failed
     */
    CString Get_ServerType();

    /**
     * @brief Request the Unobservable Property "Server UUID" on the server
     *
     * @return The Server's UUID
     *
     * @throws @c CSiLAError if the reading of the Property failed
     */
    CString Get_ServerUUID();

    /**
     * @brief Request the Unobservable Property "Server Description" on the server
     *
     * @return The Server's Description
     *
     * @throws @c CSiLAError if the reading of the Property failed
     */
    CString Get_ServerDescription();

    /**
     * @brief Request the Unobservable Property "Server Version" on the server
     *
     * @return The Server's Version
     *
     * @throws @c CSiLAError if the reading of the Property failed
     */
    CString Get_ServerVersion();

    /**
     * @brief Request the Unobservable Property "Server Vendor URL" on the server
     *
     * @return The Server's Vendor URL
     *
     * @throws @c CSiLAError if the reading of the Property failed
     */
    CString Get_ServerVendorURL();

    /**
     * @brief Request the Unobservable Property "Implemented Features" on the
     * server
     *
     * @return A list of the Features implemented by the Server
     *
     * @throws @c CSiLAError if the reading of the Property failed
     */
    std::vector<CString> Get_ImplementedFeatures();

protected:
    /**
     * @brief C'tor for derived classes
     *
     * @param priv Private data of the class
     */
    explicit CSiLAClient(PrivateImplPtr priv);

    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CSiLAClient)
};
}  // namespace SiLA2

#endif  // SILACLIENT_H
