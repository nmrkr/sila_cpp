/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ICommandManager.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   13.07.2020
/// \brief  Definition of the ICommandManager class
//============================================================================
#ifndef ICOMMANDMANAGER_H
#define ICOMMANDMANAGER_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FullyQualifiedCommandID.h>
#include <sila_cpp/server/SiLAFeature.h>

#include <functional>

namespace SiLA2
{
//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
template<template<typename P, typename R, typename... I> class W, typename P,
         typename R, typename... I>
using ExecutorFunc = std::function<R(W<P, R, I...>*)>;

template<typename F, template<typename P, typename R, typename... I> class W,
         typename P, typename R, typename... I>
using ExecutorMemFunc = R (F::*)(W<P, R, I...>*);

/**
 * @brief The ICommandManager class is the base class of every SiLA 2 Command
 * Manager containing common functionality like accessors for the Executor
 * function.
 *
 * @tparam CommandWrapper The Command Wrapper class that will be passed as
 * parameter in the Executor
 */
template<typename CommandWrapper>
class ICommandManager;

/**
 * @brief Signature of the Command Wrapper class template
 */
#define CommandWrapperTemplate                                                   \
    template<typename Parameters, typename Responses,                            \
             typename... IntermediateResponses>                                  \
    class CommandWrapperT

/**
 * @brief The ICommandManager class is the base class of every SiLA 2 Command
 * Manager containing common functionality like accessors for the Executor
 * function.
 *
 * @tparam CommandWrapperT The Command Wrapper class that will be passed as
 * parameter in the Executor and Validator
 * @tparam ParametersT The type of the Command Parameters
 * @tparam ResponsesT The type of the Command Responses
 * @tparam IntermediateResponsesT The type of the Intermediate Responses (not
 * actually a pack, but rather an optional type parameter; only necessary to have
 * the correct signature of the Command Wrapper class template)
 */
template<CommandWrapperTemplate, typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
class ICommandManager<
    CommandWrapperT<ParametersT, ResponsesT, IntermediateResponsesT...>>
{
public:
    using Parameters = ParametersT;
    using Responses = ResponsesT;
    using ExecutorF = ExecutorFunc<CommandWrapperT, ParametersT, ResponsesT,
                                   IntermediateResponsesT...>;
    template<typename FeatureT>
    using ExecutorMemF = ExecutorMemFunc<FeatureT, CommandWrapperT, ParametersT,
                                         ResponsesT, IntermediateResponsesT...>;

    /**
     * @brief C'tor
     *
     * @param Feature A pointer to the SiLA Feature that this Command Manager
     * belongs to
     */
    explicit ICommandManager(ISiLAFeature* Feature);

    /**
     * @brief Get the Fully Qualified Identifier of this Command
     *
     * @return This Command's Fully Qualified Identifier
     */
    [[nodiscard]] CFullyQualifiedCommandID identifier() const;

    /**
     * @brief Set the executor function that contains the actual implementation
     * logic of this Observable Command
     *
     * @param Executor The function containing the implementation logic of this
     * Command. Must be in the form of
     * @code
     * MyObservableCommand_Responses myExecutor(const CObservableCommandWrapper*
     * Command);
     * @endcode
     * Inside your function you can set Execution Info to inform clients about the
     * progress of the execution by calling
     * @code
     * Command->setExecutionInfo(Progress, Remaining Time);
     * @endcode
     */
    void setExecutor(ExecutorF Executor);

    /**
     * @overload
     * @brief Set the executor function from a member function pointer
     *
     * @tparam SiLAFeature User defined SiLA Feature Implementation
     * @param Feature Pointer to the SiLA Feature Implementation
     * @param Executor The function used to validate the Command's parameters.
     * See above for more info on the required parameter and return type.
     */
    template<
        typename SiLAFeature,
        typename = std::enable_if_t<std::is_base_of_v<ISiLAFeature, SiLAFeature>>>
    void setExecutor(SiLAFeature* Feature, ExecutorMemF<SiLAFeature> Executor);

    /**
     * @brief Get the Executor function
     *
     * @return The Executor
     */
    [[nodiscard]] const ExecutorF executor() const;

private:
    /**
     * @brief Helper function to create the Fully Qualified Command Identifier
     * from the Feature's Fully Qualified Identifier and the @a ParametersT
     * template parameter
     *
     * @param Feature A pointer to the SiLA Feature that this Command Manager
     * belongs to
     * @return The generated Fully Qualified Command Identifier
     */
    static CFullyQualifiedCommandID identifierFromFeature(ISiLAFeature* Feature);

    const CFullyQualifiedCommandID
        m_Identifier;      ///< The Command's Fully Qualified Identifier
    ExecutorF m_Executor;  ///< Function containing the implementation logic
};

//============================================================================
template<CommandWrapperTemplate, typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
CFullyQualifiedCommandID ICommandManager<
    CommandWrapperT<ParametersT, ResponsesT, IntermediateResponsesT...>>::
    identifierFromFeature(ISiLAFeature* Feature)
{
    // Descriptor->full_name():
    // sila2.org.silastandard.examples.greetingprovider.v1.SayHello_Parameters
    // (1) chop off the trailing "_Parameters" (= 11 chars)
    const auto FullCommandName =
        QString::fromStdString(ParametersT::GetDescriptor()->full_name())
            .chopped(11);
    // (2) get the Command Identifier which starts at the index of the last '.'
    return {Feature->fullyQualifiedIdentifier(),
            FullCommandName.mid(FullCommandName.lastIndexOf('.') + 1)};
}

//============================================================================
template<CommandWrapperTemplate, typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
ICommandManager<
    CommandWrapperT<ParametersT, ResponsesT, IntermediateResponsesT...>>::
    ICommandManager(ISiLAFeature* Feature)
    : m_Identifier{identifierFromFeature(Feature)}
{}

//============================================================================
template<CommandWrapperTemplate, typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
CFullyQualifiedCommandID ICommandManager<CommandWrapperT<
    ParametersT, ResponsesT, IntermediateResponsesT...>>::identifier() const
{
    return m_Identifier;
}

//============================================================================
template<CommandWrapperTemplate, typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
void ICommandManager<
    CommandWrapperT<ParametersT, ResponsesT,
                    IntermediateResponsesT...>>::setExecutor(ExecutorF Executor)
{
    m_Executor = Executor;
}

//============================================================================
template<CommandWrapperTemplate, typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
template<typename SiLAFeatureT, typename>
void ICommandManager<
    CommandWrapperT<ParametersT, ResponsesT, IntermediateResponsesT...>>::
    setExecutor(SiLAFeatureT* Feature, ExecutorMemF<SiLAFeatureT> Executor)
{
    setExecutor(static_cast<ExecutorF>(
        std::bind(Executor, Feature, std::placeholders::_1)));
}

//============================================================================
template<CommandWrapperTemplate, typename ParametersT, typename ResponsesT,
         typename... IntermediateResponsesT>
const ExecutorFunc<CommandWrapperT, ParametersT, ResponsesT,
                   IntermediateResponsesT...>
ICommandManager<CommandWrapperT<ParametersT, ResponsesT,
                                IntermediateResponsesT...>>::executor() const
{
    return m_Executor;
}
}  // namespace SiLA2

#undef CommandWrapperTemplate

#endif  // ICOMMANDMANAGER_H
