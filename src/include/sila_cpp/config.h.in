/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   config.h
/// \author Dominik Schmidt <dev@dominik-schmidt.de>
/// \date   28.10.2020
/// \brief  Build time configurations of the sila_cpp library.
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#ifndef SILA_CPP_CONFIG_H
#define SILA_CPP_CONFIG_H

#define GRPC_VERSION_CHECK(major, minor, patch) ((major<<16)|(minor<<8)|(patch))
#define GRPC_VERSION GRPC_VERSION_CHECK(@gRPC_VERSION_MAJOR@, @gRPC_VERSION_MINOR@, @gRPC_VERSION_PATCH@)

#endif
