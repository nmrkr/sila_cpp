/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataTypeBasic.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2021
/// \brief  Declaration of the CDataTypeBasic class
//============================================================================
#ifndef CODEGEN_DATATYPEBASIC_H
#define CODEGEN_DATATYPEBASIC_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/IDataType.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CDataTypeBasic class represents a Basic SiLA 2 Data Type (e.g. a
 * String, Integer, ...)
 */
class SILA_CPP_EXPORT CDataTypeBasic final : public IDataType
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "Basic"; }

    /**
     * @brief Convert this Data Type to its @c QVariant representation
     *
     * @return A QVariant instance for this Data Type
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this Data Type with the values given in the @c QVariant
     * @a from
     *
     * @param from The QVariant instance that represents a Data Type
     *
     * @throws std::runtime_error if the initialization fails
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get the Identifier of this Basic Data Type
     *
     * @return The Basic Data Type's Identifier
     */
    [[nodiscard]] QString identifier() const { return m_Identifier; }

private:
    QString m_Identifier;
};

/**
 * @brief Overload for debugging CDataTypeBasics
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CDataTypeBasic& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CDataTypeBasic& rhs);
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_DATATYPEBASIC_H
