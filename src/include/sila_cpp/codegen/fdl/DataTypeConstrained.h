/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataTypeConstrained.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   28.01.2021
/// \brief  Declaration of the CDataTypeConstrained class
//============================================================================
#ifndef CODEGEN_DATATYPECONSTRAINED_H
#define CODEGEN_DATATYPECONSTRAINED_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/Constraint.h>
#include <sila_cpp/codegen/fdl/DataType.h>
#include <sila_cpp/codegen/fdl/IDataType.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CDataTypeConstrained class represents a Constrained SiLA 2 Data
 * Type
 */
class SILA_CPP_EXPORT CDataTypeConstrained final : public IDataType
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "Constrained"; }

    /**
     * @brief Convert this Data Type to its @c QVariant representation
     *
     * @return A QVariant instance for this Data Type
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this Data Type with the values given in the @c QVariant
     * @a from
     *
     * @param from The QVariant instance that represents a Data Type
     *
     * @throws std::runtime_error if the initialization fails
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get the underlying (unconstrained) Data Type of this Constrained
     * Data Type
     *
     * @return The Constrained Data Type's underlying Data Type
     */
    [[nodiscard]] CDataType dataType() const { return m_DataType; }

    /**
     * @brief Get the Constraints of this Constrained Data Type
     *
     * @return The Constrained Data Type's Constraints
     */
    [[nodiscard]] QList<CConstraint> constraints() const { return m_Constraints; }

    /**
     * @brief Given a SiLA 2 Data Type Identifier @a DataType this returns all
     * possible Constraint Types that are applicable to that Data Type. If the
     * Data Type happens to not have any applicable Constraints ths will return an
     * empty list.
     *
     * @param DataType The Data Type Identifier (e.g. "String", "Integer", "List",
     * ...) of the Data Type to return all Constraints for.
     * @return A list of all applicable Constraint Types for the given @a DataType
     * or an empty list if there are none
     */
    [[nodiscard]] static QList<CConstraint::Type> possibleConstraints(
        const QString& DataType)
    {
        return m_PossibleConstraints.value(DataType);
    }

private:
    QList<CConstraint> m_Constraints;
    CDataType m_DataType;
    static QHash<QString, QList<CConstraint::Type>>
        m_PossibleConstraints;  ///< maps a Data Type to its possible constraints
};

/**
 * @brief Overload for debugging CDataTypeConstrained
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CDataTypeConstrained& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CDataTypeConstrained& rhs);
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_DATATYPECONSTRAINED_H
