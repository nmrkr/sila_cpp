/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Constraint.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   28.01.2021
/// \brief  Declaration of the IConstraint class
//============================================================================
#ifndef CODEGEN_ICONSTRAINT_H
#define CODEGEN_ICONSTRAINT_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>
#include <sila_cpp/codegen/fdl/ISerializable.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The IConstraint class represents an abstract interface for Constraints
 * of a SiLA 2 Constrained Constraint
 */
class SILA_CPP_EXPORT IConstraint : public ISerializable
{
public:
    /**
     * @brief The Type enum defines all the different Constraint types specified
     * by the SiLA 2 standard
     */
    enum class Type
    {
        Length,
        MinimalLength,
        MaximalLength,
        Set,
        Pattern,
        MaximalExclusive,
        MaximalInclusive,
        MinimalExclusive,
        MinimalInclusive,
        Unit,
        ContentType,
        ElementCount,
        MinimalElementCount,
        MaximalElementCount,
        FullyQualifiedIdentifier,
        Schema,
        AllowedTypes,
    };

    /**
     * @brief Get the Type of this Constraint
     *
     * @return The Constraint's Type
     */
    [[nodiscard]] Type type() const { return m_Type; }

    /**
     * @brief Get the string representation of the given Type @a t
     *
     * @param t The Type to convert to a string
     * @return The string corresponding to the Type @a t
     */
    [[nodiscard]] static QString typeToString(Type t);

    /**
     * @brief Given the string representation @a String of a Type return the
     * corresponding Type
     *
     * @param String The string representation of a @c Type
     * @return The @c Type corresponding to the string @a String
     */
    [[nodiscard]] static Type stringToType(const QString& String);

protected:
    Type m_Type;
};

/**
 * @brief Overload for debugging IConstraint::Types
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const IConstraint::Type& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const IConstraint::Type& rhs);
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_ICONSTRAINT_H
