/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Constraint.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   29.01.2021
/// \brief  Declaration of the CConstraint class
//============================================================================
#ifndef CODEGEN_CONSTRAINT_H
#define CODEGEN_CONSTRAINT_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/IConstraint.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
class CConstraintLength;
class CConstraintMinimalLength;
class CConstraintMaximalLength;
class CConstraintElementCount;
class CConstraintMinimalElementCount;
class CConstraintMaximalElementCount;
class CConstraintMinimalExclusive;
class CConstraintMaximalExclusive;
class CConstraintMinimalInclusive;
class CConstraintMaximalInclusive;
class CConstraintFullyQualifiedID;
class CConstraintPattern;
class CConstraintSet;
class CConstraintAllowedTypes;
class CConstraintContentType;
class CConstraintSchema;
class CConstraintUnit;

/**
 * @brief The CConstraint class represents a compound of another SiLA 2 Constraint
 * to provide a uniform interface to classes derived from @c CSiLAElement
 */
class SILA_CPP_EXPORT CConstraint : public IConstraint
{
public:
    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] QString name() const override { return "Constraints"; }

    /**
     * @brief Convert this Constraint to its @c QVariant representation
     *
     * @return A QVariant instance for this Constraint
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this Constraint with the values given in the
     * @c QVariant @a from
     *
     * @param from The QVariant instance that represents a Constraint
     *
     * @throws std::runtime_error if the initialization fails
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get the actual Constraint
     *
     * @return The actual Constraint
     */
    [[nodiscard]] IConstraint* constraint() const { return m_Constraint; }

    /**
     * @brief Get the Length Constraint that this Constraint contains. If the
     * actual Constraint is not a Length Constraint this returns a default
     * constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Length Constraint
     */
    [[nodiscard]] CConstraintLength length() const;

    /**
     * @brief Get the Minimal Length Constraint that this Constraint contains. If
     * the actual Constraint is not a Minimal Length Constraint this returns a
     * default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Minimal Length Constraint
     */
    [[nodiscard]] CConstraintMinimalLength minimalLength() const;

    /**
     * @brief Get the Maximal Length Constraint that this Constraint contains. If
     * the actual Constraint is not a Maximal Length Constraint this returns a
     * default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Maximal Length Constraint
     */
    [[nodiscard]] CConstraintMaximalLength maximalLength() const;

    /**
     * @brief Get the ElementCount Constraint that this Constraint contains. If
     * the actual Constraint is not a ElementCount Constraint this returns a
     * default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual ElementCount Constraint
     */
    [[nodiscard]] CConstraintElementCount elementCount() const;

    /**
     * @brief Get the Minimal ElementCount Constraint that this Constraint
     * contains. If the actual Constraint is not a Minimal ElementCount Constraint
     * this returns a default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Minimal ElementCount Constraint
     */
    [[nodiscard]] CConstraintMinimalElementCount minimalElementCount() const;

    /**
     * @brief Get the Maximal ElementCount Constraint that this Constraint
     * contains. If the actual Constraint is not a Maximal ElementCount Constraint
     * this returns a default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Maximal ElementCount Constraint
     */
    [[nodiscard]] CConstraintMaximalElementCount maximalElementCount() const;

    /**
     * @brief Get the Minimal Exclusive Constraint that this Constraint
     * contains. If the actual Constraint is not a Minimal Exclusive Constraint
     * this returns a default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Minimal Exclusive Constraint
     */
    [[nodiscard]] CConstraintMinimalExclusive minimalExclusive() const;

    /**
     * @brief Get the Maximal Exclusive Constraint that this Constraint
     * contains. If the actual Constraint is not a Maximal Exclusive Constraint
     * this returns a default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Maximal Exclusive Constraint
     */
    [[nodiscard]] CConstraintMaximalExclusive maximalExclusive() const;

    /**
     * @brief Get the Minimal Inclusive Constraint that this Constraint
     * contains. If the actual Constraint is not a Minimal Inclusive Constraint
     * this returns a default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Minimal Inclusive Constraint
     */
    [[nodiscard]] CConstraintMinimalInclusive minimalInclusive() const;

    /**
     * @brief Get the Maximal Inclusive Constraint that this Constraint
     * contains. If the actual Constraint is not a Maximal Inclusive Constraint
     * this returns a default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Maximal Inclusive Constraint
     */
    [[nodiscard]] CConstraintMaximalInclusive maximalInclusive() const;

    /**
     * @brief Get the Fully Qualified Identifier Constraint that this Constraint
     * contains. If the actual Constraint is not a Fully Qualified Identifier
     * Constraint this returns a default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Fully Qualified Identifier Constraint
     */
    [[nodiscard]] CConstraintFullyQualifiedID fullyQualifiedIdentifier() const;

    /**
     * @brief Get the Pattern Constraint that this Constraint contains. If the
     * actual Constraint is not a Pattern Constraint this returns a default
     * constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Pattern Constraint
     */
    [[nodiscard]] CConstraintPattern pattern() const;

    /**
     * @brief Get the Set Constraint that this Constraint contains. If the
     * actual Constraint is not a Set Constraint this returns a default
     * constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Set Constraint
     */
    [[nodiscard]] CConstraintSet set() const;

    /**
     * @brief Get the Allowed Types Constraint that this Constraint contains. If
     * the actual Constraint is not an Allowed Types Constraint this returns a
     * default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Allowed Types Constraint
     */
    [[nodiscard]] CConstraintAllowedTypes allowedTypes() const;

    /**
     * @brief Get the Content Type Constraint that this Constraint contains. If
     * the actual Constraint is not an Content Type Constraint this returns a
     * default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Content Type Constraint
     */
    [[nodiscard]] CConstraintContentType contentType() const;

    /**
     * @brief Get the Schema Constraint that this Constraint contains. If
     * the actual Constraint is not an Schema Constraint this returns a
     * default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Schema Constraint
     */
    [[nodiscard]] CConstraintSchema schema() const;

    /**
     * @brief Get the Unit Constraint that this Constraint contains. If
     * the actual Constraint is not an Unit Constraint this returns a
     * default constructed object.
     * @todo[FM] Maybe throw instead?
     *
     * @return The actual Unit Constraint
     */
    [[nodiscard]] CConstraintUnit unit() const;

private:
    IConstraint* m_Constraint{nullptr};
};

/**
 * @brief Overload for debugging CConstraints
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CConstraint& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CConstraint& rhs);
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_CONSTRAINT_H
