/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAElement.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Declaration of the CSiLAElement class
//============================================================================
#ifndef CODEGEN_SILAELEMENT_H
#define CODEGEN_SILAELEMENT_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataType.h>
#include <sila_cpp/codegen/fdl/DefinedExecutionErrorIdentifier.h>
#include <sila_cpp/codegen/fdl/ISerializable.h>
#include <sila_cpp/global.h>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The CSiLAElement class represents a SiLA Element from a Feature
 * Definition that has an Identifier, a Display Name and a Description. This
 * includes e.g. a Feature, a Command, a Property, a Defined Execution Error, a
 * Data Type Definition <b>but not a Data Type</b>. Data Types are represented by
 * the @c CSiLADataType class.
 */
class SILA_CPP_EXPORT CSiLAElement : public ISerializable
{
public:
    // rule of 5
    CSiLAElement() = default;
    CSiLAElement(const CSiLAElement&) = default;
    CSiLAElement& operator=(const CSiLAElement&) = default;
    // disable moving to prevent having a move c'tor/operator that can be reached
    // through multiple paths (-Wvirtual-move-assign)
    CSiLAElement(CSiLAElement&&) = delete;
    CSiLAElement& operator=(CSiLAElement&&) = delete;

    /**
     * @brief Convert this object to its @c QVariant representation
     *
     * @return A QVariant instance for this object
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this object with the values given in the @c QVariant
     * @a from
     *
     * @param from The QVariant instance that represents an object of this class
     *
     * @throws std::runtime_error if the initialization fails
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get the element's Identifier
     *
     * @return The Identifier of this element
     */
    [[nodiscard]] QString identifier() const { return m_Identifier; }

    /**
     * @brief Get the element's Display Name
     *
     * @return The Display Name of this element
     */
    [[nodiscard]] QString displayName() const { return m_DisplayName; }

    /**
     * @brief Get the element's Description
     *
     * @return The Description of this element
     */
    [[nodiscard]] QString description() const { return m_Description; }

protected:
    QString m_Identifier;
    QString m_DisplayName;
    QString m_Description;
};

/**
 * @brief The CSiLAElementWithDataType class is a convenience class for SiLA
 * Elements in a FDL Definition that can also have a Data Type (e.g.
 * Properties, Parameters, Responses)
 */
class SILA_CPP_EXPORT CSiLAElementWithDataType : virtual public CSiLAElement
{
public:
    /**
     * @brief Convert this object to its @c QVariant representation
     *
     * @return A QVariant instance for this object
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this object with the values given in the @c QVariant
     * @a from
     *
     * @param from The QVariant instance that represents an object of this class
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get the Data Type of this Element
     *
     * @return The Element's Data Type
     */
    [[nodiscard]] CDataType dataType() const { return m_DataType; }

protected:
    CDataType m_DataType;
};

/**
 * @brief The CSiLAElementWithErrors class is a convenience class for SiLA
 * Elements in a FDL Definition that can also have Defined Execution Errors (e.g.
 * Commands, Properties, Metadata)
 */
class SILA_CPP_EXPORT CSiLAElementWithErrors : virtual public CSiLAElement
{
public:
    /**
     * @brief Convert this object to its @c QVariant representation
     *
     * @return A QVariant instance for this object
     */
    [[nodiscard]] QVariant toVariant() const override;

    /**
     * @brief Initializes this object with the values given in the @c QVariant
     * @a from
     *
     * @param from The QVariant instance that represents an object of this class
     */
    void fromVariant(const QVariant& from) override;

    /**
     * @brief Get all Defined Execution Error Identifiers of this Metadata
     *
     * @return The Metadata's Defined Execution Error Identifiers
     */
    [[nodiscard]] QList<CDefinedExecutionErrorIdentifier> errors() const
    {
        return m_Errors;
    }

    /**
     * @brief Get the <i>i</i>th Error Identifier of this Metadata
     *
     * @param i The index of the value to return
     * @return The Metadata's <i>i</i>th Error Identifier
     */
    [[nodiscard]] CDefinedExecutionErrorIdentifier error(int i) const
    {
        return m_Errors.at(i);
    }

protected:
    QList<CDefinedExecutionErrorIdentifier> m_Errors;
};

/**
 * @brief Overload for debugging CSiLAElements
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CSiLAElement& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CSiLAElement& rhs);

/**
 * @brief Overload for debugging CSiLAElementWithErrors
 */
SILA_CPP_EXPORT QDebug operator<<(QDebug dbg, const CSiLAElementWithErrors& rhs);
SILA_CPP_EXPORT std::ostream& operator<<(std::ostream& os,
                                         const CSiLAElementWithErrors& rhs);
}  // namespace SiLA2::codegen::fdl

#endif  // CODEGEN_SILAELEMENT_H
