/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Serializable.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Declaration of the ISerializable class
//============================================================================
#ifndef CODEGEN_SERIALIZABLE_H
#define CODEGEN_SERIALIZABLE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/global.h>

#include <QVariant>

#include <iostream>

namespace SiLA2::codegen::fdl
{
/**
 * @brief The ISerializable class is an abstract interface that needs to be
 * implemented by all classes that should be serializable through Qt's @c QVariant
 */
class SILA_CPP_EXPORT ISerializable
{
public:
    /**
     * @brief D'tor
     */
    virtual ~ISerializable() = default;

    /**
     * @brief Get the name of this object
     *
     * @return The object's name
     */
    [[nodiscard]] virtual QString name() const = 0;

    /**
     * @brief Convert this object to its @c QVariant representation
     *
     * @return A QVariant instance for this object
     */
    [[nodiscard]] virtual QVariant toVariant() const = 0;

    /**
     * @brief Initializes this object with the values given in the @c QVariant
     * @a from
     *
     * @param from The QVariant instance that represents an object of this class
     */
    virtual void fromVariant(const QVariant& from) = 0;

    /**
     * @brief Insert the given @c Container (might be a list or a map) @a Element
     * into the given @c QVariantMap @a Map if @a Element is not empty
     *
     * @tparam Container The type of the element to insert
     * @param Map The map to insert into
     * @param Key The key to use
     * @param Cont The element to insert if it's not empty
     */
    template<typename Container>
    static void insertIfNotEmpty(QVariantMap& Map, const QString& Key,
                                 const Container& Cont);
};

//============================================================================
template<typename Container>
void ISerializable::insertIfNotEmpty(QVariantMap& Map, const QString& Key,
                                     const Container& Cont)
{
    if (!Cont.isEmpty())
    {
        Map.insert(Key, Cont);
    }
}
}  // namespace SiLA2::codegen::fdl
#endif  // CODEGEN_SERIALIZABLE_H
