/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicMessageFactory.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   10.02.2021
/// \brief  Declaration of the CDynamicMessageFactory class
//============================================================================
#ifndef CODEGEN_DYNAMICMESSAGEFACTORY_H
#define CODEGEN_DYNAMICMESSAGEFACTORY_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/proto/ProtobufFile.h>
#include <sila_cpp/common/FullyQualifiedCommandID.h>
#include <sila_cpp/global.h>

#include <QString>

#include <google/protobuf/message.h>

#include <polymorphic_value.h>

namespace SiLA2::codegen::proto
{
/**
 * @brief The CDynamicMessageFactory class provides a way to create arbitrary
 * protobuf messages from the internal representation of a FDL description that
 * cannot be known at compile time. The DMF builds on top of the @c Descriptors
 * from the known protobuf messages from SiLAFramework.proto.
 */
class SILA_CPP_EXPORT CDynamicMessageFactory
{
protected:
    class PrivateImpl;
    using PrivateImplPtr =
        isocpp_p0201::polymorphic_value<CDynamicMessageFactory::PrivateImpl>;

public:
    /**
     * @brief C'tor
     * Prepares the internal @c DescriptorPool by filling it with the generated
     * @c Descriptors that are known at compile time
     */
    explicit CDynamicMessageFactory(PrivateImplPtr priv = {});

    /**
     * @brief Adds the given protobuf file @a Protobuf to the internal pool of
     * dynamic proto files.
     *
     * @param Protobuf The protobuf file to add
     */
    void addProtoFile(const CProtobufFile& Protobuf);

    /**
     * @brief This will populate the internal @c DescriptorPool with the
     * @c Descriptors for all protobuf code added through @b addProtoFile().
     */
    void buildDescriptors();

    /**
     * @brief Given the fully qualified name of a message get the default
     * constructed (prototype) protobuf @c Message. This will first call
     * @b buildDescriptors() to ensure that the internal @c DescriptorPool is
     * properly filled with all dynamic @c Descriptors.
     *
     * @note This Message shares some state with the internally created
     * @c Message. Thus, the CDynamicMessageFactory object used to create the
     * returned @c Message MUST outlive this @c Message.
     *
     * @param MessageName The fully qualified name of the protobuf message for
     * which to get the prototype @c Message
     * @return The dynamic @c Message for the given @a MessageName
     *
     * @throws std::runtime_error if there is no @c Message for the given
     * @a MessageName
     */
    [[nodiscard]] std::unique_ptr<google::protobuf::Message> getPrototype(
        const std::string& MessageName);

    /**
     * @brief Given a Fully Qualified Command Identifier @a CommandID get the
     * default constructed (prototype) protobuf @c Message for the Command
     * Parameter. This is simply a convenience wrapper around @c getPrototype
     * that does the conversion from a SiLA 2 Fully Qualified Identifier to a
     * gRPC fully qualified message name.
     *
     * @param CommandID The Command Identifier whose Parameter protobuf @c Message
     * should be returned
     *
     * @return The dynamic @c Message for the Command's Parameters
     *
     * @throws std::runtime_error if there is no such @c Message
     *
     * @sa getPrototype(const std::string&)
     */
    [[nodiscard]] std::unique_ptr<google::protobuf::Message>
    getParameterPrototype(const CFullyQualifiedCommandID& CommandID);

    /**
     * @brief Given a Fully Qualified Command Identifier @a CommandID get the
     * default constructed (prototype) protobuf @c Message for the Command
     * Response. This is simply a convenience wrapper around @c getPrototype
     * that does the conversion from a SiLA 2 Fully Qualified Identifier to a
     * gRPC fully qualified message name.
     *
     * @param CommandID The Command Identifier whose Response protobuf @c Message
     * should be returned
     * @param Type The type of the RPC method to construct the name for
     *
     * @return The dynamic @c Message for the Command's Response
     *
     * @throws std::runtime_error if there is no such @c Message
     *
     * @sa getPrototype(const std::string&)
     */
    [[nodiscard]] std::unique_ptr<google::protobuf::Message> getResponsePrototype(
        const CFullyQualifiedCommandID& CommandID,
        CFullyQualifiedCommandID::RPCType Type);

protected:
    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CDynamicMessageFactory)
};
}  // namespace SiLA2::codegen::proto

#endif  // CODEGEN_DYNAMICMESSAGEFACTORY_H
