/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ExecutionError.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   09.07.2020
/// \brief  Implementation of the CExecutionError, CDefinedExecutionError,
/// CUndefinedExecutionError classes
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/error_handling/ExecutionError.h>
#include <sila_cpp/framework/grpc/SiLAFramework.pb.h>

#include "SiLAError_p.h"

#include <string>

using namespace std;
using namespace isocpp_p0201;
using namespace sila2::org::silastandard;

namespace SiLA2
{
/**
 * @brief Private data of the CExecutionError class - pimpl
 */
class CExecutionError::PrivateImpl : public CSiLAError::PrivateImpl
{
public:
    /**
     * @brief C'tor for Defined Execution Error
     */
    PrivateImpl(string id, string msg)
        : CSiLAError::PrivateImpl{eErrorType::DEFINED_EXECUTION_ERROR, move(msg)},
          ErrorIdentifier{move(id)}
    {}

    /**
     * @brief C'tor for Undefined Execution Error
     */
    explicit PrivateImpl(string msg = "")
        : CSiLAError::PrivateImpl{eErrorType::UNDEFINED_EXECUTION_ERROR,
                                  move(msg)}
    {}

    /**
     * @brief Construct a SiLA Error protobuf message from the internal fields
     * @a Message and @a Identifier (if set). Note that the caller takes ownership
     * of the returned message.
     *
     * @return A pointer to the SiLA Error protobuf message
     */
    [[nodiscard]] unique_ptr<SiLAError> makeErrorMessage() const override;

    string ErrorIdentifier{};  ///< The error identifier of the Defined Error
};

///============================================================================
///                 CExecutionError::PrivateImpl implementation
///============================================================================
unique_ptr<SiLAError> CExecutionError::PrivateImpl::makeErrorMessage() const
{
    auto Error = CSiLAError::PrivateImpl::makeErrorMessage();
    switch (Type)
    {
    case eErrorType::DEFINED_EXECUTION_ERROR:
    {
        auto EError = new DefinedExecutionError;
        EError->set_erroridentifier(ErrorIdentifier);
        EError->set_message(Message);
        Error->set_allocated_definedexecutionerror(EError);
        break;
    }
    case eErrorType::UNDEFINED_EXECUTION_ERROR:
    {
        auto EError = new UndefinedExecutionError;
        EError->set_message(Message);
        Error->set_allocated_undefinedexecutionerror(EError);
        break;
    }
    default:
        qCCritical(sila_cpp_errors) << "Shouldn't get here";
        break;
    }
    return Error;
}

///============================================================================
///                        CExecutionError implementation
///============================================================================
CExecutionError::CExecutionError(string Message)
    : CSiLAError{make_polymorphic_value<CSiLAError::PrivateImpl, PrivateImpl>(
        move(Message))}
{}

//============================================================================
CExecutionError::CExecutionError(string Identifier, string Message)
    : CSiLAError{make_polymorphic_value<CSiLAError::PrivateImpl, PrivateImpl>(
        move(Identifier), move(Message))}
{}

//============================================================================
CExecutionError::CExecutionError(const CFullyQualifiedDefinedErrorID& Identifier,
                                 string Message)
    : CSiLAError{make_polymorphic_value<CSiLAError::PrivateImpl, PrivateImpl>(
        Identifier.toStdString(), move(Message))}
{}

//============================================================================
string CExecutionError::errorIdentifier() const
{
    PIMPL_D(const CExecutionError);
    return d->ErrorIdentifier;
}

//============================================================================
CExecutionError CExecutionError::fromErrorMessage(const SiLAError& Error)
{
    if (Error.has_definedexecutionerror())
    {
        const auto& DefinedError = Error.definedexecutionerror();
        return CExecutionError{CFullyQualifiedDefinedErrorID::fromStdString(
                                   DefinedError.erroridentifier()),
                               DefinedError.message()};
    }
    else if (Error.has_undefinedexecutionerror())
    {
        const auto& UndefinedError = Error.undefinedexecutionerror();
        return CExecutionError{UndefinedError.message()};
    }
    else
    {
        qCWarning(sila_cpp_errors)
            << "CExecutionError::fromErrorMessage was called with an Error "
               "Message that does not contain an Execution Error!";
        return CExecutionError{};
    }
}

//============================================================================
const char* CExecutionError::what() const noexcept
{
    PIMPL_D(const CExecutionError);
    static string ErrorString;
    switch (d->Type)
    {
    case eErrorType::DEFINED_EXECUTION_ERROR:
        ErrorString = "Defined Execution Error:\n\tIdentifier: "s
                      + d->ErrorIdentifier + "\n\tMessage: " + d->Message;
        break;
    case eErrorType::UNDEFINED_EXECUTION_ERROR:
        ErrorString = "Undefined Execution Error:\n\tMessage: " + d->Message;
        break;
    default:
        qCCritical(sila_cpp_errors) << "Shouldn't get here";
        break;
    }
    return ErrorString.c_str();
}

///============================================================================
///                    CDefinedExecutionError implementation
///============================================================================
SILA_CPP_DISABLE_WARNING_PUSH
SILA_CPP_DISABLE_WARNING_DEPRECATED
CDefinedExecutionError::CDefinedExecutionError(string Identifier, string Message)
    : CExecutionError{move(Identifier), move(Message)}
{}
SILA_CPP_DISABLE_WARNING_POP

//============================================================================
CDefinedExecutionError::CDefinedExecutionError(
    const CFullyQualifiedDefinedErrorID& Identifier, string Message)
    : CExecutionError{Identifier, move(Message)}
{}

///============================================================================
///                   CUndefinedExecutionError implementation
///============================================================================
CUndefinedExecutionError::CUndefinedExecutionError(string Message)
    : CExecutionError{move(Message)}
{}
}  // namespace SiLA2
