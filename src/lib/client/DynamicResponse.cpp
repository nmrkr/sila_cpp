/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicResponse.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   16.03.2021
/// \brief  Implementation of the CDynamicResponse class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicResponse.h>
#include <sila_cpp/client/DynamicStructure.h>
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>
#include <sila_cpp/common/FullyQualifiedCommandID.h>

#include "DynamicValue_p.h"

#include <utility>

using namespace std;
using namespace isocpp_p0201;
using namespace SiLA2::codegen;

namespace SiLA2
{
/**
 * @brief Private data of the CCustomDataType class - pimpl
 */
class CDynamicResponse::PrivateImpl : public CDynamicValue::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    explicit PrivateImpl(CDynamicResponse* parent,
                         const fdl::CResponse& ResponseFDL,
                         CFullyQualifiedCommandID CommandIdentifier,
                         shared_ptr<proto::CDynamicMessageFactory> DMF,
                         CDynamicFeatureStub* FeatureStub);

    PrivateImpl(const PrivateImpl& rhs) = default;
    PrivateImpl(PrivateImpl&& rhs) = default;

    const fdl::CResponse Response{};
    const string Identifier;
    const CFullyQualifiedCommandID CommandID;
    CDynamicFeatureStub* Feature;
};

//============================================================================
CDynamicResponse::PrivateImpl::PrivateImpl(
    CDynamicResponse* parent, const fdl::CResponse& ResponseFDL,
    CFullyQualifiedCommandID CommandIdentifier,
    shared_ptr<proto::CDynamicMessageFactory> DMF,
    CDynamicFeatureStub* FeatureStub)
    : CDynamicValue::PrivateImpl{parent, ResponseFDL.dataType(), move(DMF)},
      Response{ResponseFDL},
      Identifier{Response.identifier().toStdString()},
      CommandID{move(CommandIdentifier)},
      Feature{FeatureStub}
{}

///===========================================================================
CDynamicResponse::CDynamicResponse(google::protobuf::Message* Value,
                                   const fdl::CResponse& ResponseFDL,
                                   CFullyQualifiedCommandID CommandID,
                                   shared_ptr<proto::CDynamicMessageFactory> DMF,
                                   CDynamicFeatureStub* Feature)
    : CDynamicValue{
        make_polymorphic_value<CDynamicValue::PrivateImpl, PrivateImpl>(
            this, ResponseFDL, move(CommandID), move(DMF), Feature)}
{
    PIMPL_D(CDynamicResponse);

    if (!Value)
    {
        PrivateImpl::setEmptyNestedValue(Feature, *this);
        return;
    }

    namespace gp = google::protobuf;
    if (d->DataTypeType == eDataTypeType::LIST)
    {
        const auto Reflection = Value->GetReflection();
        const auto Field = Value->GetDescriptor()->FindFieldByName(d->Identifier);
        // can't directly iterate over `Ref` because that would require to
        // allocate an object of abstract `Message`
        const auto Ref =
            Reflection->GetRepeatedFieldRef<gp::Message>(*Value, Field);
        for (int i = 0; i < Ref.size(); ++i)
        {
            const auto& Val = Reflection->MutableRepeatedMessage(Value, Field, i);
            auto tmp = Val->New();
            tmp->CopyFrom(*Val);
            d->Values.emplace_back(tmp);
        }
    }
    else
    {
        d->Value.reset(Value->New());
        d->Value->CopyFrom(*Value);
    }
}

//============================================================================
CDynamicResponse::CDynamicResponse(const CDynamicResponse& rhs)
    : CDynamicValue{
        make_polymorphic_value<CDynamicValue::PrivateImpl, PrivateImpl>(
            *rhs.d_func())}
{
    d_ptr->q_ptr = this;
}

//============================================================================
CFullyQualifiedCommandResponseID CDynamicResponse::identifier() const
{
    PIMPL_D(const CDynamicResponse);
    return {d->CommandID, d->Identifier};
}

//============================================================================
QString CDynamicResponse::displayName() const
{
    PIMPL_D(const CDynamicResponse);
    return d->Response.displayName();
}

//============================================================================
QString CDynamicResponse::description() const
{
    PIMPL_D(const CDynamicResponse);
    return d->Response.description().simplified();
}

//============================================================================
CDynamicFeatureStub* CDynamicResponse::feature() const
{
    PIMPL_D(const CDynamicResponse);
    return d->Feature;
}

//============================================================================
CDynamicStructure CDynamicResponse::structure() const
{
    PIMPL_D(const CDynamicResponse);

    const auto DataTypeFDL = d->getUnderlyingDataType();
    if (DataTypeFDL.type() == fdl::IDataType::Type::Structure)
    {
        using RPCType = CFullyQualifiedCommandID::RPCType;
        return CDynamicStructure{
            DataTypeFDL.structure(),
            d->CommandID.responseMessageName(RPCType::RESULT), d->Identifier,
            d->DynamicMessageFactory, d->Feature};
    }
    return {};
}

//============================================================================
QString CDynamicResponse::prettyString() const
{
    PIMPL_D(const CDynamicResponse);
    return QString::fromStdString(d->Identifier) + '='
           + CDynamicValue::prettyString();
}

///===========================================================================
CDynamicResponseList::CDynamicResponseList(const QList<CDynamicResponse>& rhs)
    : QList{rhs}
{}

//============================================================================
QList<CFullyQualifiedCommandResponseID> CDynamicResponseList::identifiers() const
{
    QList<CFullyQualifiedCommandResponseID> List;
    List.reserve(size());
    transform(begin(), end(), back_inserter(List),
              [](const auto& Resp) { return Resp.identifier(); });
    return List;
}

//============================================================================
const CDynamicResponse& CDynamicResponseList::at(
    const CFullyQualifiedCommandResponseID& Identifier) const
{
    for (int i = 0; i < size(); ++i)
    {
        if (at(i).identifier() == Identifier)
        {
            return at(i);
        }
    }
    throw std::out_of_range{"No Dynamic Response named \""s
                            + Identifier.toStdString() + "\" in Response List!"};
}

//============================================================================
const CDynamicResponse& CDynamicResponseList::at(string_view Identifier) const
{
    for (int i = 0; i < size(); ++i)
    {
        if (at(i).identifier().identifier().toStdString() == Identifier)
        {
            return at(i);
        }
    }
    throw std::out_of_range{"No Dynamic Response named \""s + Identifier.data()
                            + "\" in Response List!"};
}

//============================================================================
CDynamicResponse& CDynamicResponseList::operator[](string_view Identifier)
{
    for (int i = 0; i < size(); ++i)
    {
        if (at(i).identifier().identifier().toStdString() == Identifier)
        {
            return QList::operator[](i);
        }
    }
    using namespace std::string_literals;
    throw std::out_of_range{"No Dynamic Response named \""s + Identifier.data()
                            + "\" in Response List!"};
}

//============================================================================
CDynamicResponse& CDynamicResponseList::operator[](
    const CFullyQualifiedCommandResponseID& Identifier)
{
    for (int i = 0; i < size(); ++i)
    {
        if (at(i).identifier() == Identifier)
        {
            return QList::operator[](i);
        }
    }
    using namespace std::string_literals;
    throw std::out_of_range{"No Dynamic Response named \""s
                            + Identifier.toStdString() + "\" in Response List!"};
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDynamicResponse& rhs)
{
    QDebugStateSaver s{dbg};
    dbg.nospace() << "CDynamicResponse(" << rhs.identifier() << '=';
    dbg << dynamic_cast<const SiLA2::CDynamicValue&>(rhs);
    return dbg << ')';
}

//============================================================================
ostream& operator<<(ostream& os, const CDynamicResponse& rhs)
{
    os << "CDynamicResponse(" << rhs.identifier() << '=';
    os << dynamic_cast<const SiLA2::CDynamicValue&>(rhs);
    return os << ')';
}
}  // namespace SiLA2
