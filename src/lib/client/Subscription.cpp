/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Subscription.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   21.07.2021
/// \brief  Implementation of the
///         CThreadPoolMaxThreadCountReachedError and CSubscriptionBase classes
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include "Subscription.h"

using namespace std;

namespace SiLA2
{
//============================================================================
CThreadPoolMaxThreadCountReachedError::CThreadPoolMaxThreadCountReachedError()
    : runtime_error{"The maximum thread count of " + to_string(maxThreadCount())
                    + " has been reached."}
{}

//============================================================================
int CThreadPoolMaxThreadCountReachedError::maxThreadCount()
{
    return CSubscriptionThreadPool::instance()->maxThreadCount();
}

///===========================================================================
weak_ptr<CSubscriptionThreadPool> CSubscriptionThreadPool::m_Instance{};

//============================================================================
shared_ptr<CSubscriptionThreadPool> CSubscriptionThreadPool::instance() noexcept
{
    auto Instance = m_Instance.lock();
    if (Q_UNLIKELY(!Instance))
    {
        Instance.reset(new CSubscriptionThreadPool);
        // This should be enough for most cases, hopefully...
        Instance->setMaxThreadCount(65535);
        m_Instance = Instance;
    }
    return Instance;
}

//============================================================================
bool CSubscriptionThreadPool::maxThreadCountReached()
{
    return activeThreadCount() >= maxThreadCount();
}
}  // namespace SiLA2
