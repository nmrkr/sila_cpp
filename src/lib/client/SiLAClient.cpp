/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAClient.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   08.01.2020
/// \brief  Implementation of the CSiLAClient class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/SiLAClient.h>
#include <sila_cpp/common/FullyQualifiedFeatureID.h>
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/common/ServerInformation.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/framework/error_handling/ClientError.h>
#include <sila_cpp/internal/HostInfo.h>

#include "SiLAClient_p.h"
#include "SiLAService.grpc.pb.h"

#include <QCommandLineParser>
#include <QSslSocket>

#include <grpcpp/grpcpp.h>

using namespace std;
using namespace isocpp_p0201;
using namespace sila2::org::silastandard::core::silaservice::v1;

namespace SiLA2
{
/**
 * @brief Setup the command line parser
 */
shared_ptr<QCommandLineParser> CSiLAClient::PrivateImpl::Parser = []() {
    auto Parser = make_shared<QCommandLineParser>();
    // default description if none is provided by the user
    Parser->setApplicationDescription("A SiLA2 client");
    Parser->addHelpOption();
    Parser->addVersionOption();
    static const auto DefaultAddress = CServerAddress{};
    Parser->addOptions(
        {{{"s", "server-host"},
          "The IP address or hostname of the SiLA server to connect "
          "with\n(default: "
              + DefaultAddress.ip() + ')',
          "ip-or-hostname",
          DefaultAddress.ip()},
         {{"p", "server-port"},
          "The port on which the SiLA server runs\n(default: "
              + DefaultAddress.port() + ')',
          "port",
          DefaultAddress.port()},
         {{"r", "root-ca"},
          "Root certificate authority that signed the \033[3mserver's\033[0m "
          "certificate. If the server has a self-signed certificate that wasn't "
          "signed by a CA then use the server's certificate here. If left empty "
          "the client will try to automatically fetch the server's certificate "
          "and use that as the root certificate.\n(default: empty)",
          "filename"},
         {{"c", "encryption-cert"},
          "SSL certificate filename, e.g. 'client-ssl.crt' Note that the client "
          "doesn't necessarily require a certificate.\n(default: empty)",
          "filename"},
         {{"k", "encryption-key"},
          "SSL key filename, e.g. 'client-ssl.key' Note that the client doesn't "
          "necessarily require a key.\n(default: empty)",
          "filename"},
         {{"i", "force-insecure"},
          "Forces the client to connect to the given SiLA server using insecure "
          "(i.e. unencrypted) communication. Note that this should not be used "
          "in production but for debugging only!"}});
    return Parser;
}();

//============================================================================
CSiLAClient::PrivateImpl::PrivateImpl(CSiLAClient* parent, CServerAddress Address)
    : q_ptr{parent}, ServerAddress{std::move(Address)}
{
    Q_UNUSED(SiLALogManager)
}

//============================================================================
void CSiLAClient::PrivateImpl::parseCommandLine()
{
    if (qApp && !QCoreApplication::arguments().empty())
    {
        Parser->process(QCoreApplication::arguments());

        // Server Address / Hostname
        if (Parser->isSet("server-host"))
        {
            ServerAddress.setIP(Parser->value("server-host"));
        }
        if (Parser->isSet("server-port"))
        {
            ServerAddress.setPort(Parser->value("server-port"));
        }

        // SSL Certificate

        const auto RootCA = Parser->value("root-ca");
        const auto Certificate = Parser->value("encryption-cert");
        const auto Key = Parser->value("encryption-key");
        const auto HasRootCA = RootCA.isEmpty();
        const auto HasCertificate = Certificate.isEmpty();
        const auto HasKey = Key.isEmpty();

        if (Parser->isSet("force-insecure")
            && (HasRootCA || HasCertificate || HasKey))
        {
            throw invalid_argument{
                "Cannot use '--insecure' in combination with '--root-ca', "
                "'--encryption-cert' or '--encryption-key'"};
        }

        ForceInsecure |= Parser->isSet("force-insecure");

        if (HasRootCA)
        {
            Credentials.setRootCAFromFile(RootCA);
        }
        if (HasCertificate)
        {
            Credentials.setCertificateFromFile(Certificate);
        }
        if (HasKey)
        {
            Credentials.setKeyFromFile(Key);
        }
    }
}

//============================================================================
void CSiLAClient::PrivateImpl::onPeerVerifyError(QSslSocket& Socket,
                                                 const QSslError& Error) const
{
    PIMPL_Q(const CSiLAClient);

    static QList<QSslError> IgnoredErrors;
    if (IgnoredErrors.contains(Error))
    {
        // we already got this error previously. i.e. it has already been ignored
        return;
    }

    const bool IsPrivateIP = internal::CHostInfo::isPrivateIP(ServerAddress.ip());
    if (IsPrivateIP)
    {
        qCInfo(sila_cpp_client)
            << "SSL peer verify error:" << Error
            << "Continuing handshake since the peer is in a private IP range.";
        IgnoredErrors << Error;
        Socket.ignoreSslErrors(IgnoredErrors);
        return;
    }
    qCWarning(sila_cpp_client) << "SSL peer verify error:" << Error;
    if (const auto Cert = Error.certificate(); !Cert.isNull())
    {
        static QSet<QSslCertificate> TrustedCertificates;
        if (TrustedCertificates.contains(Cert))
        {
            qCInfo(sila_cpp_client)
                << "Certificate has already been trusted. Continuing handshake.";
            IgnoredErrors << Error;
            Socket.ignoreSslErrors(IgnoredErrors);
            return;
        }
        else
        {
            qCInfo(sila_cpp_client) << "Checking if user trusts certificate...";
            if (q->acceptUntrustedServerCertificate(Cert))
            {
                qCInfo(sila_cpp_client)
                    << "Certificate has been trusted. Continuing handshake.";
                TrustedCertificates.insert(Cert);
                IgnoredErrors << Error;
                Socket.ignoreSslErrors(IgnoredErrors);
                return;
            }
            qCWarning(sila_cpp_client) << "User did not accept certificate.";
        }
        qCWarning(sila_cpp_client) << "Certificate is not trusted.";
    }
    throwCannotConnect("SSL peer verify error: "s
                       + Error.errorString().toStdString());
}

//============================================================================
void CSiLAClient::PrivateImpl::onSSLErrors(QSslSocket& Socket,
                                           const QList<QSslError>& Errors) const
{
    qCWarning(sila_cpp_client) << "SSL errors:" << Errors << &Socket;
    QList<QSslError> IgnoredErrors;
    const auto ErrorString = accumulate(
        cbegin(Errors), cend(Errors), "SSL errors: "s,
        [&IgnoredErrors, IP = ServerAddress.ip()](const string& Part,
                                                  const auto& Error) {
            switch (Error.error())
            {
            case QSslError::HostNameMismatch:
                // Hostname mismatch is a common "error" we can fix by using
                // SetSslTargetNameOverride for the `grpc::ChannelArgument` below
                qCDebug(sila_cpp_client)
                    << "Non-critical SSL error: The hostname" << IP
                    << "does not match any of the hostnames in the server "
                       "certificate";
                IgnoredErrors << Error;
                break;
            case QSslError::SelfSignedCertificate:
                qCDebug(sila_cpp_client)
                    << "Non-critical SSL error: Self-signed certificate";
                IgnoredErrors << Error;
                break;
            case QSslError::CertificateUntrusted:
                qCDebug(sila_cpp_client)
                    << "Non-critical SSL error: Untrusted certificate";
                IgnoredErrors << Error;
                break;
            default:
                // If there's anything else wrong with the certificate
                // gRPC won't like that. But it would still connect to the
                // server using insecure credentials and don't give us any
                // indication that there is something wrong - other than
                // spitting out all kinds of weird errors on the command
                // line. So, we exit here with the error message from
                // QSslSocket to give the user at least some kind of
                // valuable help.
                return Part + Error.errorString().toStdString()
                       + "(code: " + to_string(Error.error()) + ')';
            }
            return Part;
        });
    Socket.ignoreSslErrors(IgnoredErrors);

    if (IgnoredErrors.size() < Errors.size())
    {
        throwCannotConnect(ErrorString);
    }
}

//============================================================================
CSSLCredentials CSiLAClient::PrivateImpl::getServerCertificate() const
{
    if (!QSslSocket::supportsSsl())
    {
        qCCritical(sila_cpp_client) << "Could not load OpenSSL libraries! Secure "
                                       "connection not supported.";
        qCInfo(sila_cpp_client)
            << "Qt requires" << QSslSocket::sslLibraryBuildVersionString();
        return {};
    }

    qCDebug(sila_cpp_client) << "Trying to get the server's SSL certificate...";

    QSslSocket Socket;
    QObject::connect(&Socket, &QSslSocket::connected, []() {
        qCDebug(sila_cpp_client) << "Connected to SSL socket";
    });
    QObject::connect(&Socket, &QSslSocket::encrypted, []() {
        qCDebug(sila_cpp_client) << "SSL handshake successful";
    });
    QObject::connect(&Socket, &QSslSocket::stateChanged, [](const auto State) {
        qCDebug(sila_cpp_client) << "SSL socket state changed to" << State;
    });
    QObject::connect(&Socket, &QSslSocket::peerVerifyError,
                     [this, &Socket](const QSslError& Error) {
                         onPeerVerifyError(Socket, Error);
                     });
    QObject::connect(
        &Socket, qOverload<const QList<QSslError>&>(&QSslSocket::sslErrors),
        [this, &Socket](const auto& Errors) { onSSLErrors(Socket, Errors); });

    Socket.setPeerVerifyMode(QSslSocket::VerifyPeer);
    Socket.connectToHostEncrypted(
        ServerAddress.ip(), static_cast<quint16>(ServerAddress.port().toInt()));
    if (!Socket.waitForEncrypted(10000))
    {
        qCWarning(sila_cpp_client)
            << "Could not connect to Server due to SSL errors:"
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
            << Socket.sslErrors();
#else
            << Socket.sslHandshakeErrors();
#endif

        throwCannotConnect("SSL handshake failed or timed out");
    }
    if (Socket.peerCertificate().isNull())
    {
        throwCannotConnect("SSL certificate from server is empty");
    }
    return CSSLCredentials{Socket.peerCertificate().toPem().toStdString()};
}

//============================================================================
void CSiLAClient::PrivateImpl::connectToServer()
{
    if (ForceInsecure)
    {
        qCInfo(sila_cpp_client)
            << "Trying to connect to server using insecure communication...";
        Channel = grpc::CreateChannel(ServerAddress.toStdString(),
                                      grpc::InsecureChannelCredentials());
        if (Channel->WaitForConnected(gpr_time_from_seconds(1, GPR_TIMESPAN)))
        {
            qCInfo(sila_cpp_client)
                << "Successfully connected to SiLA server at" << ServerAddress
                << "using insecure communication";
            return;
        }
        throwCannotConnect("Could not establish a communication!");
    }
    else
    {
        // couldn't get root CA from c'tor argument nor the command line
        if (Credentials.rootCA().empty())
        {
            Credentials = getServerCertificate();
        }

        qCInfo(sila_cpp_client)
            << "Trying to connect to server using secure communication...";

        const auto ChannelCredentials = Credentials.toChannelCredentials();
        // reverse-lookup the hostname for the given IP
        using internal::CHostInfo;
        const auto TargetNameOverrides =
            vector{""s, "SiLA2"s,
                   CHostInfo::lookupHostname(ServerAddress.ip()).toStdString()};
        for (const auto& Name : TargetNameOverrides)
        {
            auto ChannelArgs = grpc::ChannelArguments{};
            if (!Name.empty())
            {
                qCDebug(sila_cpp_client)
                    << "Trying SSL target name override" << Name;
                ChannelArgs.SetSslTargetNameOverride(Name);
            }
            Channel = grpc::CreateCustomChannel(ServerAddress.toStdString(),
                                                ChannelCredentials, ChannelArgs);
            if (Channel->WaitForConnected(gpr_time_from_seconds(2, GPR_TIMESPAN)))
            {
                qCInfo(sila_cpp_client)
                    << "Successfully connected to SiLA server at" << ServerAddress
                    << "using secure communication";
                return;
            }
            else
            {
                qCDebug(sila_cpp_client) << "Could not connect to server"
                                         << (Name.empty() ? "without" : "with")
                                         << "SSL target name override" << Name;
            }
        }
        throwCannotConnect("Could not establish a secure communication!");
    }
}

//============================================================================
void CSiLAClient::PrivateImpl::throwCannotConnect(const string& Message) const
{
    throw runtime_error{
        "Could not connect to SiLA server at " + ServerAddress.toStdString()
        + "! "
        + (Message.empty() ? "Is the server running?" : "Reason: " + Message)};
}

//============================================================================
CSiLAClient::CSiLAClient(PrivateImplPtr priv) : d_ptr{std::move(priv)}
{}

///============================================================================
CSiLAClient::CSiLAClient(const CServerAddress& Address)
    : CSiLAClient{make_polymorphic_value<PrivateImpl>(this, Address)}
{}

//============================================================================
void CSiLAClient::connect()
{
    PIMPL_D(CSiLAClient);

    if (d->Channel)
    {
        return;
    }

    d->parseCommandLine();

    try
    {
        d->connectToServer();
    }
    catch (...)
    {
        d->Channel.reset();
        throw;
    }
    d->SiLAServiceStub = SiLAService::NewStub(d->Channel);

    d->ServerInfo.setServerName(Get_ServerName().value());
    d->ServerInfo.setVersion(Get_ServerVersion().value());
    d->ServerInfo.setDescription(Get_ServerDescription().value());
    d->ServerInfo.setServerUUID(Get_ServerUUID().value());

    qCInfo(sila_cpp_client).nospace()
        << "Connected to SiLA Server " << d->ServerInfo.serverName()
        << " (UUID: " << d->ServerInfo.serverUUID().toString() << ')'
        << " running in version " << d->ServerInfo.version() << ' ' << '\n'
        << " Service description: " << d->ServerInfo.description();
}

//============================================================================
void CSiLAClient::connect(const CSSLCredentials& Credentials)
{
    PIMPL_D(CSiLAClient);
    d->Credentials = Credentials;
    connect();
}

//============================================================================
void CSiLAClient::connectInsecure()
{
    PIMPL_D(CSiLAClient);
    d->ForceInsecure = true;
    qCWarning(sila_cpp_server)
        << "Connecting to SiLA 2 Server without encryption. This violates the "
           "SiLA 2 specification! Only use this in a safe environment.";
    connect();
    d->ForceInsecure = false;
}

//=============================================================================
shared_ptr<grpc::Channel> CSiLAClient::channel() const
{
    PIMPL_D(const CSiLAClient);
    return d->Channel;
}

//============================================================================
const CServerAddress& CSiLAClient::serverAddress() const
{
    PIMPL_D(const CSiLAClient);
    return d->ServerAddress;
}

//============================================================================
const CServerInformation& CSiLAClient::serverInformation() const
{
    PIMPL_D(const CSiLAClient);
    return d->ServerInfo;
}

//============================================================================
const CSSLCredentials& CSiLAClient::credentials() const
{
    PIMPL_D(const CSiLAClient);
    return d->Credentials;
}

//============================================================================
bool CSiLAClient::acceptUntrustedServerCertificate(
    const QSslCertificate& Certificate) const
{
    qCWarning(sila_cpp_client) << "Not accepting untrusted server certificate"
                               << Certificate.toText().toStdString();
    return false;
}

//============================================================================
shared_ptr<QCommandLineParser> CSiLAClient::commandLineParser()
{
    return PrivateImpl::Parser;
}

//============================================================================
CFullyQualifiedFeatureID CSiLAClient::silaServiceFeatureID()
{
    static CFullyQualifiedFeatureID FeatureID{"org.silastandard", "core",
                                              "SiLAService", "v1"};
    return FeatureID;
}

///============================================================================
CString CSiLAClient::GetFeatureDefinition(
    const CFullyQualifiedFeatureID& FeatureID)
{
    PIMPL_D(CSiLAClient);
    GetFeatureDefinition_Parameters Parameter;
    Parameter.set_allocated_featureidentifier(
        CString{FeatureID.toStdString()}.toProtoMessagePtr());
    grpc::ClientContext Context;
    static GetFeatureDefinition_Responses Response;

    qCDebug(sila_cpp_client)
        << "--- Calling Unobservable Command GetFeatureDefinition";
    const auto Status =
        d->SiLAServiceStub->GetFeatureDefinition(&Context, Parameter, &Response);
    qCDebug(sila_cpp_client) << "GetFeatureDefinition response:" << Response;

    SiLA2::throwOnError(Status);
    return Response.featuredefinition();
}

//=============================================================================
void CSiLAClient::SetServerName(const CString& ServerName)
{
    PIMPL_D(CSiLAClient);
    SetServerName_Parameters Parameter;
    Parameter.set_allocated_servername(ServerName.toProtoMessagePtr());
    grpc::ClientContext Context;
    static SetServerName_Responses Response;

    qCDebug(sila_cpp_client) << "--- Calling Unobservable Command SetServerName";
    const auto Status =
        d->SiLAServiceStub->SetServerName(&Context, Parameter, &Response);
    qCDebug(sila_cpp_client) << "SetServerName response:" << Response;

    SiLA2::throwOnError(Status);
}

//=============================================================================
CString CSiLAClient::Get_ServerType()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ServerType_Responses Response;

    qCDebug(sila_cpp_client) << "--- Requesting Unobservable Property ServerType";
    const auto Status =
        d->SiLAServiceStub->Get_ServerType(&Context, {}, &Response);
    qCDebug(sila_cpp_client) << "ServerType response:" << Response;

    SiLA2::throwOnError(Status);
    return Response.servertype();
}

//=============================================================================
CString CSiLAClient::Get_ServerUUID()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ServerUUID_Responses Response;

    qCDebug(sila_cpp_client) << "--- Requesting Unobservable Property ServerUUID";
    const auto Status =
        d->SiLAServiceStub->Get_ServerUUID(&Context, {}, &Response);
    qCDebug(sila_cpp_client) << "ServerUUID response:" << Response;

    SiLA2::throwOnError(Status);
    return Response.serveruuid();
}

//=============================================================================
CString CSiLAClient::Get_ServerDescription()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ServerDescription_Responses Response;

    qCDebug(sila_cpp_client)
        << "--- Requesting Unobservable Property ServerDescription";
    const auto Status =
        d->SiLAServiceStub->Get_ServerDescription(&Context, {}, &Response);
    qCDebug(sila_cpp_client) << "ServerDescription response:" << Response;

    SiLA2::throwOnError(Status);
    return Response.serverdescription();
}

//=============================================================================
CString CSiLAClient::Get_ServerVersion()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ServerVersion_Responses Response;

    qCDebug(sila_cpp_client)
        << "--- Requesting Unobservable Property ServerVersion";
    const auto Status =
        d->SiLAServiceStub->Get_ServerVersion(&Context, {}, &Response);
    qCDebug(sila_cpp_client) << "ServerVersion response:" << Response;

    SiLA2::throwOnError(Status);
    return Response.serverversion();
}

//=============================================================================
CString CSiLAClient::Get_ServerVendorURL()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ServerVendorURL_Responses Response;

    qCDebug(sila_cpp_client)
        << "--- Requesting Unobservable Property ServerVendorURL";
    const auto Status =
        d->SiLAServiceStub->Get_ServerVendorURL(&Context, {}, &Response);
    qCDebug(sila_cpp_client) << "ServerVendorURL response:" << Response;

    SiLA2::throwOnError(Status);
    return Response.servervendorurl();
}

//=============================================================================
CString CSiLAClient::Get_ServerName()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ServerName_Responses Response;

    qCDebug(sila_cpp_client) << "--- Requesting Unobservable Property ServerName";
    const auto Status =
        d->SiLAServiceStub->Get_ServerName(&Context, {}, &Response);
    qCDebug(sila_cpp_client) << "ServerName response:" << Response;

    SiLA2::throwOnError(Status);
    return Response.servername();
}

//=============================================================================
vector<CString> CSiLAClient::Get_ImplementedFeatures()
{
    PIMPL_D(CSiLAClient);
    grpc::ClientContext Context;
    static Get_ImplementedFeatures_Responses Response;

    qCDebug(sila_cpp_client) << "--- Requesting Unobservable Property "
                                "ImplementedFeatures";
    const auto Status =
        d->SiLAServiceStub->Get_ImplementedFeatures(&Context, {}, &Response);
    qCDebug(sila_cpp_client) << "ImplementedFeatures response:" << Response;

    SiLA2::throwOnError(Status);
    auto Result = vector<CString>{};
    for_each(begin(Response.implementedfeatures()),
             end(Response.implementedfeatures()),
             [&Result](const auto& el) { Result.emplace_back(el); });

    return Result;
}
}  // namespace SiLA2
