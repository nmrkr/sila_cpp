/*******************************************************************************
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA 2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 *****************************************************************************/

//============================================================================
/// \file   HeartbeatThread.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.10.2021
/// \brief  Implementation of the CHeartbeatThread class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include "HeartbeatThread.h"

#include <sila_cpp/client/SiLAClient.h>
#include <sila_cpp/common/utils.h>
#include <sila_cpp/framework/error_handling/SiLAError.h>

#include <grpcpp/alarm.h>
#include <grpcpp/grpcpp.h>

using namespace std;

/**
 * @brief Overload for debugging @c grpc_connectivity_state
 */
QDebug operator<<(QDebug dbg, grpc_connectivity_state rhs);

namespace SiLA2
{
/**
 * @brief The default number of seconds to wait between heartbeat checks
 */
static constexpr auto DEFAULT_TIMEOUT = 10;

/**
 * @brief Returns the number of seconds that the heartbeat thread waits before
 * checking if the server is still connected. The value is read from the
 * environment variable @b SILA_CPP_CLIENT_HEARTBEAT_TIMEOUT. If this variable is
 * not set or it's set to a negative value or zero the default value is used.
 *
 * @return The number of seconds to wait between heartbeat checks
 */
int timeoutFromEnvironment()
{
    bool ok;
    const auto EnvTimeout =
        qEnvironmentVariable("SILA_CPP_CLIENT_HEARTBEAT_TIMEOUT").toInt(&ok);
    return (ok && EnvTimeout > 0) ? EnvTimeout : DEFAULT_TIMEOUT;
}

//============================================================================
CHeartbeatThread::CHeartbeatThread(const shared_ptr<CSiLAClient>& Client)
    : m_Timeout{timeoutFromEnvironment()}, m_Client{Client}
{}

//============================================================================
bool CHeartbeatThread::beat()
{
    try
    {
        if (const auto Client = m_Client.lock(); isRunning() && Client)
        {
            Client->Get_ServerUUID();
            return true;
        }
    }
    catch (const CSiLAError&)
    {
        stop();
    }
    return false;
}

//============================================================================
void CHeartbeatThread::stop()
{
    requestInterruption();
}

//============================================================================
void CHeartbeatThread::run()
{
    auto SharedClient = m_Client.lock();
    Q_ASSERT(SharedClient);
    const auto ServerInfo = SharedClient->serverInformation();
    const auto Channel = SharedClient->channel();
    SharedClient.reset();

    auto State = Channel->GetState(false);

    // NOTE: We instruct the @a Channel to either notify us through @a CQ when the
    //  channel state changes or in case no state change happens notify us every
    //  second. That way, the call to @c CQ.Next(...) in the loop below won't
    //  block too long.
    grpc::CompletionQueue CQ;
    static const auto CheckChannelTimeout{gpr_time_from_seconds(1, GPR_TIMESPAN)};
    const auto CheckChannelStateTag = reinterpret_cast<void*>(1);
    Channel->NotifyOnStateChange(State, CheckChannelTimeout, &CQ,
                                 CheckChannelStateTag);
    // NOTE: Additionally, we want to perform the actual heartbeat (i.e. polling
    //  the ServerUUID) every @a m_Timeout seconds. Here it suffices to use an
    //  @c Alarm that will put @a HeartbeatTag into the @a CQ after @a m_Timeout
    //  seconds.
    grpc::Alarm Alarm;
    static const auto Timeout{gpr_time_from_seconds(m_Timeout, GPR_TIMESPAN)};
    const auto DoHeartbeatTag = reinterpret_cast<void*>(2);
    Alarm.Set(&CQ, Timeout, DoHeartbeatTag);

    void* Tag = nullptr;
    bool ok;
    while (CQ.Next(&Tag, &ok))
    {
        if (Tag == CheckChannelStateTag)
        {
            State = Channel->GetState(false);
            qCDebug(sila_cpp_heartbeat) << "Channel state" << State;

            if (State != GRPC_CHANNEL_READY || !m_Client.lock())
            {
                stop();
            }
            Channel->NotifyOnStateChange(State, CheckChannelTimeout, &CQ,
                                         CheckChannelStateTag);
        }
        else if (Tag == DoHeartbeatTag)
        {
            beat();
            Alarm.Set(&CQ, Timeout, DoHeartbeatTag);
        }

        if (isInterruptionRequested())
        {
            Alarm.Cancel();
            break;
        }
    }
    shutdownAndDrainCompletionQueue(CQ);
    qCDebug(sila_cpp_heartbeat) << "Final channel state" << State;
    qCInfo(sila_cpp_heartbeat)
        << "Heartbeat stopped for server" << ServerInfo.serverName();
}
}  // namespace SiLA2

//============================================================================
QDebug operator<<(QDebug dbg, grpc_connectivity_state rhs)
{
    switch (rhs)
    {
    case GRPC_CHANNEL_IDLE:
        return dbg << "IDLE";
    case GRPC_CHANNEL_CONNECTING:
        return dbg << "CONNECTING";
    case GRPC_CHANNEL_READY:
        return dbg << "READY";
    case GRPC_CHANNEL_TRANSIENT_FAILURE:
        return dbg << "TRANSIENT_FAILURE";
    case GRPC_CHANNEL_SHUTDOWN:
        return dbg << "SHUTDOWN";
    default:
        qCWarning(sila_cpp_internal)
            << "Unknown grpc_connectivity_state" << static_cast<int>(rhs);
        return dbg;
    }
}
