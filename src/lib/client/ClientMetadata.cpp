/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ClientMetadata.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.02.2021
/// \brief  Implementation of the CClientMetadata class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/ClientMetadata.h>
#include <sila_cpp/client/DynamicStructure.h>
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>
#include <sila_cpp/codegen/fdl/Metadata.h>
#include <sila_cpp/framework/error_handling/ClientError.h>

#include "DynamicCall.h"
#include "DynamicValue_p.h"

#include <SiLAFramework.pb.h>

#include <utility>

using namespace std;
using namespace isocpp_p0201;
using namespace SiLA2::codegen;
using sila2::org::silastandard::String;

namespace SiLA2
{
/**
 * @brief Private data of the CClientMetadata class - pimpl
 */
class CClientMetadata::PrivateImpl : public CDynamicValue::PrivateImpl
{
public:
    PrivateImpl(CClientMetadata* parent, CFullyQualifiedMetadataID MetadataID,
                const fdl::CMetadata& MetadataFDL,
                shared_ptr<grpc::Channel> SharedChannel,
                shared_ptr<proto::CDynamicMessageFactory> DMF,
                CDynamicFeatureStub* FeatureStub);

    PrivateImpl(const PrivateImpl& rhs) = default;
    PrivateImpl(PrivateImpl&& rhs) = default;

    void getAffectedCalls();

    const fdl::CMetadata Metadata;
    const CFullyQualifiedMetadataID Identifier;
    const shared_ptr<grpc::Channel> Channel;
    QStringList AffectedCalls;
    CDynamicFeatureStub* Feature;
};

//============================================================================
CClientMetadata::PrivateImpl::PrivateImpl(
    CClientMetadata* parent, CFullyQualifiedMetadataID MetadataID,
    const fdl::CMetadata& MetadataFDL, shared_ptr<grpc::Channel> SharedChannel,
    shared_ptr<proto::CDynamicMessageFactory> DMF,
    CDynamicFeatureStub* FeatureStub)
    : CDynamicValue::PrivateImpl{parent, MetadataFDL.dataType(), move(DMF)},
      Metadata{MetadataFDL},
      Identifier{move(MetadataID)},
      Channel{move(SharedChannel)},
      Feature{FeatureStub}
{}

//============================================================================
void CClientMetadata::PrivateImpl::getAffectedCalls()
{
    auto Responses =
        DynamicMessageFactory->getPrototype(Identifier.responseMessageName());
    auto Status = CDynamicCall::call(
        Channel, Identifier.toMethodName(),
        DynamicMessageFactory->getPrototype(Identifier.parameterMessageName()),
        Responses);

    throwOnError(Status);
    qCDebug(sila_cpp_client) << *Responses;

    const auto AffectedCallsField = Responses->GetDescriptor()->field(0);
    const auto Reflection = Responses->GetReflection();
    auto RepeatedField =
        Reflection->GetRepeatedFieldRef<String>(*Responses, AffectedCallsField);
    // NOTE: We cannot use @c std::transform() here, unfortunately. The individual
    //   values we would get can't be used to @c CopyFrom() and we can't access
    //   the @c value() without getting a segmentation fault. Don't really know
    //   why this is happening... The only possible thing is to get every value
    //   manually from the Reflection.
    for (int i = 0; i < RepeatedField.size(); ++i)
    {
        auto Field = String::default_instance().New();
        Field->CopyFrom(
            Reflection->GetRepeatedMessage(*Responses, AffectedCallsField, i));
        AffectedCalls.push_back(QString::fromStdString(Field->value()));
    }
    qCDebug(sila_cpp_client) << Identifier << "affects" << AffectedCalls;
}

///===========================================================================
CClientMetadata::CClientMetadata(CFullyQualifiedMetadataID Identifier,
                                 const fdl::CMetadata& MetadataFDL,
                                 shared_ptr<grpc::Channel> Channel,
                                 shared_ptr<proto::CDynamicMessageFactory> DMF,
                                 CDynamicFeatureStub* Feature)
    : CDynamicValue{
        make_polymorphic_value<CDynamicValue::PrivateImpl, PrivateImpl>(
            this, move(Identifier), MetadataFDL, move(Channel), move(DMF),
            Feature)}
{
    PIMPL_D(CClientMetadata);
    d->getAffectedCalls();
    PrivateImpl::setEmptyNestedValue(Feature, *this);
}

//============================================================================
CClientMetadata::CClientMetadata(const CClientMetadata& rhs)
    : CDynamicValue{
        make_polymorphic_value<CDynamicValue::PrivateImpl, PrivateImpl>(
            *rhs.d_func())}
{
    d_ptr->q_ptr = this;
}

//============================================================================
CFullyQualifiedMetadataID CClientMetadata::identifier() const
{
    PIMPL_D(const CClientMetadata);
    return d->Identifier;
}

//============================================================================
QString CClientMetadata::displayName() const
{
    PIMPL_D(const CClientMetadata);
    return d->Metadata.displayName();
}

//============================================================================
QString CClientMetadata::description() const
{
    PIMPL_D(const CClientMetadata);
    return d->Metadata.description().simplified();
}

//============================================================================
QStringList CClientMetadata::affectedCalls() const
{
    PIMPL_D(const CClientMetadata);
    return d->AffectedCalls;
}

//============================================================================
CDynamicFeatureStub* CClientMetadata::feature() const
{
    PIMPL_D(const CClientMetadata);
    return d->Feature;
}

//============================================================================
string CClientMetadata::toSerializedMessage() const
{
    PIMPL_D(const CClientMetadata);
    if (auto Value = getCopy(); Value)
    {
        auto Message =
            d->DynamicMessageFactory->getPrototype(d->Identifier.messageName());
        const auto FieldDescriptor = Message->GetDescriptor()->field(0);
        const auto Reflection = Message->GetReflection();
        Reflection->SetAllocatedMessage(Message.get(), Value.release(),
                                        FieldDescriptor);

        return Message->SerializeAsString();
    }
    return "";
}

//============================================================================
google::protobuf::Message* CClientMetadata::toProtoMessagePtr() const
{
    PIMPL_D(const CClientMetadata);
    return d->toProtoMessagePtr(d->Identifier.messageName(),
                                d->Identifier.identifier().toStdString());
}

//============================================================================
CDynamicStructure CClientMetadata::structure() const
{
    PIMPL_D(const CClientMetadata);

    const auto DataTypeFDL = d->getUnderlyingDataType();
    if (DataTypeFDL.type() == fdl::IDataType::Type::Structure)
    {
        return CDynamicStructure{DataTypeFDL.structure(),
                                 d->Identifier.parameterMessageName(),
                                 d->Identifier.identifier().toStdString(),
                                 d->DynamicMessageFactory, d->Feature};
    }
    return {};
}

//============================================================================
QString CClientMetadata::prettyString() const
{
    PIMPL_D(const CClientMetadata);
    return d->Identifier.toString() + '=' + CDynamicValue::prettyString();
}

///===========================================================================
QList<CFullyQualifiedMetadataID> CClientMetadataList::identifiers() const
{
    QList<CFullyQualifiedMetadataID> List;
    List.reserve(size());
    transform(begin(), end(), back_inserter(List),
              [](const auto& Metadata) { return Metadata.identifier(); });
    return List;
}

//============================================================================
CClientMetadata& CClientMetadataList::operator[](
    const CFullyQualifiedMetadataID& Identifier)
{
    for (int i = 0; i < size(); ++i)
    {
        if (at(i).identifier() == Identifier)
        {
            return QList::operator[](i);
        }
    }
    throw std::out_of_range{"No Metadata with Identifier \""s
                            + Identifier.toStdString() + "\" in Metadata List!"};
}

//============================================================================
multimap<string, string> CClientMetadataList::toMultiMap() const
{
    multimap<string, string> Result;
    transform(begin(), end(), inserter(Result, Result.begin()),
              [](const auto& Metadata) {
                  return pair{Metadata.identifier().toHeaderName(),
                              Metadata.toSerializedMessage()};
              });
    return Result;
}

//============================================================================
QDebug operator<<(QDebug dbg, const CClientMetadata& rhs)
{
    QDebugStateSaver s{dbg};
    dbg.nospace() << "CClientMetadata(" << rhs.identifier() << '=';
    dbg << dynamic_cast<const SiLA2::CDynamicValue&>(rhs);
    return dbg << ')';
}

//============================================================================
ostream& operator<<(ostream& os, const CClientMetadata& rhs)
{
    os << "CClientMetadata(" << rhs.identifier() << '=';
    os << dynamic_cast<const SiLA2::CDynamicValue&>(rhs);
    return os << ')';
}
}  // namespace SiLA2
