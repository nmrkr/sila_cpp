/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicParameter.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.02.2021
/// \brief  Implementation of the CDynamicParameter class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicFeatureStub.h>
#include <sila_cpp/client/DynamicParameter.h>
#include <sila_cpp/client/DynamicStructure.h>
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>
#include <sila_cpp/common/FullyQualifiedCommandID.h>

#include "DynamicValue_p.h"

#include <utility>

using namespace std;
using namespace isocpp_p0201;
using namespace SiLA2::codegen;

namespace SiLA2
{
/**
 * @brief Private data of the CCustomDataType class - pimpl
 */
class CDynamicParameter::PrivateImpl : public CDynamicValue::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    PrivateImpl(CDynamicParameter* parent, const fdl::CParameter& ParameterFDL,
                CFullyQualifiedCommandID CommandIdentifier,
                shared_ptr<proto::CDynamicMessageFactory> DMF,
                CDynamicFeatureStub* FeatureStub);

    PrivateImpl(const PrivateImpl& rhs) = default;
    PrivateImpl(PrivateImpl&& rhs) = default;

    const fdl::CParameter Parameter{};
    const std::string Identifier;
    const CFullyQualifiedCommandID CommandID;
    CDynamicFeatureStub* Feature{nullptr};
};

//============================================================================
CDynamicParameter::PrivateImpl::PrivateImpl(
    CDynamicParameter* parent, const fdl::CParameter& ParameterFDL,
    CFullyQualifiedCommandID CommandIdentifier,
    shared_ptr<proto::CDynamicMessageFactory> DMF,
    CDynamicFeatureStub* FeatureStub)
    : CDynamicValue::PrivateImpl{parent, ParameterFDL.dataType(), move(DMF)},
      Parameter{ParameterFDL},
      Identifier{Parameter.identifier().toStdString()},
      CommandID{move(CommandIdentifier)},
      Feature{FeatureStub}
{}

///===========================================================================
CDynamicParameter::CDynamicParameter(
    const fdl::CParameter& ParameterFDL, CFullyQualifiedCommandID CommandID,
    shared_ptr<proto::CDynamicMessageFactory> DMF, CDynamicFeatureStub* Feature)
    : CDynamicValue{
        make_polymorphic_value<CDynamicValue::PrivateImpl, PrivateImpl>(
            this, ParameterFDL, move(CommandID), move(DMF), Feature)}
{
    PrivateImpl::setEmptyNestedValue(Feature, *this);
}

//============================================================================
CDynamicParameter::CDynamicParameter(const CDynamicParameter& rhs)
    : CDynamicValue{
        make_polymorphic_value<CDynamicValue::PrivateImpl, PrivateImpl>(
            *rhs.d_func())}
{
    d_ptr->q_ptr = this;
}

//============================================================================
CFullyQualifiedCommandParameterID CDynamicParameter::identifier() const
{
    PIMPL_D(const CDynamicParameter);
    return {d->CommandID, d->Identifier};
}

//============================================================================
QString CDynamicParameter::displayName() const
{
    PIMPL_D(const CDynamicParameter);
    return d->Parameter.displayName();
}

//============================================================================
QString CDynamicParameter::description() const
{
    PIMPL_D(const CDynamicParameter);
    return d->Parameter.description().simplified();
}

//============================================================================
CDynamicFeatureStub* CDynamicParameter::feature() const
{
    PIMPL_D(const CDynamicParameter);
    return d->Feature;
}

//============================================================================
google::protobuf::Message* CDynamicParameter::toProtoMessagePtr() const
{
    PIMPL_D(const CDynamicParameter);
    return d->toProtoMessagePtr(d->CommandID.parameterMessageName(),
                                d->Identifier);
}

//============================================================================
CDynamicStructure CDynamicParameter::structure() const
{
    PIMPL_D(const CDynamicParameter);

    const auto DataTypeFDL = d->getUnderlyingDataType();
    if (DataTypeFDL.type() == fdl::IDataType::Type::Structure)
    {
        return CDynamicStructure{
            DataTypeFDL.structure(), d->CommandID.parameterMessageName(),
            d->Identifier, d->DynamicMessageFactory, d->Feature};
    }
    return {};
}

//============================================================================
QString CDynamicParameter::prettyString() const
{
    PIMPL_D(const CDynamicParameter);
    return QString::fromStdString(d->Identifier) + '='
           + CDynamicValue::prettyString();
}

///===========================================================================
CDynamicParameterList::CDynamicParameterList(const QList<CDynamicParameter>& rhs)
    : QList{rhs}
{}

//============================================================================
QList<CFullyQualifiedCommandParameterID> CDynamicParameterList::identifiers() const
{
    QList<CFullyQualifiedCommandParameterID> List;
    List.reserve(size());
    transform(begin(), end(), back_inserter(List),
              [](const auto& Param) { return Param.identifier(); });
    return List;
}

//============================================================================
const CDynamicParameter& CDynamicParameterList::at(
    const CFullyQualifiedCommandParameterID& Identifier) const
{
    for (int i = 0; i < size(); ++i)
    {
        if (at(i).identifier() == Identifier)
        {
            return at(i);
        }
    }
    throw std::out_of_range{"No Dynamic Parameter named \""s
                            + Identifier.toStdString() + "\" in Parameter List!"};
}

//============================================================================
const CDynamicParameter& CDynamicParameterList::at(string_view Identifier) const
{
    for (int i = 0; i < size(); ++i)
    {
        if (at(i).identifier().identifier().toStdString() == Identifier)
        {
            return at(i);
        }
    }
    throw std::out_of_range{"No Dynamic Parameter named \""s + Identifier.data()
                            + "\" in Parameter List!"};
}

//============================================================================
CDynamicParameter& CDynamicParameterList::operator[](
    const CFullyQualifiedCommandParameterID& Identifier)
{
    for (int i = 0; i < size(); ++i)
    {
        if (at(i).identifier() == Identifier)
        {
            return operator[](i);
        }
    }
    throw std::out_of_range{"No Dynamic Parameter named \""s
                            + Identifier.toStdString() + "\" in Parameter List!"};
}

//============================================================================
CDynamicParameter& CDynamicParameterList::operator[](string_view Identifier)
{
    for (int i = 0; i < size(); ++i)
    {
        if (at(i).identifier().identifier().toStdString() == Identifier)
        {
            return operator[](i);
        }
    }
    throw std::out_of_range{"No Dynamic Parameter named \""s + Identifier.data()
                            + "\" in Parameter List!"};
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDynamicParameter& rhs)
{
    QDebugStateSaver s{dbg};
    dbg.nospace() << "CDynamicParameter(" << rhs.identifier() << '=';
    dbg << dynamic_cast<const SiLA2::CDynamicValue&>(rhs);
    return dbg << ')';
}

//============================================================================
ostream& operator<<(ostream& os, const CDynamicParameter& rhs)
{
    os << "CDynamicParameter(" << rhs.identifier() << '=';
    os << dynamic_cast<const SiLA2::CDynamicValue&>(rhs);
    return os << ')';
}
}  // namespace SiLA2
