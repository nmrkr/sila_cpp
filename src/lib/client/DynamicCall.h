/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicCall.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   12.02.2021
/// \brief  Declaration of the CDynamicCall class
//============================================================================
#ifndef DYNAMICCALL_H
#define DYNAMICCALL_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/config.h>
#include <sila_cpp/global.h>

#include <QException>

#include <grpcpp/impl/codegen/status.h>

#include <polymorphic_value.h>

#include <map>

//============================================================================
//                            FORWARD DECLARATIONS
//============================================================================
#if GRPC_VERSION >= GRPC_VERSION_CHECK(1, 32, 0)
namespace grpc
{
class Channel;
}  // namespace grpc
#else
namespace grpc_impl
{
class Channel;
}  // namespace grpc_impl
namespace grpc
{
typedef grpc_impl::Channel Channel;
}  // namespace grpc
#endif

namespace google::protobuf
{
class Message;
}  // namespace google::protobuf

namespace SiLA2
{
/**
 * @brief The CDynamicCall class handles the sending an receiving of generic
 * messages given the name of a remote method. It acts as some kind of bridge
 * between a SiLA 2 Dynamic Feature Stub and the underlying gRPC framework. It has
 * been adapted from
 * https://github.com/grpc/grpc/blob/91627677fd2920b07fb28e36d96a4b9aa5971860/test/cpp/util/cli_call.h
 */
class CDynamicCall
{
    class PrivateImpl;
    using PrivateImplPtr = isocpp_p0201::polymorphic_value<PrivateImpl>;

public:
    using MetadataContainer = std::multimap<std::string, std::string>;

    /**
     * @brief Construct a new Dynamic Call that uses @a Channel to call the method
     * with the name @a MethodName and the given @a Metadata on the server.
     *
     * You can subsequently call @b write() to send messages to the server and
     * @b read() to read the server's reply. Once you're done writing call
     * @b writesDone() to indicate this. If you've sent and received all messages
     * call @c finish() to obtain the final @c Status of the RPC.
     *
     * @param Channel The channel used to communicate with the server
     * @param MethodName The fully qualified gRPC method name of the RPC to call
     * @param Metadata The metadata to add to the call
     *
     * @sa write()
     * @sa read()
     * @sa writesDone()
     * @sa finish()
     */
    CDynamicCall(std::shared_ptr<grpc::Channel> Channel, std::string MethodName,
                 const std::multimap<std::string, std::string>& Metadata = {});

    /**
     * @brief Performs a unary Dynamic Call to the method with the name
     * @a MethodName a by sending @a Request along with @a Metadata through
     * @a Channel to the server and receiving @a Response as well as
     * @a InitialMetadata and @a TrailingMetadata
     *
     * @param Channel The channel used to communicate with the server
     * @param MethodName The fully qualified gRPC method name of the RPC to call
     * @param Request The message to send
     * @param Response A reference to the message in which to store the server's
     * response (overwrites any existing content)
     * @param Metadata The metadata to add to the call
     * @param InitialMetadata A pointer to the received initial metadata
     * @param TrailingMetadata A pointer to the received trailing metadata
     * @return A @c grpc::Status indicating whether the RPC was successful or not
     */
    static grpc::Status call(
        std::shared_ptr<grpc::Channel> Channel, std::string MethodName,
        const std::unique_ptr<google::protobuf::Message>& Request,
        std::unique_ptr<google::protobuf::Message>& Response,
        const MetadataContainer& Metadata = {},
        MetadataContainer* InitialMetadata = nullptr,
        MetadataContainer* TrailingMetadata = nullptr);

    /**
     * @brief Sends the given message @a Message to the server
     *
     * @param Message The message to send
     * @returns true, if the write was successful, false otherwise
     */
    bool write(const std::unique_ptr<google::protobuf::Message>& Message);

    /**
     * @brief Indicates to the server that all messages have been sent and that
     * there will be no more calls to @b write()
     *
     * @returns true, if the write was successful, false otherwise
     */
    bool writesDone();

    /**
     * @brief Receives a message along with the initial metadata from the server
     * and stores it in @a Message and @a Metadata respectively.
     * @a Message must have the correct type.
     *
     * @param Message A reference to the message in which to store the server's
     * response (overwrites any existing content)
     * @param InitialMetadata A pointer to the initial metadata that has been
     * received from the server
     * @returns true, if the read is successful, false otherwise (@a Message and
     * @a Metadata remain unchanged)
     */
    bool read(std::unique_ptr<google::protobuf::Message>& Message,
              MetadataContainer* InitialMetadata = nullptr);

    /**
     * @brief Cancels the Call
     */
    void cancel();

    /**
     * @brief Finishes the RPC
     *
     * @param TrailingMetadata A pointer to the trailing metadata that has been
     * received from the server
     * @return A @c grpc::Status indicating whether the RPC was successful or not
     */
    grpc::Status finish(MetadataContainer* TrailingMetadata = nullptr);

    /**
     * @brief Returns the number of seconds after which call operation (read,
     * write, ...) is considered "timed out"
     *
     * The default is 5 seconds.
     *
     * @return The number of seconds after which a call operation will fail due to
     * a "timeout"
     */
    [[nodiscard]] int timeoutSeconds() const;

    /**
     * @brief Sets the number of seconds after which a call operation (read,
     * write, ...) should be considered as "timed out" to avoid blocking the
     * caller potentially forever.
     *
     * @param Seconds The number of seconds after which an operation will fail due
     * to a "timeout". If this is 0 or negative it means that the call will block
     * forever.
     */
    void setTimeoutSeconds(int Seconds);

    /**
     * @brief Returns the fully qualified gRPC method name of the RPC that is
     * performed by this call
     *
     * @return The call's fully qualified gRPC method name
     */
    [[nodiscard]] std::string method() const;

protected:
    PrivateImplPtr d_ptr;

private:
    PIMPL_DECLARE_PRIVATE(CDynamicCall)
};

/**
 * @brief The @c CDynamicCallTimeoutError class represents an error that is issued
 * because a Dynamic Call operation (read, write, ...) could not be completed
 * within the configured time, e.g. because the server did not send any data for a
 * certain number of seconds.
 */
class CDynamicCallTimeoutError : public QException
{
public:
    /**
     * @brief The CallOperationType enum defines the different types of operations
     * that could run into a timeout during a Dynamic Call
     */
    enum class CallOperationType
    {
        START,
        WRITE,
        WRITES_DONE,
        READ,
        FINISH,
    };

    /**
     * @brief C'tor
     *
     * @param CallOpType The type of operation that timed out during a Dynamic
     * Call
     * @param Call The call that timed out
     */
    CDynamicCallTimeoutError(CallOperationType CallOpType, CDynamicCall* Call);

    /**
     * @brief Convert the given @c CallOperationType @a Type to its human-readable
     * string representation
     *
     * @param Type The @c CallOperationType to convert
     * @return The @a Type's human-readable string representation
     */
    [[nodiscard]] static std::string callOperationTypeToString(
        CallOperationType Type);

    /**
     * @override
     * @brief Get a string with explanatory information about the error.
     */
    [[nodiscard]] const char* what() const noexcept override;

    /**
     * @override
     *
     * @brief Throws this error
     */
    void raise() const override { throw *this; }

    /**
     * @override
     *
     * @brief Clones this error
     *
     * @return A copy of this error
     */
    [[nodiscard]] CDynamicCallTimeoutError* clone() const override
    {
        return new CDynamicCallTimeoutError{*this};
    }

private:
    const std::string m_Message;
};
}  // namespace SiLA2

#endif  // DYNAMICCALL_H
