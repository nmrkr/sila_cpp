/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicStructure.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   01.03.2021
/// \brief  Implementation of the CDynamicStructureElement and
/// CDynamicStructure classes
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicStructure.h>
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>

#include "DynamicValue_p.h"

#include <utility>

using namespace std;
using namespace isocpp_p0201;
using namespace SiLA2::codegen;

namespace SiLA2
{
/**
 * @brief Private data of the CDynamicStructureElement class - pimpl
 */
class CDynamicStructureElement::PrivateImpl : public CDynamicValue::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    explicit PrivateImpl(CDynamicStructureElement* parent, string ID);

    /**
     * @brief C'tor
     */
    PrivateImpl(CDynamicStructureElement* parent,
                const fdl::CDataTypeStructureElement& ElementFDL,
                string StructMessageName,
                shared_ptr<proto::CDynamicMessageFactory> DMF,
                CDynamicFeatureStub* FeatureStub);

    PrivateImpl(const PrivateImpl& rhs) = default;
    PrivateImpl(PrivateImpl&& rhs) = default;

    const fdl::CDataTypeStructureElement StructElement{};
    const string Identifier;
    const string StructureMessageName;
    CDynamicFeatureStub* Feature{nullptr};
};

//============================================================================
CDynamicStructureElement::PrivateImpl::PrivateImpl(
    CDynamicStructureElement* parent, string ID)
    : CDynamicValue::PrivateImpl{parent}, Identifier{move(ID)}
{}

//============================================================================
CDynamicStructureElement::PrivateImpl::PrivateImpl(
    CDynamicStructureElement* parent,
    const fdl::CDataTypeStructureElement& ElementFDL, string StructMessageName,
    shared_ptr<proto::CDynamicMessageFactory> DMF,
    CDynamicFeatureStub* FeatureStub)
    : CDynamicValue::PrivateImpl{parent, ElementFDL.dataType(), move(DMF)},
      StructElement{ElementFDL},
      Identifier{StructElement.identifier().toStdString()},
      StructureMessageName{move(StructMessageName)},
      Feature{FeatureStub}
{}

///===========================================================================
CDynamicStructureElement::CDynamicStructureElement(
    const fdl::CDataTypeStructureElement& ElementFDL, string StructMessageName,
    shared_ptr<proto::CDynamicMessageFactory> DMF, CDynamicFeatureStub* Feature,
    google::protobuf::Message* Value)
    : CDynamicValue(
        make_polymorphic_value<CDynamicValue::PrivateImpl, PrivateImpl>(
            this, ElementFDL, move(StructMessageName), move(DMF), Feature))
{
    PIMPL_D(CDynamicStructureElement);

    if (Value)
    {
        d->Value.reset(Value->New());
        d->Value->CopyFrom(*Value);
    }
    else if (d->Feature)
    {
        PrivateImpl::setEmptyNestedValue(d->Feature, *this);
    }
}

//============================================================================
CDynamicStructureElement::CDynamicStructureElement(string Identifier)
    : CDynamicValue(
        make_polymorphic_value<CDynamicValue::PrivateImpl, PrivateImpl>(
            this, move(Identifier)))
{}

//============================================================================
CDynamicStructureElement::CDynamicStructureElement(
    const CDynamicStructureElement& rhs)
    : CDynamicValue{
        make_polymorphic_value<CDynamicValue::PrivateImpl, PrivateImpl>(
            *rhs.d_func())}
{
    d_ptr->q_ptr = this;
}

//============================================================================
string CDynamicStructureElement::identifier() const
{
    PIMPL_D(const CDynamicStructureElement);
    return d->Identifier;
}

//============================================================================
QString CDynamicStructureElement::displayName() const
{
    PIMPL_D(const CDynamicStructureElement);
    return d->StructElement.displayName();
}

//============================================================================
QString CDynamicStructureElement::description() const
{
    PIMPL_D(const CDynamicStructureElement);
    return d->StructElement.description().simplified();
}

//============================================================================
google::protobuf::Message* CDynamicStructureElement::toProtoMessagePtr() const
{
    PIMPL_D(const CDynamicStructureElement);
    return d->toProtoMessagePtr(d->StructureMessageName, d->Identifier);
}

//============================================================================
CDynamicStructure CDynamicStructureElement::structure() const
{
    PIMPL_D(const CDynamicStructureElement);

    const auto DataTypeFDL = d->getUnderlyingDataType();
    if (DataTypeFDL.type() == fdl::IDataType::Type::Structure)
    {
        return CDynamicStructure{DataTypeFDL.structure(), d->StructureMessageName,
                                 d->Identifier, d->DynamicMessageFactory,
                                 d->Feature};
    }
    return {};
}

//============================================================================
string CDynamicStructureElement::structMessageName() const
{
    PIMPL_D(const CDynamicStructureElement);
    return d->StructureMessageName;
}

//============================================================================
shared_ptr<proto::CDynamicMessageFactory> CDynamicStructureElement::dmf() const
{
    PIMPL_D(const CDynamicStructureElement);
    return d->DynamicMessageFactory;
}

//============================================================================
fdl::CDataTypeStructureElement CDynamicStructureElement::fdl() const
{
    PIMPL_D(const CDynamicStructureElement);
    return d->StructElement;
}

//============================================================================
QString CDynamicStructureElement::prettyString() const
{
    PIMPL_D(const CDynamicStructureElement);
    return QString::fromStdString(d->Identifier) + '='
           + CDynamicValue::prettyString();
}

///===========================================================================
CDynamicStructure::CDynamicStructure(const QList<CDynamicStructureElement>& rhs)
    : QList{rhs},
      m_StructMessageName{constFirst().structMessageName()},
      m_DynamicMessageFactory{constFirst().dmf()}
{}

//============================================================================
google::protobuf::Message* CDynamicStructure::toProtoMessagePtr() const
{
    if (!m_DynamicMessageFactory)
    {
        qCWarning(sila_cpp_client)
            << "Cannot convert Structure " << m_StructMessageName
            << "to a protobuf Message!";
        return nullptr;
    }

    auto Message = m_DynamicMessageFactory->getPrototype(m_StructMessageName);
    for (const auto& el : *this)
    {
        if (auto ElementMessage = el.toProtoMessagePtr(); ElementMessage)
        {
            Message->MergeFrom(*ElementMessage);
            delete ElementMessage;
        }
    }
    return Message.release();
}

//============================================================================
CDynamicStructure::CDynamicStructure(
    fdl::CDataTypeStructure StructureFDL, const string& SurroundingMessageName,
    const string& StructFieldName, shared_ptr<proto::CDynamicMessageFactory> DMF,
    CDynamicFeatureStub* Feature)
    : m_Structure{::move(StructureFDL)},
      m_StructMessageName{SurroundingMessageName + '.' + StructFieldName
                          + "_Struct"},
      m_DynamicMessageFactory{::move(DMF)}
{
    reserve(m_Structure.elements().size());
    transform(
        ::begin(m_Structure.elements()), ::end(m_Structure.elements()),
        back_inserter(*this), [this, Feature](const auto& ElementFDL) mutable {
            return CDynamicStructureElement{ElementFDL, m_StructMessageName,
                                            m_DynamicMessageFactory, Feature};
        });
}

//============================================================================
QStringList CDynamicStructure::elementIdentifiers() const
{
    QStringList List;
    List.reserve(size());
    transform(begin(), end(), back_inserter(List), [](const auto& Elem) {
        return QString::fromStdString(Elem.identifier());
    });
    return List;
}

//============================================================================
void CDynamicStructure::copyElements(const CDynamicStructure& from)
{
    clear();
    copy(::begin(from), ::end(from), back_inserter(*this));
}

//============================================================================
QString CDynamicStructure::prettyString() const
{
    QString Result;
    if (!isEmpty())
    {
        Result += '(';
        auto it = begin();
        auto end_it = end();
        if (it != end_it)
        {
            Result += it->prettyString();
            ++it;
        }
        while (it != end_it)
        {
            Result += ", " + it->prettyString();
            ++it;
        }
        Result += ')';
    }
    return Result;
}

//============================================================================
CDynamicStructureElement& CDynamicStructure::operator[](string_view Identifier)
{
    for (int i = 0; i < size(); ++i)
    {
        if (at(i).identifier() == Identifier)
        {
            return QList::operator[](i);
        }
    }
    throw out_of_range{"No Dynamic StructureElement named \""s + Identifier.data()
                       + "\" in StructureElement List!"};
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDynamicStructureElement& rhs)
{
    QDebugStateSaver s{dbg};
    dbg.nospace() << "CDynamicStructureElement(" << rhs.identifier() << '=';
    dbg << dynamic_cast<const SiLA2::CDynamicValue&>(rhs);
    return dbg << ')';
}

//============================================================================
ostream& operator<<(ostream& os, const CDynamicStructureElement& rhs)
{
    os << "CDynamicStructureElement(" << rhs.identifier() << '=';
    os << dynamic_cast<const SiLA2::CDynamicValue&>(rhs);
    return os << ')';
}
}  // namespace SiLA2
