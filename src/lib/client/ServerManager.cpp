/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   ServerManager.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   20.01.2021
/// \brief  Implementation of the CServerManager class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/client/DynamicSiLAClient.h>
#include <sila_cpp/client/ServerManager.h>
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/common/logging.h>

#include "HeartbeatThread.h"

using namespace std;
using namespace isocpp_p0201;

namespace SiLA2
{
/**
 * @brief Private data of the CServerManager class - pimpl
 */
class CServerManager::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    explicit PrivateImpl(CServerManager* parent);

    /**
     * @brief Add a discovered server that is running on the given @a Address
     *
     * @param UUID The UUID of the server to add
     * @param Address The IP address and port on which the server is running
     * @param ServerInfo Additional information about the discovered Server that
     * was read from TXT records
     * @param CACertificate The certificate of the CA (Certificate Authority) that
     * signed the certificate of the Server
     */
    void onServerAdded(const QUuid& UUID, const CServerAddress& Address,
                       const CServerInformation& ServerInfo,
                       const QString& CACertificate);

    /**
     * @brief Remove the discovered server identified by the given @a UUID
     *
     * @param UUID The UUID of the server to remove
     */
    void onServerRemoved(const QUuid& UUID);

    /**
     * @brief Connect to the Server given through its @a ServerUUID
     *
     * @param ServerUUID The UUID of the Server to connect to
     * @param Certificate The certificate that should be used to connect to the
     * Server
     * @param ForceInsecure Whether to force connecting using unencrypted
     * communication
     *
     * @return A @c shared_ptr to the dynamic client connected to the server or
     * @c nullptr if the connection could not be established
     */
    shared_ptr<CDynamicSiLAClient> connect(const QUuid& ServerUUID,
                                           const QString& Certificate,
                                           bool ForceInsecure);

    /**
     * @brief Add a server to the Manager and connect to it
     *
     * @param Address The IP address and port on which the Server is running
     * @param Certificate The certificate that should be used to connect to the
     * Server
     * @param ForceInsecure Whether to force connecting using unencrypted
     * communication
     *
     * @return A @c shared_ptr to the dynamic client connected to the server or
     * @c nullptr if the connection could not be established
     */
    shared_ptr<CDynamicSiLAClient> addAndConnect(const CServerAddress& Address,
                                                 const QString& Certificate,
                                                 bool ForceInsecure);

    /**
     * @brief Starts the heartbeat mechanism for the Client connected to the
     * Server with the given @a ServerUUID
     *
     * @param ServerUUID The UUID of the Server that the Client that should be
     * monitored is connected to
     */
    void startHeartbeat(const QUuid& ServerUUID);

    /**
     * @brief Stops the heartbeat mechanism for the Client that was connected to
     * the Server with the given @a ServerUUID
     *
     * @param ServerUUID The UUID of the Server that the Client was connected to
     */
    void stopHeartbeat(const QUuid& ServerUUID);

    CServerManager* q_ptr;

    shared_ptr<CSiLAServerDiscovery> Discovery;

    struct sConnection
    {
        CServerAddress Address;
        CServerInformation Info{"", "", "", "", ""};
        shared_ptr<CDynamicSiLAClient> Client{nullptr};
        bool Discovered{false};  ///< whether the server was found automatically
        bool Available{false};   ///< whether a discovered server can be connected
        QString CACertificate{};
        CHeartbeatThread* Heartbeat{nullptr};
    };
    QHash<QUuid, sConnection> Connections;

    PIMPL_DECLARE_PUBLIC(CServerManager)
};

//============================================================================
CServerManager::PrivateImpl::PrivateImpl(CServerManager* parent)
    : q_ptr{parent}, Discovery{make_shared<CSiLAServerDiscovery>(parent)}
{
    QObject::connect(Discovery.get(), &CSiLAServerDiscovery::serverAdded,
                     [this](const QString& UUID, const CServerAddress& Address,
                            const CServerInformation& ServerInfo,
                            const QString& CACertificate) {
                         onServerAdded(UUID, Address, ServerInfo, CACertificate);
                         Connections[UUID].Discovered = true;
                     });
    QObject::connect(Discovery.get(), &CSiLAServerDiscovery::serverRemoved,
                     [this](const QString& UUID) { onServerRemoved(UUID); });
}

//============================================================================
void CServerManager::PrivateImpl::onServerAdded(
    const QUuid& UUID, const CServerAddress& Address,
    const CServerInformation& ServerInfo, const QString& CACertificate)
{
    PIMPL_Q(CServerManager);

    if (q->addServer(UUID, Address) || !Connections.value(UUID).Available)
    {
        auto& Connection = Connections[UUID];
        Connection.Address = Address;
        Connection.Info = ServerInfo;
        Connection.CACertificate = CACertificate;
        Connection.Available = true;
        emit q->serverDiscovered(UUID, Address);
    }
}

//============================================================================
void CServerManager::PrivateImpl::onServerRemoved(const QUuid& UUID)
{
    PIMPL_Q(CServerManager);

    const auto ServerInfo = Connections.value(UUID).Info;
    const auto Heartbeat = Connections.value(UUID).Heartbeat;
    // force a beat to see if the server is actually gone
    if ((Heartbeat && !Heartbeat->beat())
        || (!Heartbeat && Connections.value(UUID).Available))
    {
        qCDebug(sila_cpp_client) << "Server" << UUID << "disappeared";
        auto& Connection = Connections[UUID];
        Connection.Available = false;
        Connection.Client.reset();
        emit q->serverDisappeared(UUID, ServerInfo);
    }
}

//============================================================================
shared_ptr<CDynamicSiLAClient> CServerManager::PrivateImpl::connect(
    const QUuid& ServerUUID, const QString& Certificate, bool ForceInsecure)
{
    PIMPL_Q(CServerManager);
    try
    {
        if (!Connections.contains(ServerUUID))
        {
            throw runtime_error{"There is no Server with this UUID present in "
                                "the Manager."};
        }

        auto& Connection = Connections[ServerUUID];
        if (!Connection.Client)
        {
            Connection.Client =
                make_shared<CDynamicSiLAClient>(Connection.Address);
            if (ForceInsecure)
            {
                Connection.Client->connectInsecure();
            }
            else
            {
                Connection.Client->connect(
                    CSSLCredentials{Certificate.isEmpty() ?
                                        Connection.CACertificate.toStdString() :
                                        Certificate.toStdString()});
            }
            if (ServerUUID != Connection.Client->Get_ServerUUID().toQString())
            {
                Connection.Client.reset();
                throw runtime_error{"The UUID of the Server does not match the "
                                    "UUID as reported by the SiLA Server "
                                    "Discovery! Refusing to continue."};
            }
            qCDebug(sila_cpp_client) << "Connected to Server" << ServerUUID;
            Connection.Info = Connection.Client->serverInformation();
            Connection.CACertificate =
                QString::fromStdString(Connection.Client->credentials().rootCA());
            startHeartbeat(ServerUUID);
        }
        return Connection.Client;
    }
    catch (const exception& err)
    {
        Connections[ServerUUID].Client.reset();
        auto Error = QString{"Couldn't connect to server %1 because: %2"}.arg(
            ServerUUID.toString(QUuid::WithoutBraces), err.what());
        emit q->serverConnectionFailed(ServerUUID, Error);
    }
    return nullptr;
}

//============================================================================
shared_ptr<CDynamicSiLAClient> CServerManager::PrivateImpl::addAndConnect(
    const CServerAddress& Address, const QString& Certificate, bool ForceInsecure)
{
    PIMPL_Q(CServerManager);
    try
    {
        auto Client = make_shared<CDynamicSiLAClient>(Address);
        if (ForceInsecure)
        {
            Client->connectInsecure();
        }
        else
        {
            Client->connect(CSSLCredentials{Certificate.toStdString()});
        }
        const auto UUID = QUuid::fromString(Client->Get_ServerUUID().toQString());

        if (Connections.contains(UUID) || q->addServer(UUID, Address))
        {
            auto& Connection = Connections[UUID];
            Connection.Client = Client;
            Connection.Info = Client->serverInformation();
            Connection.CACertificate =
                QString::fromStdString(Connection.Client->credentials().rootCA());
            startHeartbeat(UUID);
        }
        return Connections.value(UUID).Client;
    }
    catch (const exception& err)
    {
        auto Error = QString{"Couldn't connect to server at %1 because: %2"}.arg(
            Address.toString(), err.what());
        emit q->serverConnectionFailed(Address, Error);
    }
    return nullptr;
}

//============================================================================
void CServerManager::PrivateImpl::startHeartbeat(const QUuid& ServerUUID)
{
    PIMPL_Q(CServerManager);

    auto& Connection = Connections[ServerUUID];
    Connection.Heartbeat = new CHeartbeatThread{Connection.Client};
    QObject::connect(Connection.Heartbeat, &QThread::finished, q,
                     [this, ServerUUID]() { onServerRemoved(ServerUUID); });
    QObject::connect(Connection.Heartbeat, &QThread::finished,
                     Connection.Heartbeat, &QThread::deleteLater);
    Connection.Heartbeat->start();
}

//============================================================================
void CServerManager::PrivateImpl::stopHeartbeat(const QUuid& ServerUUID)
{
    PIMPL_Q(CServerManager);

    if (auto Heartbeat = Connections.value(ServerUUID).Heartbeat; Heartbeat)
    {
        QObject::disconnect(Heartbeat, &QThread::finished, q, nullptr);
        Heartbeat->stop();
    }
}

///===========================================================================
CServerManager::CServerManager(QObject* parent, PrivateImplPtr priv)
    : QObject{parent},
      d_ptr{priv ? std::move(priv) : make_polymorphic_value<PrivateImpl>(this)}
{
    QObject::connect(
        this,
        qOverload<const QUuid&, const QString&>(
            &CServerManager::serverConnectionFailed),
        this, [this](const QUuid& /*ServerUUID*/, const QString& ErrorMsg) {
            emit serverConnectionFailed(ErrorMsg);
        });
    QObject::connect(
        this,
        qOverload<const CServerAddress&, const QString&>(
            &CServerManager::serverConnectionFailed),
        this, [this](const CServerAddress& /*Address*/, const QString& ErrorMsg) {
            emit serverConnectionFailed(ErrorMsg);
        });
}

//============================================================================
CServerManager::~CServerManager()
{
    stop();
}

//============================================================================
QList<QUuid> CServerManager::serverUUIDs() const
{
    PIMPL_D(const CServerManager);
    return d->Connections.keys();
}

//============================================================================
shared_ptr<CDynamicSiLAClient> CServerManager::connection(
    const QUuid& ServerUUID) const
{
    PIMPL_D(const CServerManager);

    if (d->Connections.contains(ServerUUID))
    {
        return d->Connections.value(ServerUUID).Client;
    }
    qCWarning(sila_cpp_client) << "There is no Server with the given UUID"
                               << ServerUUID << "present in this Server Manager!";
    return nullptr;
}

//============================================================================
CServerInformation CServerManager::serverInformation(const QUuid& ServerUUID) const
{
    PIMPL_D(const CServerManager);

    if (d->Connections.contains(ServerUUID))
    {
        const auto Connection = d->Connections.value(ServerUUID);
        if (Connection.Client)
        {
            return Connection.Client->serverInformation();
        }
        else
        {
            return Connection.Info;
        }
    }
    qCWarning(sila_cpp_client) << "There is no Server with the given UUID"
                               << ServerUUID << "present in this Server Manager!";
    return {"", "", "", "", ""};
}

//============================================================================
QString CServerManager::certificate(const QUuid& ServerUUID) const
{
    PIMPL_D(const CServerManager);
    if (d->Connections.contains(ServerUUID))
    {
        return d->Connections.value(ServerUUID).CACertificate;
    }
    qCWarning(sila_cpp_client) << "There is no Server with the given UUID"
                               << ServerUUID << "present in this Server Manager!";
    return {};
}

//============================================================================
bool CServerManager::isDiscoveryActive() const
{
    PIMPL_D(const CServerManager);
    return d->Discovery->isActive();
}

//============================================================================
void CServerManager::start()
{
    PIMPL_D(CServerManager);
    d->Discovery->startDiscovery();
    emit started();
}

//============================================================================
void CServerManager::stop()
{
    PIMPL_D(CServerManager);
    d->Discovery->stopDiscovery();
    emit stopped();
}

//============================================================================
void CServerManager::restart()
{
    blockSignals(true);
    stop();
    blockSignals(false);
    start();
}

//============================================================================
bool CServerManager::addServer(const QUuid& ServerUUID,
                               const SiLA2::CServerAddress& Address)
{
    PIMPL_D(CServerManager);

    if (ServerUUID.isNull())
    {
        qCWarning(sila_cpp_client)
            << "Cannot add Server running on " << Address << "because the UUID"
            << ServerUUID << "is invalid";
        return false;
    }

    if (d->Connections.contains(ServerUUID))
    {
        qCDebug(sila_cpp_client).nospace()
            << "Server with ServerUUID " << ServerUUID << " (running on "
            << d->Connections.value(ServerUUID).Address
            << ") already present in Server Manager. Won't add "
               "again - remove the other Connection first.";
        return false;
    }
    d->Connections.insert(ServerUUID, {Address});
    qCDebug(sila_cpp_client) << "Added Server" << ServerUUID;
    return true;
}

//============================================================================
shared_ptr<CDynamicSiLAClient> CServerManager::connect(const QUuid& ServerUUID,
                                                       const QString& Certificate)
{
    PIMPL_D(CServerManager);
    return d->connect(ServerUUID, Certificate, false);
}

//============================================================================
shared_ptr<CDynamicSiLAClient> CServerManager::connectInsecure(
    const QUuid& ServerUUID)
{
    PIMPL_D(CServerManager);
    return d->connect(ServerUUID, QString{}, true);
}

//============================================================================
shared_ptr<CDynamicSiLAClient> CServerManager::addAndConnect(
    const QUuid& ServerUUID, const CServerAddress& Address,
    const QString& Certificate)
{
    addServer(ServerUUID, Address);
    return connect(ServerUUID, Certificate);
}

//============================================================================
shared_ptr<CDynamicSiLAClient> CServerManager::addAndConnectInsecure(
    const QUuid& ServerUUID, const CServerAddress& Address)
{
    PIMPL_D(CServerManager);
    addServer(ServerUUID, Address);
    return d->connect(ServerUUID, QString{}, true);
}

//============================================================================
shared_ptr<CDynamicSiLAClient> CServerManager::addAndConnect(
    const CServerAddress& Address, const QString& Certificate)
{
    PIMPL_D(CServerManager);
    return d->addAndConnect(Address, Certificate, false);
}

//============================================================================
shared_ptr<CDynamicSiLAClient> CServerManager::addAndConnectInsecure(
    const CServerAddress& Address)
{
    PIMPL_D(CServerManager);
    return d->addAndConnect(Address, QString{}, true);
}

//============================================================================
bool CServerManager::removeServer(const QUuid& ServerUUID)
{
    PIMPL_D(CServerManager);

    if (ServerUUID.isNull())
    {
        qCWarning(sila_cpp_client) << "Cannot remove Server because the UUID"
                                   << ServerUUID << "is invalid";
        return false;
    }
    if (d->Connections.remove(ServerUUID))
    {
        d->stopHeartbeat(ServerUUID);
        qCDebug(sila_cpp_client) << "Removed Server" << ServerUUID;
        return true;
    }
    return false;
}

//============================================================================
void CServerManager::disconnect(const QUuid& ServerUUID)
{
    PIMPL_D(CServerManager);

    if (!d->Connections.contains(ServerUUID))
    {
        qCWarning(sila_cpp_client)
            << "Cannot disconnect from Server" << ServerUUID
            << "because there is no Server with this UUID "
               "present in the Manager!";
        return;
    }

    d->Connections[ServerUUID].Client.reset();
    d->stopHeartbeat(ServerUUID);
    qCDebug(sila_cpp_client) << "Disconnected from Server" << ServerUUID;
}
}  // namespace SiLA2
