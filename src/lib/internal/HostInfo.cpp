/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2020 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   HostInfo.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   29.09.2020
/// \brief  Implementation of the CHostInfo class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/internal/HostInfo.h>

#include <QHostAddress>
#include <QHostInfo>
#include <qglobal.h>

namespace SiLA2::internal
{
//=============================================================================
QString CHostInfo::lookupHostname(const QString& IP)
{
    if (IP == QStringLiteral("localhost"))
    {
        // no need to look up "localhost" -> it's already a host name
        return IP;
    }

    const auto Host = QHostInfo::fromName(IP);
    if (Host.error() != QHostInfo::NoError)
    {
        qCWarning(sila_cpp_internal)
            << "Could not look up IP" << IP << "\nError:" << Host.errorString();
        return "";
    }
    auto Hostname = Host.hostName();
    qCDebug(sila_cpp_internal)
        << "Looked up IP" << IP << "to be host" << Hostname;
    return Hostname;
}

//=============================================================================
QString CHostInfo::localHostName()
{
#ifdef Q_OS_WIN
    return QHostInfo::fromName("::1").hostName();
#elif defined(Q_OS_UNIX)
    return QHostInfo::localHostName() + '.' + QHostInfo::localDomainName();
#endif
}

//============================================================================
bool CHostInfo::isPrivateIP(const QString& IP)
{
    static const auto Block24 = QHostAddress::parseSubnet("10.0.0.0/8");
    static const auto Block20 = QHostAddress::parseSubnet("172.16.0.0/12");
    static const auto Block16 = QHostAddress::parseSubnet("192.168.0.0/16");
    QHostAddress Address;
    if (!Address.setAddress(IP))
    {
        const auto HostInfo = QHostInfo::fromName(IP);
        if (HostInfo.addresses().isEmpty())
        {
            qCDebug(sila_cpp_internal) << "Could not determine if" << IP
                                       << "is in a private IP address range";
            return false;
        }
        Address = HostInfo.addresses().first();
        qCDebug(sila_cpp_internal) << "Looked up" << IP << "to be" << Address;
    }
    return Address.isInSubnet(Block24) || Address.isInSubnet(Block20)
           || Address.isInSubnet(Block16) || Address.isLoopback()
           || Address.isUniqueLocalUnicast();
}
}  // namespace SiLA2::internal
