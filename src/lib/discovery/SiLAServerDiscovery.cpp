/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAServerDiscovery.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   20.01.2021
/// \brief  Implementation of the CSiLAServerDiscovery class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/ServerAddress.h>
#include <sila_cpp/common/ServerInformation.h>
#include <sila_cpp/common/constants.h>
#include <sila_cpp/common/logging.h>
#include <sila_cpp/discovery/SiLAServerDiscovery.h>

#include <QSslCertificate>
#include <QSslCertificateExtension>
#include <QString>
#include <QUuid>

#include <QtZeroConf/qzeroconf.h>

#include <memory>

using namespace std;
using namespace isocpp_p0201;

namespace SiLA2
{
static constexpr auto TXT_RECORD_MAX_LENGTH = 255;

/**
 * @brief Private data of the CSiLAServerDiscovery class - pimpl
 */
class CSiLAServerDiscovery::PrivateImpl
{
public:
    /**
     * @brief C'tor
     */
    explicit PrivateImpl(CSiLAServerDiscovery* parent);

    /**
     * @brief Slot for QZeroconf's @c serviceAdded signal. Converts the service
     * into a SiLA Server end emits @c CSiLAServerDiscovery::serverAdded
     *
     * @param Service The service that got added
     */
    void addService(const QZeroConfService& Service);

    /**
     * @brief Slot for QZeroconf's @c serviceRemoved signal. Converts the service
     * into a SiLA Server end emits @c CSiLAServerDiscovery::serverRemoved
     *
     * @param Service The service that got removed
     */
    void removeService(const QZeroConfService& Service);

    /**
     * @brief Extracts the Server's CA certificate from the @a TXTRecords
     *
     * @param TXTRecords The TXT records of the Server's Discovery announcement
     *
     * @return The Server's CA certificate as a PEM encoded string or an empty
     * string if the Server does not include its CA certificate in the TXT records
     */
    static QString extractCAFromTXT(
        const QMap<QByteArray, QByteArray>& TXTRecords);

    /**
     * @brief Check if the UUID of the Server that was discovered matches the UUID
     * in the server's certificate. This function will only perform the check if
     * the Common Name of the @a CACertificate is "SiLA2" since otherwise we can't
     * be certain that the certificate contains the Server's UUID.
     *
     * @param CACertificate The CA certificate that was extracted from the TXT
     * records
     * @param ServerUUID The Server UUID that was used for announcing the
     * Server (this is the UUID that should also be stored in the
     * @a CACertificate if its Common Name is "SiLA2")
     *
     * @returns true, if the Common Name is not "SiLA2" or when the Common Name is
     * "SiLA2" and the UUIDs match
     * @returns false, if the Common Name is "SiLA2" and the UUIDs don't match
     */
    [[nodiscard]] static bool checkServerAuthenticity(
        const QString& CACertificate, const QUuid& ServerUUID);

    CSiLAServerDiscovery* q_ptr;

    shared_ptr<QZeroConf> ZeroConf{};      ///< ZeroConf for SiLA Discovery
    static const char* const ServiceName;  ///< service name as defined in Part B

    PIMPL_DECLARE_PUBLIC(CSiLAServerDiscovery)
};

//============================================================================
const char* const CSiLAServerDiscovery::PrivateImpl::ServiceName = "_sila._tcp";

//============================================================================
CSiLAServerDiscovery::PrivateImpl::PrivateImpl(CSiLAServerDiscovery* parent)
    : q_ptr{parent}, ZeroConf{make_shared<QZeroConf>(parent)}
{
    PIMPL_Q(CSiLAServerDiscovery);

    Q_UNUSED(SiLALogManager)

    connect(ZeroConf.get(), &QZeroConf::servicePublished, []() {
        qCInfo(sila_cpp_discovery)
            << "ZeroConf service for SiLA Server published";
    });

    connect(ZeroConf.get(), &QZeroConf::serviceAdded,
            [&](const QZeroConfService& s) { addService(s); });
    connect(ZeroConf.get(), &QZeroConf::serviceRemoved,
            [&](const QZeroConfService& s) { removeService(s); });
    connect(ZeroConf.get(), &QZeroConf::error, q, [](const auto& Error) {
        static const QString PublishError =
            "Could not publish ZeroConf service! The following error occurred:";
        switch (Error)
        {
        case QZeroConf::serviceRegistrationFailed:
            qCWarning(sila_cpp_discovery)
                << PublishError << "Service Registration Failed";
            break;
        case QZeroConf::serviceNameCollision:
            qCWarning(sila_cpp_discovery)
                << PublishError << "Service Name Collision";
            break;
        case QZeroConf::browserFailed:
            qCWarning(sila_cpp_discovery) << "Browser Failed";
            break;
        default:
            break;
        }
    });
}

//============================================================================
void CSiLAServerDiscovery::PrivateImpl::addService(const QZeroConfService& Service)
{
    PIMPL_Q(CSiLAServerDiscovery);
    qCDebug(sila_cpp_discovery).nospace().noquote()
        << "Found service with name " << Service->name() << " on "
        << Service->ip().toString() << ':' << Service->port();
    const auto TXTRecords = Service->txt();
    qCDebug(sila_cpp_discovery) << "TXT records" << TXTRecords;
    const CServerAddress Address{Service->ip().toString(),
                                 QString::number(Service->port())};
    const CServerInformation ServerInfo{TXTRecords.value("server_name"),
                                        "UnknownServerType",
                                        TXTRecords.value("description"),
                                        TXTRecords.value("version"),
                                        "",
                                        Service->name()};
    const auto CACertificate = extractCAFromTXT(TXTRecords);
    // NOTE: Part B states that we MAY reject servers that do not have their UUID
    //  stored in the certificate (which also means we MAY reject servers that
    //  have any other UUID in the certificate than the one they advertised
    //  themselves with). For now I'm averse to simply prohibiting any connection
    //  to a server where the UUID in the certificate is missing or doesn't match.
    //  If there's demand from users of sila_cpp later we could add something to
    //  let the user choose if they want to (dis-)allow connections to such
    //  servers.
    qCDebug(sila_cpp_discovery)
        << "Server UUID identical with UUID from certificate?"
        << checkServerAuthenticity(CACertificate, Service->name());
    emit q->serverAdded(Service->name(), Address, ServerInfo, CACertificate);
}

//============================================================================
void CSiLAServerDiscovery::PrivateImpl::removeService(
    const QZeroConfService& Service)
{
    PIMPL_Q(CSiLAServerDiscovery);
    qCDebug(sila_cpp_discovery).nospace().noquote()
        << "Service with name " << Service->name() << " on "
        << Service->ip().toString() << ':' << Service->port() << " disappeared";
    emit q->serverRemoved(Service->name());
}

//============================================================================
QString CSiLAServerDiscovery::PrivateImpl::extractCAFromTXT(
    const QMap<QByteArray, QByteArray>& TXTRecords)
{
    return accumulate(
        TXTRecords.constKeyValueBegin(), TXTRecords.constKeyValueEnd(), QString{},
        [](const QString& Cert, const pair<QByteArray, QByteArray>& Record) {
            if (Record.first.startsWith("ca"))
            {
                return Cert + Record.second;
            }
            return Cert;
        });
}

//============================================================================
bool CSiLAServerDiscovery::PrivateImpl::checkServerAuthenticity(
    const QString& CACertificate, const QUuid& ServerUUID)
{
    const QSslCertificate Cert{CACertificate.toLatin1()};
    if (Cert.isSelfSigned() && Cert.issuerDisplayName() == "SiLA2")
    {
        auto Extensions = Cert.extensions();
        return any_of(cbegin(Extensions), cend(Extensions),
                      [ServerUUID](const auto& Extension) {
                          using namespace constants;
                          return Extension.oid() == SILA2_IANA_PEN
                                 && Extension.value().toUuid() == ServerUUID;
                      });
    }
    return true;
}

//============================================================================
CSiLAServerDiscovery::CSiLAServerDiscovery(QObject* parent, PrivateImplPtr priv)
    : QObject{parent},
      d_ptr{priv ? move(priv) : make_polymorphic_value<PrivateImpl>(this)}
{
}

//============================================================================
CSiLAServerDiscovery::~CSiLAServerDiscovery()
{
    stopDiscovery();
}

//============================================================================
void CSiLAServerDiscovery::publishServer(const CServerInformation& ServerInfo,
                                         int ServerPort, const QString& RootCA)
{
    PIMPL_D(CSiLAServerDiscovery);

    d->ZeroConf->addServiceTxtRecord("version", ServerInfo.version());
    d->ZeroConf->addServiceTxtRecord("server_name", ServerInfo.serverName());
    d->ZeroConf->addServiceTxtRecord("description", ServerInfo.description());

    int CurrentPos = 0;
    size_t RecordNum = 0;
    const auto CALength = RootCA.length();
    while (CurrentPos < CALength)
    {
        auto Key = QString{"ca%1"}.arg(RecordNum++);
        // TXT records are formatted as "key=value" => subtract 1 for the '='
        const auto ValueLength = TXT_RECORD_MAX_LENGTH - Key.length() - 1;
        d->ZeroConf->addServiceTxtRecord(Key,
                                         RootCA.mid(CurrentPos, ValueLength));
        CurrentPos += ValueLength;
    }

    // Naming convention according to SiLA 2 Part B
    d->ZeroConf->startServicePublish(
        qPrintable(ServerInfo.serverUUID().toString(QUuid::WithoutBraces)),
        PrivateImpl::ServiceName, "local.", static_cast<quint16>(ServerPort));
}

//============================================================================
void CSiLAServerDiscovery::unpublishServer()
{
    PIMPL_D(CSiLAServerDiscovery);

    d->ZeroConf->stopServicePublish();
}

//============================================================================
void CSiLAServerDiscovery::startDiscovery()
{
    PIMPL_D(CSiLAServerDiscovery);

    if (!d->ZeroConf->browserExists())
    {
        d->ZeroConf->startBrowser(PrivateImpl::ServiceName);
        qCInfo(sila_cpp_discovery)
            << "Started searching for available SiLA Servers";
    }
}

//============================================================================
void CSiLAServerDiscovery::stopDiscovery()
{
    PIMPL_D(CSiLAServerDiscovery);
    d->ZeroConf->stopBrowser();
}

//============================================================================
bool CSiLAServerDiscovery::isActive() const
{
    PIMPL_D(const CSiLAServerDiscovery);
    return d->ZeroConf->browserExists();
}
}  // namespace SiLA2
