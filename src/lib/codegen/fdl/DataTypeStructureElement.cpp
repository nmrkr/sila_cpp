/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataTypeStructureElement.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   28.01.2021
/// \brief  Implementation of the CDataTypeStructureElement class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/logging.h>
#include <sila_cpp/codegen/fdl/DataTypeStructureElement.h>

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CDataTypeStructureElement::toVariant() const
{
    auto Map = CSiLAElement::toVariant().toMap();
    Map.insert("DataType", m_DataType.toVariant());
    return Map;
}

//============================================================================
void CDataTypeStructureElement::fromVariant(const QVariant& from)
{
    CSiLAElement::fromVariant(from);
    m_DataType.fromVariant(from.toMap().value("DataType"));
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDataTypeStructureElement& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "Element(" << rhs.identifier()
                         << ", Data Type: " << rhs.dataType() << ')';
}

//============================================================================
std::ostream& operator<<(
    std::ostream& os, const SiLA2::codegen::fdl::CDataTypeStructureElement& rhs)
{
    return os << "Element(" << rhs.identifier()
              << ", Data Type: " << rhs.dataType() << ')';
}
}  // namespace SiLA2::codegen::fdl
