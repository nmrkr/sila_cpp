/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Serializer.cpp.cc
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Implementation of the CFDLSerializer class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/FDLSerializer.h>
#include <sila_cpp/common/FullyQualifiedFeatureID.h>
#include <sila_cpp/common/logging.h>

#include "FDLErrorHandler.h"

#include <QXmlStreamReader>

#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

using namespace std;

namespace SiLA2::codegen::fdl
{
///===========================================================================
///                               SERIALIZATION
///===========================================================================
/**
 * @brief Write the given @c QVariant object @a Variant to the given XML stream
 * @a Stream using the @a NodeName as the XML tag for the element
 *
 * @param NodeName The XML tag for the given @a Variant
 * @param Variant The object to write to the stream
 * @param Stream The XML stream to write to
 */
void writeVariantToStream(const QString& NodeName, const QVariant& Variant,
                          QXmlStreamWriter& Stream);

/**
 * @brief Write the given @c QVariantMap object @a Variant to the given XML stream
 * @a Stream
 *
 * @param Variant The map to write to the stream
 * @param Stream The XML stream to write to
 */
void writeVariantMapToStream(const QVariant& Variant, QXmlStreamWriter& Stream);

/**
 * @brief Write the given @c QVariantList object @a Variant to the given XML
 * stream @a Stream
 *
 * @param Variant The list to write to the stream
 * @param Stream The XML stream to write to
 */
void writeVariantListToStream(const QVariant& Variant, QXmlStreamWriter& Stream);

/**
 * @brief Write the given text @a Variant to the given XML stream @a Stream
 *
 * @param Variant The text to write to the stream
 * @param Stream The XML stream to write to
 */
void writeTextToStream(const QVariant& Variant, QXmlStreamWriter& Stream);

//============================================================================
void writeVariantToStream(const QString& NodeName, const QVariant& Variant,
                          QXmlStreamWriter& Stream)
{
    qCDebug(sila_cpp_codegen) << NodeName;

    if (!NodeName.isEmpty())
    {
        Stream.writeStartElement(NodeName);
    }

    switch (Variant.type())
    {
    case QVariant::Map:
        writeVariantMapToStream(Variant, Stream);
        break;
    case QVariant::List:
        writeVariantListToStream(Variant, Stream);
        break;
    default:
        writeTextToStream(Variant, Stream);
        break;
    }

    if (!NodeName.isEmpty())
    {
        Stream.writeEndElement();
    }
}

//============================================================================
void writeVariantMapToStream(const QVariant& Variant, QXmlStreamWriter& Stream)
{
    QMultiMap Map = Variant.toMap();
    for (const auto& Attribute : Map.values("attribute"))
    {
        const auto AttributeMap = Attribute.toMap();
        Stream.writeAttribute(AttributeMap.firstKey(),
                              AttributeMap.first().toString());
        Map.remove(Map.key(Attribute));
    }

    for (auto it = Map.begin(); it != Map.end(); ++it)
    {
        writeVariantToStream(it.key(), it.value(), Stream);
    }
}

//============================================================================
void writeVariantListToStream(const QVariant& Variant, QXmlStreamWriter& Stream)
{
    const auto List = Variant.toList();
    for (const auto& el : List)
    {
        writeVariantToStream("", el, Stream);
    }
}

//============================================================================
void writeTextToStream(const QVariant& Variant, QXmlStreamWriter& Stream)
{
    Stream.writeCharacters(Variant.toString());
}

//============================================================================
QString CFDLSerializer::serialize(const ISerializable& Element)
{
    return serialize(Element.name(), Element.toVariant());
}

//============================================================================
QString CFDLSerializer::serialize(const QString& ElementName,
                                  const QVariant& ElementVariant)
{
    QString FDL;
    QXmlStreamWriter Stream{&FDL};
    Stream.setAutoFormatting(true);
    Stream.writeStartDocument();
    writeVariantToStream(ElementName, ElementVariant, Stream);
    Stream.writeEndDocument();
    qCDebug(sila_cpp_codegen()) << FDL.toStdString();
    return FDL;
}

///===========================================================================
///                              DESERIALIZATION
///===========================================================================

/**
 * @brief Maps strings to the @c Type they represent
 */
QMap<QString, QVariant::Type> StringToType{
    {"Feature", QVariant::Map},
    {"DataType", QVariant::Map},
    {"List", QVariant::Map},
    {"Structure", QVariant::Map},
    {"Element", QVariant::Map},
    {"Command", QVariant::Map},
    {"Parameter", QVariant::Map},
    {"Response", QVariant::Map},
    {"IntermediateResponse", QVariant::Map},
    {"Property", QVariant::Map},
    {"DefinedExecutionError", QVariant::Map},
    {"Metadata", QVariant::Map},
    {"DefinedExecutionErrors", QVariant::Map},
    {"DataTypeDefinition", QVariant::Map},
    {"Constrained", QVariant::Map},
    {"Constraints", QVariant::Map},
    {"Set", QVariant::List},
    {"Unit", QVariant::Map},
    {"UnitComponent", QVariant::Map},
    {"ContentType", QVariant::Map},
    {"Parameters", QVariant::List},  ///< belongs to ContentType constraint
    {"Schema", QVariant::Map},
    {"AllowedTypes", QVariant::List},
};

/**
 * @brief Read a @c QVariant object from the given XML stream @a Stream
 *
 * @param Stream The XML stream to read from
 * @return The read @c QVariant object
 */
QVariant readVariantFromStream(QXmlStreamReader& Stream);

/**
 * @brief Read a @c QVariantMap from the given stream @a Stream. The returned Map
 * will also contain all attributes of the current Stream Element.
 *
 * @param Stream The XML stream to read from
 * @return The read @c QVariantMap
 */
QVariant readVariantMapFromStream(QXmlStreamReader& Stream);

/**
 * @brief Read a @c QVariantList from the given stream @a Stream
 *
 * @param Stream The XML stream to read from
 * @return The read @c QVariantList
 */
QVariant readVariantListFromStream(QXmlStreamReader& Stream);

/**
 * @brief Read the text content of the current XML element from the given stream
 * @a Stream
 *
 * @param Stream The XML stream to read from
 * @return The text content of the current element
 */
QVariant readTextFromStream(QXmlStreamReader& Stream);

//============================================================================
QVariant readVariantFromStream(QXmlStreamReader& Stream)
{
    if (Stream.hasError())
    {
        throw std::runtime_error{"Could not parse Feature Definition! "
                                 "The following error occurred: "
                                 + Stream.errorString().toStdString()};
    }

    const auto TypeString = Stream.name().toString();
    qCDebug(sila_cpp_codegen) << TypeString;
    switch (StringToType.value(TypeString))
    {
    case QVariant::Map:
        return readVariantMapFromStream(Stream);
    case QVariant::List:
        return readVariantListFromStream(Stream);
    default:
        return readTextFromStream(Stream);
    }
}

//============================================================================
QVariant readVariantMapFromStream(QXmlStreamReader& Stream)
{
    qCDebug(sila_cpp_codegen) << "---" << Stream.name();
    QMultiMap<QString, QVariant> Map;

    const auto Attributes = Stream.attributes();
    for (const auto& Attribute : Attributes)
    {
        Map.insert(Attribute.name().toString(), Attribute.value().toString());
    }

    while (Stream.readNextStartElement())
    {
        Map.insert(Stream.name().toString(), readVariantFromStream(Stream));
    }
    //    qCDebug(sila_cpp_codegen) << "Map" << Map;
    return Map;
}

//============================================================================
QVariant readVariantListFromStream(QXmlStreamReader& Stream)
{
    qCDebug(sila_cpp_codegen) << "---" << Stream.name();
    QVariantList List;
    while (Stream.readNextStartElement())
    {
        List.append(readVariantFromStream(Stream));
    }
    //    qCDebug(sila_cpp_codegen) << "List" << List;
    return List;
}

//============================================================================
QVariant readTextFromStream(QXmlStreamReader& Stream)
{
    return Stream.readElementText();
}

//============================================================================
bool CFDLSerializer::deserialize(ISerializable& Element, const QString& FDL)
{
    QXmlStreamReader Stream{FDL};
    Stream.readNextStartElement();
    try
    {
        Element.fromVariant(readVariantFromStream(Stream));
    }
    catch (const runtime_error& err)
    {
        qCWarning(sila_cpp_codegen)
            << "Failed to deserialize Feature! Reason:" << err.what();
        return false;
    }
    return true;
}

//============================================================================
bool CFDLSerializer::validate(const CFullyQualifiedFeatureID& FeatureID,
                              const QString& FDL)
{
#ifdef Q_OS_WIN
    // currently, xerces-c does not support https:// URLs even with the winsock
    // network accessor and thus all of the following would always fail with the
    // error "unsupported protocol in URL"
    // (see https://issues.apache.org/jira/browse/XERCESC-2220)
    return true;
#else
    xercesc::XMLPlatformUtils::Initialize();
    xercesc::XercesDOMParser Parser;
    CFDLErrorHandler ErrorHandler{FDL};
    Parser.setErrorHandler(&ErrorHandler);
    Parser.setValidationScheme(xercesc::XercesDOMParser::Val_Always);
    Parser.setDoNamespaces(true);
    Parser.setDoSchema(true);
    Parser.setLoadSchema(true);
    Parser.setValidationSchemaFullChecking(true);

    const auto FDLString = FDL.toStdString();
    xercesc::MemBufInputSource FDLInputSource{
        reinterpret_cast<const XMLByte* const>(FDLString.c_str()),
        sizeof(XMLByte) * static_cast<ulong>(FDLString.size()),
        FeatureID.toStdString().c_str()};
    Parser.parse(FDLInputSource);
    if (Parser.getErrorCount() > 0)
    {
        qCWarning(sila_cpp_codegen)
            << "Validation of Feature Description for Feature" << FeatureID
            << "failed";
        return false;
    }
    qCDebug(sila_cpp_codegen) << "Validation of Feature Description for Feature"
                              << FeatureID << "successful";
    return true;
#endif
}
}  // namespace SiLA2::codegen::fdl
