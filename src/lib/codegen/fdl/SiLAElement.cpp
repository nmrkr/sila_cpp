/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   SiLAElement.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Implementation of the CSiLAElement class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/SiLAElement.h>
#include <sila_cpp/common/logging.h>

#include "utils.h"

using namespace std;

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CSiLAElement::toVariant() const
{
    return QVariantMap{{"Identifier", m_Identifier},
                       {"DisplayName", m_DisplayName},
                       {"Description", m_Description}};
}

//============================================================================
void CSiLAElement::fromVariant(const QVariant& from)
{
    auto Map = from.toMap();
    if (Q_UNLIKELY(Map.empty()))
    {
        throw runtime_error{"Failed to parse Feature Description! <"
                            + name().toStdString() + "> is empty."};
    }
    m_Identifier = Map.value("Identifier").toString();
    if (Q_UNLIKELY(m_Identifier.isEmpty()))
    {
        throw runtime_error{"Failed to parse Feature Description! <"
                            + name().toStdString() + "> has no <Identifier>."};
    }
    m_DisplayName = Map.value("DisplayName").toString();
    m_Description = Map.value("Description").toString();
}

///===========================================================================
QVariant CSiLAElementWithDataType::toVariant() const
{
    auto Map = CSiLAElement::toVariant().toMap();
    Map.insert("DataType", m_DataType.toVariant());
    return Map;
}

//============================================================================
void CSiLAElementWithDataType::fromVariant(const QVariant& from)
{
    CSiLAElement::fromVariant(from);
    m_DataType.fromVariant(from.toMap().value("DataType"));
}

///===========================================================================
QVariant CSiLAElementWithErrors::toVariant() const
{
    auto Map = CSiLAElement::toVariant().toMap();
    insertIfNotEmpty(Map, "DefinedExecutionErrors",
                     toReverseVariantMaps("Identifier", m_Errors));
    return Map;
}

//============================================================================
void CSiLAElementWithErrors::fromVariant(const QVariant& from)
{
    CSiLAElement::fromVariant(from);
    const QMultiMap Map = from.toMap().value("DefinedExecutionErrors").toMap();
    fromReverseVariantList(Map.values("Identifier"), m_Errors);
}

//============================================================================
QDebug operator<<(QDebug dbg, const CSiLAElement& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "CSiLAElement(" << rhs.identifier() << ", "
                         << rhs.displayName() << ", " << rhs.description() << ')';
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CSiLAElement& rhs)
{
    return os << "CSiLAElement(" << rhs.identifier() << ", " << rhs.displayName()
              << ", " << rhs.description() << ')';
}

//============================================================================
QDebug operator<<(QDebug dbg, const CSiLAElementWithErrors& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace() << "CSiLAElement(" << rhs.identifier() << ", "
                         << rhs.displayName() << ", " << rhs.description() << ", "
                         << rhs.errors() << ')';
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CSiLAElementWithErrors& rhs)
{
    return os << "CSiLAElement(" << rhs.identifier() << ", " << rhs.displayName()
              << ", " << rhs.description() << ", " << rhs.errors() << ')';
}
}  // namespace SiLA2::codegen::fdl
