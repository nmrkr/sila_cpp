/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Property.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   02.02.2021
/// \brief  Implementation of the CProperty class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/Property.h>
#include <sila_cpp/common/logging.h>

using namespace std;

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CProperty::toVariant() const
{
    auto Map = CSiLAElementWithErrors::toVariant().toMap();
    Map.insert("Observable", m_Observable ? "Yes" : "No");
    Map.insert("DataType", m_DataType.toVariant());
    return Map;
}

//============================================================================
void CProperty::fromVariant(const QVariant& from)
{
    CSiLAElementWithErrors::fromVariant(from);
    CSiLAElementWithDataType::fromVariant(from);
    const QMultiMap<QString, QVariant> Map = from.toMap();
    m_Observable = (Map.value("Observable").toString() == "Yes");
}

//============================================================================
QDebug operator<<(QDebug dbg, const CProperty& rhs)
{
    QDebugStateSaver s{dbg};
    return dbg.nospace()
           << "\n\t\tCProperty(" << rhs.identifier() << ':'
           << "\n\t\t\tObservable: " << (rhs.isObservable() ? "Yes" : "No")
           << "\n\t\t\tData Type: " << rhs.dataType()
           << "\n\t\t\tErrors: " << rhs.errors() << ')';
}

//============================================================================
ostream& operator<<(ostream& os, const CProperty& rhs)
{
    return os << "\n\t\tCProperty(" << rhs.identifier() << ':'
              << "\n\t\t\tObservable: " << (rhs.isObservable() ? "Yes" : "No")
              << "\n\t\t\tData Type: " << rhs.dataType()
              << "\n\t\t\tErrors: " << rhs.errors() << ')';
}
}  // namespace SiLA2::codegen::fdl
