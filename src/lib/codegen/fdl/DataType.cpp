/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataType.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   27.01.2021
/// \brief  Implementation of the CDataType class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataType.h>
#include <sila_cpp/codegen/fdl/DataTypeBasic.h>
#include <sila_cpp/codegen/fdl/DataTypeConstrained.h>
#include <sila_cpp/codegen/fdl/DataTypeIdentifier.h>
#include <sila_cpp/codegen/fdl/DataTypeList.h>
#include <sila_cpp/codegen/fdl/DataTypeStructure.h>
#include <sila_cpp/common/logging.h>

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CDataType::toVariant() const
{
    switch (m_Type)
    {
    case Type::Basic:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CDataTypeBasic*>(m_DataType)->toVariant()}};
    case Type::Constrained:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CDataTypeConstrained*>(m_DataType)->toVariant()}};
    case Type::Identifier:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CDataTypeIdentifier*>(m_DataType)->toVariant()}};
    case Type::List:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CDataTypeList*>(m_DataType)->toVariant()}};
    case Type::Structure:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CDataTypeStructure*>(m_DataType)->toVariant()}};
    default:
        qCCritical(sila_cpp_codegen) << "Shouldn't get here";
        return {};
    }
}

//============================================================================
void CDataType::fromVariant(const QVariant& from)
{
    // qCDebug(sila_cpp_codegen) << "from:\n" << from;
    const auto Map = from.toMap();
    if (Q_UNLIKELY(Map.empty()))
    {
        throw std::runtime_error{"Failed to parse Feature Description! "
                                 "<DataType> is empty."};
    }
    const auto& Key = Map.firstKey();
    m_Type = stringToType(Key);

    switch (m_Type)
    {
    case Type::Basic:
        m_DataType = new CDataTypeBasic;
        dynamic_cast<CDataTypeBasic*>(m_DataType)->fromVariant(Map.value(Key));
        break;
    case Type::Constrained:
        m_DataType = new CDataTypeConstrained;
        dynamic_cast<CDataTypeConstrained*>(m_DataType)
            ->fromVariant(Map.value(Key));
        break;
    case Type::Identifier:
        m_DataType = new CDataTypeIdentifier;
        dynamic_cast<CDataTypeIdentifier*>(m_DataType)
            ->fromVariant(Map.value(Key));
        break;
    case Type::List:
        m_DataType = new CDataTypeList;
        dynamic_cast<CDataTypeList*>(m_DataType)->fromVariant(Map.value(Key));
        break;
    case Type::Structure:
        m_DataType = new CDataTypeStructure;
        dynamic_cast<CDataTypeStructure*>(m_DataType)->fromVariant(Map.value(Key));
        break;
    default:
        break;
    }
}

//============================================================================
CDataTypeBasic CDataType::basic() const
{
    SILA_CPP_DISABLE_WARNING_PUSH
    SILA_CPP_DISABLE_WARNING_NULL_DEREFERENCE
    switch (m_Type)
    {
    case Type::Basic:
        return *(dynamic_cast<CDataTypeBasic*>(m_DataType));
    default:
        throw std::bad_cast{};
    }
    SILA_CPP_DISABLE_WARNING_POP
}

//============================================================================
CDataTypeConstrained CDataType::constrained() const
{
    SILA_CPP_DISABLE_WARNING_PUSH
    SILA_CPP_DISABLE_WARNING_NULL_DEREFERENCE
    switch (m_Type)
    {
    case Type::Constrained:
        return *(dynamic_cast<CDataTypeConstrained*>(m_DataType));
    default:
        throw std::bad_cast{};
    }
    SILA_CPP_DISABLE_WARNING_POP
}

//============================================================================
CDataTypeIdentifier CDataType::identifier() const
{
    SILA_CPP_DISABLE_WARNING_PUSH
    SILA_CPP_DISABLE_WARNING_NULL_DEREFERENCE
    switch (m_Type)
    {
    case Type::Identifier:
        return *(dynamic_cast<CDataTypeIdentifier*>(m_DataType));
    default:
        throw std::bad_cast{};
    }
    SILA_CPP_DISABLE_WARNING_POP
}

//============================================================================
CDataTypeList CDataType::list() const
{
    SILA_CPP_DISABLE_WARNING_PUSH
    SILA_CPP_DISABLE_WARNING_NULL_DEREFERENCE
    switch (m_Type)
    {
    case Type::List:
        return *(dynamic_cast<CDataTypeList*>(m_DataType));
    default:
        throw std::bad_cast{};
    }
    SILA_CPP_DISABLE_WARNING_POP
}

//============================================================================
CDataTypeStructure CDataType::structure() const
{
    SILA_CPP_DISABLE_WARNING_PUSH
    SILA_CPP_DISABLE_WARNING_NULL_DEREFERENCE
    switch (m_Type)
    {
    case Type::Structure:
        return *(dynamic_cast<CDataTypeStructure*>(m_DataType));
    default:
        throw std::bad_cast{};
    }
    SILA_CPP_DISABLE_WARNING_POP
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDataType& rhs)
{
    using namespace SiLA2::codegen::fdl;
    QDebugStateSaver s{dbg};
    dbg.nospace() << "CDataType(" << CDataType::typeToString(rhs.type()) << ", ";
    switch (rhs.type())
    {
    case CDataType::Type::Basic:
        dbg << *(dynamic_cast<CDataTypeBasic*>(rhs.dataType()));
        break;
    case CDataType::Type::Constrained:
        dbg << *(dynamic_cast<CDataTypeConstrained*>(rhs.dataType()));
        break;
    case CDataType::Type::Identifier:
        dbg << *(dynamic_cast<CDataTypeIdentifier*>(rhs.dataType()));
        break;
    case CDataType::Type::List:
        dbg << *(dynamic_cast<CDataTypeList*>(rhs.dataType()));
        break;
    case CDataType::Type::Structure:
        dbg << *(dynamic_cast<CDataTypeStructure*>(rhs.dataType()));
        break;
    default:
        qCCritical(sila_cpp_codegen) << "Shouldn't get here";
        break;
    }

    return dbg << ')';
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CDataType& rhs)
{
    using namespace SiLA2::codegen::fdl;
    os << "CDataType(" << CDataType::typeToString(rhs.type()) << ", ";
    switch (rhs.type())
    {
    case CDataType::Type::Basic:
        os << *(dynamic_cast<CDataTypeBasic*>(rhs.dataType()));
        break;
    case CDataType::Type::Constrained:
        os << *(dynamic_cast<CDataTypeConstrained*>(rhs.dataType()));
        break;
    case CDataType::Type::Identifier:
        os << *(dynamic_cast<CDataTypeIdentifier*>(rhs.dataType()));
        break;
    case CDataType::Type::List:
        os << *(dynamic_cast<CDataTypeList*>(rhs.dataType()));
        break;
    case CDataType::Type::Structure:
        os << *(dynamic_cast<CDataTypeStructure*>(rhs.dataType()));
        break;
    default:
        qCCritical(sila_cpp_codegen) << "Shouldn't get here";
        break;
    }

    return os << ')';
}
}  // namespace SiLA2::codegen::fdl
