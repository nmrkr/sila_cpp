/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DataTypeList.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   28.01.2021
/// \brief  Implementation of the CDataTypeList class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/DataTypeList.h>
#include <sila_cpp/common/logging.h>

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CDataTypeList::toVariant() const
{
    return QVariantMap{{"DataType", m_DataType.toVariant()}};
}

//============================================================================
void CDataTypeList::fromVariant(const QVariant& from)
{
    const auto Map = from.toMap();
    if (Q_UNLIKELY(Map.empty()))
    {
        throw std::runtime_error{"Failed to parse Feature Description! <List> "
                                 "data type is empty."};
    }
    m_DataType.fromVariant(Map.value("DataType"));
}

//============================================================================
QDebug operator<<(QDebug dbg, const CDataTypeList& rhs)
{
    return dbg << rhs.dataType();
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CDataTypeList& rhs)
{
    return os << rhs.dataType();
}
}  // namespace SiLA2::codegen::fdl
