/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   Constraint.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   29.01.2021
/// \brief  Implementation of the CConstraint class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/fdl/Constraint.h>
#include <sila_cpp/codegen/fdl/ConstraintAllowedTypes.h>
#include <sila_cpp/codegen/fdl/ConstraintContentType.h>
#include <sila_cpp/codegen/fdl/ConstraintElementCount.h>
#include <sila_cpp/codegen/fdl/ConstraintExclusive.h>
#include <sila_cpp/codegen/fdl/ConstraintFullyQualifiedID.h>
#include <sila_cpp/codegen/fdl/ConstraintInclusive.h>
#include <sila_cpp/codegen/fdl/ConstraintLength.h>
#include <sila_cpp/codegen/fdl/ConstraintPattern.h>
#include <sila_cpp/codegen/fdl/ConstraintSchema.h>
#include <sila_cpp/codegen/fdl/ConstraintSet.h>
#include <sila_cpp/codegen/fdl/ConstraintUnit.h>
#include <sila_cpp/common/logging.h>

namespace SiLA2::codegen::fdl
{
//============================================================================
QVariant CConstraint::toVariant() const
{
    SILA_CPP_DISABLE_WARNING_PUSH
    SILA_CPP_DISABLE_WARNING_NULL_DEREFERENCE
    switch (m_Type)
    {
    case Type::Length:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintLength*>(m_Constraint)->toVariant()}};
    case Type::MinimalLength:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintMinimalLength*>(m_Constraint)->toVariant()}};
    case Type::MaximalLength:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintMaximalLength*>(m_Constraint)->toVariant()}};
    case Type::Set:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintSet*>(m_Constraint)->toVariant()}};
    case Type::Pattern:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintPattern*>(m_Constraint)->toVariant()}};
    case Type::MaximalExclusive:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintMaximalExclusive*>(m_Constraint)
                 ->toVariant()}};
    case Type::MaximalInclusive:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintMaximalInclusive*>(m_Constraint)
                 ->toVariant()}};
    case Type::MinimalExclusive:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintMinimalExclusive*>(m_Constraint)
                 ->toVariant()}};
    case Type::MinimalInclusive:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintMinimalInclusive*>(m_Constraint)
                 ->toVariant()}};
    case Type::Unit:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintUnit*>(m_Constraint)->toVariant()}};
    case Type::ContentType:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintContentType*>(m_Constraint)->toVariant()}};
    case Type::ElementCount:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintElementCount*>(m_Constraint)->toVariant()}};
    case Type::MinimalElementCount:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintMinimalElementCount*>(m_Constraint)
                 ->toVariant()}};
    case Type::MaximalElementCount:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintMaximalElementCount*>(m_Constraint)
                 ->toVariant()}};
    case Type::FullyQualifiedIdentifier:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintFullyQualifiedID*>(m_Constraint)
                 ->toVariant()}};
    case Type::Schema:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintSchema*>(m_Constraint)->toVariant()}};
    case Type::AllowedTypes:
        return QVariantMap{
            {typeToString(m_Type),
             dynamic_cast<CConstraintAllowedTypes*>(m_Constraint)->toVariant()}};
    default:
        qCCritical(sila_cpp_codegen) << "Shouldn't get here!";
        return {};
    }
    SILA_CPP_DISABLE_WARNING_POP
}

//============================================================================
void CConstraint::fromVariant(const QVariant& from)
{
    const auto Map = from.toMap();
    if (Q_UNLIKELY(Map.empty()))
    {
        throw std::runtime_error{"Failed to parse Feature Description! "
                                 "<Constraints> is empty."};
    }
    const auto& Key = Map.firstKey();
    m_Type = stringToType(Key);

    SILA_CPP_DISABLE_WARNING_PUSH
    SILA_CPP_DISABLE_WARNING_NULL_DEREFERENCE
    switch (m_Type)
    {
    case Type::Length:
        m_Constraint = new CConstraintLength;
        dynamic_cast<CConstraintLength*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MinimalLength:
        m_Constraint = new CConstraintMinimalLength;
        dynamic_cast<CConstraintMinimalLength*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MaximalLength:
        m_Constraint = new CConstraintMaximalLength;
        dynamic_cast<CConstraintMaximalLength*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::Set:
        m_Constraint = new CConstraintSet;
        dynamic_cast<CConstraintSet*>(m_Constraint)->fromVariant(Map.value(Key));
        break;
    case Type::Pattern:
        m_Constraint = new CConstraintPattern;
        dynamic_cast<CConstraintPattern*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MaximalExclusive:
        m_Constraint = new CConstraintMaximalExclusive;
        dynamic_cast<CConstraintMaximalExclusive*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MaximalInclusive:
        m_Constraint = new CConstraintMaximalInclusive;
        dynamic_cast<CConstraintMaximalInclusive*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MinimalExclusive:
        m_Constraint = new CConstraintMinimalExclusive;
        dynamic_cast<CConstraintMinimalExclusive*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MinimalInclusive:
        m_Constraint = new CConstraintMinimalInclusive;
        dynamic_cast<CConstraintMinimalInclusive*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::Unit:
        m_Constraint = new CConstraintUnit;
        dynamic_cast<CConstraintUnit*>(m_Constraint)->fromVariant(Map.value(Key));
        break;
    case Type::ContentType:
        m_Constraint = new CConstraintContentType;
        dynamic_cast<CConstraintContentType*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::ElementCount:
        m_Constraint = new CConstraintElementCount;
        dynamic_cast<CConstraintElementCount*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MinimalElementCount:
        m_Constraint = new CConstraintMinimalElementCount;
        dynamic_cast<CConstraintMinimalElementCount*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::MaximalElementCount:
        m_Constraint = new CConstraintMaximalElementCount;
        dynamic_cast<CConstraintMaximalElementCount*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::FullyQualifiedIdentifier:
        m_Constraint = new CConstraintFullyQualifiedID;
        dynamic_cast<CConstraintFullyQualifiedID*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::Schema:
        m_Constraint = new CConstraintSchema;
        dynamic_cast<CConstraintSchema*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    case Type::AllowedTypes:
        m_Constraint = new CConstraintAllowedTypes;
        dynamic_cast<CConstraintAllowedTypes*>(m_Constraint)
            ->fromVariant(Map.value(Key));
        break;
    }
    SILA_CPP_DISABLE_WARNING_POP
}

//============================================================================
CConstraintLength CConstraint::length() const
{
    switch (m_Type)
    {
    case Type::Length:
        return *(dynamic_cast<CConstraintLength*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintMinimalLength CConstraint::minimalLength() const
{
    switch (m_Type)
    {
    case Type::MinimalLength:
        return *(dynamic_cast<CConstraintMinimalLength*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintMaximalLength CConstraint::maximalLength() const
{
    switch (m_Type)
    {
    case Type::MaximalLength:
        return *(dynamic_cast<CConstraintMaximalLength*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintElementCount CConstraint::elementCount() const
{
    switch (m_Type)
    {
    case Type::ElementCount:
        return *(dynamic_cast<CConstraintElementCount*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintMinimalElementCount CConstraint::minimalElementCount() const
{
    switch (m_Type)
    {
    case Type::MinimalElementCount:
        return *(dynamic_cast<CConstraintMinimalElementCount*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintMaximalElementCount CConstraint::maximalElementCount() const
{
    switch (m_Type)
    {
    case Type::MaximalElementCount:
        return *(dynamic_cast<CConstraintMaximalElementCount*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintMinimalExclusive CConstraint::minimalExclusive() const
{
    switch (m_Type)
    {
    case Type::MinimalExclusive:
        return *(dynamic_cast<CConstraintMinimalExclusive*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintMaximalExclusive CConstraint::maximalExclusive() const
{
    switch (m_Type)
    {
    case Type::MaximalExclusive:
        return *(dynamic_cast<CConstraintMaximalExclusive*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintMinimalInclusive CConstraint::minimalInclusive() const
{
    switch (m_Type)
    {
    case Type::MinimalInclusive:
        return *(dynamic_cast<CConstraintMinimalInclusive*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintMaximalInclusive CConstraint::maximalInclusive() const
{
    switch (m_Type)
    {
    case Type::MaximalInclusive:
        return *(dynamic_cast<CConstraintMaximalInclusive*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintFullyQualifiedID CConstraint::fullyQualifiedIdentifier() const
{
    switch (m_Type)
    {
    case Type::FullyQualifiedIdentifier:
        return *(dynamic_cast<CConstraintFullyQualifiedID*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintPattern CConstraint::pattern() const
{
    switch (m_Type)
    {
    case Type::Pattern:
        return *(dynamic_cast<CConstraintPattern*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintSet CConstraint::set() const
{
    switch (m_Type)
    {
    case Type::Set:
        return *(dynamic_cast<CConstraintSet*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintAllowedTypes CConstraint::allowedTypes() const
{
    switch (m_Type)
    {
    case Type::AllowedTypes:
        return *(dynamic_cast<CConstraintAllowedTypes*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintContentType CConstraint::contentType() const
{
    switch (m_Type)
    {
    case Type::ContentType:
        return *(dynamic_cast<CConstraintContentType*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintSchema CConstraint::schema() const
{
    switch (m_Type)
    {
    case Type::Schema:
        return *(dynamic_cast<CConstraintSchema*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
CConstraintUnit CConstraint::unit() const
{
    switch (m_Type)
    {
    case Type::Unit:
        return *(dynamic_cast<CConstraintUnit*>(m_Constraint));
    default:
        return {};
    }
}

//============================================================================
QDebug operator<<(QDebug dbg, const CConstraint& rhs)
{
    using namespace SiLA2::codegen::fdl;
    QDebugStateSaver s{dbg};
    dbg.nospace() << "CConstraint(" << rhs.type() << ", ";

    SILA_CPP_DISABLE_WARNING_PUSH
    SILA_CPP_DISABLE_WARNING_NULL_DEREFERENCE
    switch (rhs.type())
    {
    case CConstraint::Type::Length:
        dbg << *(dynamic_cast<CConstraintLength*>(rhs.constraint()));
        break;
    case CConstraint::Type::MinimalLength:
        dbg << *(dynamic_cast<CConstraintMinimalLength*>(rhs.constraint()));
        break;
    case CConstraint::Type::MaximalLength:
        dbg << *(dynamic_cast<CConstraintMaximalLength*>(rhs.constraint()));
        break;
    case CConstraint::Type::Set:
        dbg << *(dynamic_cast<CConstraintSet*>(rhs.constraint()));
        break;
    case CConstraint::Type::Pattern:
        dbg << *(dynamic_cast<CConstraintPattern*>(rhs.constraint()));
        break;
    case CConstraint::Type::MaximalExclusive:
        dbg << *(dynamic_cast<CConstraintMaximalExclusive*>(rhs.constraint()));
        break;
    case CConstraint::Type::MaximalInclusive:
        dbg << *(dynamic_cast<CConstraintMaximalInclusive*>(rhs.constraint()));
        break;
    case CConstraint::Type::MinimalExclusive:
        dbg << *(dynamic_cast<CConstraintMinimalExclusive*>(rhs.constraint()));
        break;
    case CConstraint::Type::MinimalInclusive:
        dbg << *(dynamic_cast<CConstraintMinimalInclusive*>(rhs.constraint()));
        break;
    case CConstraint::Type::Unit:
        dbg << *(dynamic_cast<CConstraintUnit*>(rhs.constraint()));
        break;
    case CConstraint::Type::ContentType:
        dbg << *(dynamic_cast<CConstraintContentType*>(rhs.constraint()));
        break;
    case CConstraint::Type::ElementCount:
        dbg << *(dynamic_cast<CConstraintElementCount*>(rhs.constraint()));
        break;
    case CConstraint::Type::MinimalElementCount:
        dbg << *(dynamic_cast<CConstraintMinimalElementCount*>(rhs.constraint()));
        break;
    case CConstraint::Type::MaximalElementCount:
        dbg << *(dynamic_cast<CConstraintMaximalElementCount*>(rhs.constraint()));
        break;
    case CConstraint::Type::FullyQualifiedIdentifier:
        dbg << *(dynamic_cast<CConstraintFullyQualifiedID*>(rhs.constraint()));
        break;
    case CConstraint::Type::Schema:
        dbg << *(dynamic_cast<CConstraintSchema*>(rhs.constraint()));
        break;
    case CConstraint::Type::AllowedTypes:
        dbg << *(dynamic_cast<CConstraintAllowedTypes*>(rhs.constraint()));
        break;
    }
    SILA_CPP_DISABLE_WARNING_POP

    return dbg << ')';
}

//============================================================================
std::ostream& operator<<(std::ostream& os, const CConstraint& rhs)
{
    using namespace SiLA2::codegen::fdl;
    os << "CConstraint(" << rhs.type() << ", ";

    SILA_CPP_DISABLE_WARNING_PUSH
    SILA_CPP_DISABLE_WARNING_NULL_DEREFERENCE
    switch (rhs.type())
    {
    case CConstraint::Type::Length:
        os << *(dynamic_cast<CConstraintLength*>(rhs.constraint()));
        break;
    case CConstraint::Type::MinimalLength:
        os << *(dynamic_cast<CConstraintMinimalLength*>(rhs.constraint()));
        break;
    case CConstraint::Type::MaximalLength:
        os << *(dynamic_cast<CConstraintMaximalLength*>(rhs.constraint()));
        break;
    case CConstraint::Type::Set:
        os << *(dynamic_cast<CConstraintSet*>(rhs.constraint()));
        break;
    case CConstraint::Type::Pattern:
        os << *(dynamic_cast<CConstraintPattern*>(rhs.constraint()));
        break;
    case CConstraint::Type::MaximalExclusive:
        os << *(dynamic_cast<CConstraintMaximalExclusive*>(rhs.constraint()));
        break;
    case CConstraint::Type::MaximalInclusive:
        os << *(dynamic_cast<CConstraintMaximalInclusive*>(rhs.constraint()));
        break;
    case CConstraint::Type::MinimalExclusive:
        os << *(dynamic_cast<CConstraintMinimalExclusive*>(rhs.constraint()));
        break;
    case CConstraint::Type::MinimalInclusive:
        os << *(dynamic_cast<CConstraintMinimalInclusive*>(rhs.constraint()));
        break;
    case CConstraint::Type::Unit:
        os << *(dynamic_cast<CConstraintUnit*>(rhs.constraint()));
        break;
    case CConstraint::Type::ContentType:
        os << *(dynamic_cast<CConstraintContentType*>(rhs.constraint()));
        break;
    case CConstraint::Type::ElementCount:
        os << *(dynamic_cast<CConstraintElementCount*>(rhs.constraint()));
        break;
    case CConstraint::Type::MinimalElementCount:
        os << *(dynamic_cast<CConstraintMinimalElementCount*>(rhs.constraint()));
        break;
    case CConstraint::Type::MaximalElementCount:
        os << *(dynamic_cast<CConstraintMaximalElementCount*>(rhs.constraint()));
        break;
    case CConstraint::Type::FullyQualifiedIdentifier:
        os << *(dynamic_cast<CConstraintFullyQualifiedID*>(rhs.constraint()));
        break;
    case CConstraint::Type::Schema:
        os << *(dynamic_cast<CConstraintSchema*>(rhs.constraint()));
        break;
    case CConstraint::Type::AllowedTypes:
        os << *(dynamic_cast<CConstraintAllowedTypes*>(rhs.constraint()));
        break;
    }
    SILA_CPP_DISABLE_WARNING_POP

    return os << ')';
}
}  // namespace SiLA2::codegen::fdl
