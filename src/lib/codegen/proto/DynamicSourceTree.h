/**
 ** This file is part of the sila_cpp project.
 ** Copyright 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   DynamicSourceTree.h
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   13.01.2021
/// \brief  Declaration of the CDynamicSourceTree and CFile classes
//============================================================================
#ifndef CODEGEN_DYNAMICSOURCETREE_H
#define CODEGEN_DYNAMICSOURCETREE_H

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/codegen/proto/ProtobufFile.h>

#include <google/protobuf/compiler/importer.h>

#include <string>
#include <vector>

namespace SiLA2::codegen::proto
{
/**
 * @brief The CDynamicSourceTree class represents a SourceTree of files that are
 * created dynamically during runtime and that don't reside anywhere persistently
 */
class CDynamicSourceTree : public google::protobuf::compiler::SourceTree
{
public:
    /**
     * @brief Construct a new, empty @c CDynamicSourceTree
     */
    CDynamicSourceTree();

    /**
     * @brief Construct a new @c CDynamicSourceTree with the given file @a File
     *
     * @param File A protobuf file
     */
    explicit CDynamicSourceTree(const CProtobufFile& File);

    /**
     * @brief Add the given file @a File to the Source Tree
     *
     * @param File The file to add
     */
    void addFile(const CProtobufFile& File);

    /**
     * @brief Get all of the files currently in the Source Tree
     *
     * @return The Source Tree's files
     */
    [[nodiscard]] std::vector<CProtobufFile> files() const { return m_Files; }

    /**
     * @override
     * @brief Open the file given by @a Filename
     *
     * @param Filename The name of the file to open
     * @return A stream that reads the file or @c nullptr if the file couldn't be
     * opened
     */
    google::protobuf::io::ZeroCopyInputStream* Open(
        const std::string& Filename) override;

    /**
     * @override
     * @brief If @a Open() returns @c nullptr calling this method will return the
     * error that occurred
     *
     * @return The last error that occurred during @a Open()
     */
    std::string GetLastErrorMessage() override { return m_LastError; }

private:
    std::vector<CProtobufFile> m_Files{};
    std::string m_LastError;
};
}  // namespace SiLA2::codegen::proto

#endif  // CODEGEN_DYNAMICSOURCETREE_H
