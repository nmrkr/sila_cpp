/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FullyQualifiedCommandID.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   22.01.2021
/// \brief  Implementation of the CFullyQualifiedCommandID class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FullyQualifiedCommandID.h>
#include <sila_cpp/common/logging.h>

#include <QStringList>

using namespace std;

namespace SiLA2
{
//============================================================================
CFullyQualifiedCommandID::CFullyQualifiedCommandID(
    const CFullyQualifiedFeatureID& FeatureID, string_view Identifier)
    : CFullyQualifiedFeatureID{FeatureID}, m_Identifier{Identifier.data()}
{}

//============================================================================
CFullyQualifiedCommandID::CFullyQualifiedCommandID(
    const CFullyQualifiedFeatureID& FeatureID, QStringView Identifier)
    : CFullyQualifiedFeatureID{FeatureID}, m_Identifier{Identifier.toString()}
{}

//============================================================================
CFullyQualifiedCommandID CFullyQualifiedCommandID::fromString(const QString& from)
{
    return fromStdString(from.toStdString());
}

//============================================================================
CFullyQualifiedCommandID CFullyQualifiedCommandID::fromStdString(
    const string& from)
{
    auto List = QString::fromStdString(from).split('/');
    if (List.size() != 6 || List.at(4).toLower() != "command")
    {
        qCWarning(sila_cpp_common).nospace()
            << "The given string \"" << from
            << "\" is not in the correct format of a "
               "Fully Qualified Command Identifier";
        return {};
    }
    return {{List.at(0), List.at(1), List.at(2), List.at(3)}, List.at(5)};
}

//============================================================================
bool CFullyQualifiedCommandID::operator==(
    const CFullyQualifiedCommandID& rhs) const
{
    return featureIdentifier() == rhs.featureIdentifier()
           && m_Identifier.toLower() == rhs.m_Identifier.toLower();
}

//============================================================================
CFullyQualifiedCommandID CFullyQualifiedCommandID::commandIdentifier() const
{
    return *this;
}

//============================================================================
QString CFullyQualifiedCommandID::identifier() const
{
    return m_Identifier;
}

//============================================================================
bool CFullyQualifiedCommandID::isValid() const
{
    return CFullyQualifiedFeatureID::isValid() && !m_Identifier.isEmpty();
}

//============================================================================
QString CFullyQualifiedCommandID::toString() const
{
    return CFullyQualifiedFeatureID::toString() + "/Command/" + m_Identifier;
}

//============================================================================
string CFullyQualifiedCommandID::toStdString() const
{
    return toString().toStdString();
}

//============================================================================
string CFullyQualifiedCommandID::toMethodName(RPCType Type) const
{
    return QStringList{
        "", QString::fromStdString(CFullyQualifiedFeatureID::toServiceName()),
        m_Identifier + typeToString(Type)}
        .join('/')
        .toStdString();
}

//============================================================================
string CFullyQualifiedCommandID::parameterMessageName() const
{
    return toFullyQualifiedMessageName(identifier() + "_Parameters");
}

//============================================================================
string CFullyQualifiedCommandID::responseMessageName(RPCType Type) const
{
    switch (Type)
    {
    case RPCType::INITIATION:
        return "sila2.org.silastandard.CommandConfirmation";
    case RPCType::INFO:
        return "sila2.org.silastandard.ExecutionInfo";
    case RPCType::INTERMEDIATE_RESULT:
        return toFullyQualifiedMessageName(identifier()
                                           + "_IntermediateResponses");
    case RPCType::INVALID:
    case RPCType::RESULT:
        return toFullyQualifiedMessageName(identifier() + "_Responses");
    default:
        qCCritical(sila_cpp_common)
            << "Unknown RPCType" << static_cast<int>(Type);
        return "";
    }
}

//============================================================================
CFullyQualifiedCommandID::CFullyQualifiedCommandID() = default;

//============================================================================
QString CFullyQualifiedCommandID::typeToString(RPCType Type)
{
    switch (Type)
    {
    case RPCType::INVALID:
    case RPCType::INITIATION:
        return "";
    case RPCType::INFO:
        return "_Info";
    case RPCType::INTERMEDIATE_RESULT:
        return "_IntermediateResult";
    case RPCType::RESULT:
        return "_Result";
    default:
        qCCritical(sila_cpp_common)
            << "Unknown RPCType" << static_cast<int>(Type);
        return "";
    }
}

///===========================================================================
CFullyQualifiedCommandParameterID::CFullyQualifiedCommandParameterID(
    const CFullyQualifiedCommandID& CommandID, QStringView Identifier)
    : CFullyQualifiedCommandID{CommandID}, m_Identifier{Identifier.toString()}
{}

//============================================================================
CFullyQualifiedCommandParameterID::CFullyQualifiedCommandParameterID(
    const CFullyQualifiedCommandID& CommandID, string_view Identifier)
    : CFullyQualifiedCommandID{CommandID}, m_Identifier{Identifier.data()}
{}

//============================================================================
CFullyQualifiedCommandParameterID
CFullyQualifiedCommandParameterID::fromStdString(const string& from)
{
    auto List = QString::fromStdString(from).split('/');
    if (List.size() != 8 || List.at(6).toLower() != "parameter")
    {
        qCWarning(sila_cpp_common).nospace()
            << "The given string \"" << from
            << "\" is not in the correct format of a "
               "Fully Qualified Command Parameter Identifier";
        return {};
    }
    return {{{List.at(0), List.at(1), List.at(2), List.at(3)}, List.at(5)},
            List.at(7)};
}

//============================================================================
QString CFullyQualifiedCommandParameterID::toString() const
{
    return CFullyQualifiedCommandID::toString() + "/Parameter/" + m_Identifier;
}

//============================================================================
CFullyQualifiedCommandParameterID::CFullyQualifiedCommandParameterID() = default;

//============================================================================
CFullyQualifiedCommandParameterID CFullyQualifiedCommandParameterID::fromString(
    const QString& from)
{
    return fromStdString(from.toStdString());
}

//============================================================================
bool CFullyQualifiedCommandParameterID::operator==(
    const CFullyQualifiedCommandParameterID& rhs) const
{
    return featureIdentifier() == rhs.featureIdentifier()
           && m_Identifier.toLower() == rhs.m_Identifier.toLower();
}

//============================================================================
CFullyQualifiedCommandParameterID
CFullyQualifiedCommandParameterID::parameterIdentifier() const
{
    return *this;
}

//============================================================================
QString CFullyQualifiedCommandParameterID::identifier() const
{
    return m_Identifier;
}

//============================================================================
bool CFullyQualifiedCommandParameterID::isValid() const
{
    return CFullyQualifiedCommandID::isValid() && !m_Identifier.isEmpty();
}

//============================================================================
string CFullyQualifiedCommandParameterID::toStdString() const
{
    return toString().toStdString();
}

///===========================================================================
CFullyQualifiedCommandResponseID::CFullyQualifiedCommandResponseID(
    const CFullyQualifiedCommandID& CommandID, QStringView Identifier)
    : CFullyQualifiedCommandID{CommandID}, m_Identifier{Identifier.toString()}
{}

//============================================================================
CFullyQualifiedCommandResponseID::CFullyQualifiedCommandResponseID(
    const CFullyQualifiedCommandID& CommandID, string_view Identifier)
    : CFullyQualifiedCommandID{CommandID}, m_Identifier{Identifier.data()}
{}

//============================================================================
CFullyQualifiedCommandResponseID CFullyQualifiedCommandResponseID::fromStdString(
    const string& from)
{
    auto List = QString::fromStdString(from).split('/');
    if (List.size() != 8 || List.at(6).toLower() != "response")
    {
        qCWarning(sila_cpp_common).nospace()
            << "The given string \"" << from
            << "\" is not in the correct format of a "
               "Fully Qualified Command Response Identifier";
        return {};
    }
    return {{{List.at(0), List.at(1), List.at(2), List.at(3)}, List.at(5)},
            List.at(7)};
}

//============================================================================
QString CFullyQualifiedCommandResponseID::toString() const
{
    return CFullyQualifiedCommandID::toString() + "/Response/" + m_Identifier;
}

//============================================================================
CFullyQualifiedCommandResponseID::CFullyQualifiedCommandResponseID() = default;

//============================================================================
CFullyQualifiedCommandResponseID CFullyQualifiedCommandResponseID::fromString(
    const QString& from)
{
    return fromStdString(from.toStdString());
}

//============================================================================
bool CFullyQualifiedCommandResponseID::operator==(
    const CFullyQualifiedCommandResponseID& rhs) const
{
    return featureIdentifier() == rhs.featureIdentifier()
           && m_Identifier.toLower() == rhs.m_Identifier.toLower();
}

//============================================================================
CFullyQualifiedCommandResponseID
CFullyQualifiedCommandResponseID::responseIdentifier() const
{
    return *this;
}

//============================================================================
QString CFullyQualifiedCommandResponseID::identifier() const
{
    return m_Identifier;
}

//============================================================================
bool CFullyQualifiedCommandResponseID::isValid() const
{
    return CFullyQualifiedCommandID::isValid() && !m_Identifier.isEmpty();
}

//============================================================================
string CFullyQualifiedCommandResponseID::toStdString() const
{
    return toString().toStdString();
}

///===========================================================================
CFullyQualifiedCommandIntermediateResponseID::
    CFullyQualifiedCommandIntermediateResponseID(
        const CFullyQualifiedCommandID& CommandID, QStringView Identifier)
    : CFullyQualifiedCommandID{CommandID}, m_Identifier{Identifier.toString()}
{}

//============================================================================
CFullyQualifiedCommandIntermediateResponseID::
    CFullyQualifiedCommandIntermediateResponseID(
        const CFullyQualifiedCommandID& CommandID, string_view Identifier)
    : CFullyQualifiedCommandID{CommandID}, m_Identifier{Identifier.data()}
{}

//============================================================================
CFullyQualifiedCommandIntermediateResponseID
CFullyQualifiedCommandIntermediateResponseID::fromStdString(const string& from)
{
    auto List = QString::fromStdString(from).split('/');
    if (List.size() != 8 || List.at(6).toLower() != "intermediateresponse")
    {
        qCWarning(sila_cpp_common).nospace()
            << "The given string \"" << from
            << "\" is not in the correct format of a "
               "Fully Qualified Command Intermediate Response Identifier";
        return {};
    }
    return {{{List.at(0), List.at(1), List.at(2), List.at(3)}, List.at(5)},
            List.at(7)};
}

//============================================================================
QString CFullyQualifiedCommandIntermediateResponseID::toString() const
{
    return CFullyQualifiedCommandID::toString() + "/IntermediateResponse/"
           + m_Identifier;
}

//============================================================================
CFullyQualifiedCommandIntermediateResponseID::
    CFullyQualifiedCommandIntermediateResponseID() = default;

//============================================================================
CFullyQualifiedCommandIntermediateResponseID
CFullyQualifiedCommandIntermediateResponseID::fromString(const QString& from)
{
    return fromStdString(from.toStdString());
}

//============================================================================
bool CFullyQualifiedCommandIntermediateResponseID::operator==(
    const CFullyQualifiedCommandIntermediateResponseID& rhs) const
{
    return featureIdentifier() == rhs.featureIdentifier()
           && m_Identifier.toLower() == rhs.m_Identifier.toLower();
}

//============================================================================
CFullyQualifiedCommandIntermediateResponseID
CFullyQualifiedCommandIntermediateResponseID::intermediateResponseIdentifier()
    const
{
    return *this;
}

//============================================================================
QString CFullyQualifiedCommandIntermediateResponseID::identifier() const
{
    return m_Identifier;
}

//============================================================================
bool CFullyQualifiedCommandIntermediateResponseID::isValid() const
{
    return CFullyQualifiedCommandID::isValid() && !m_Identifier.isEmpty();
}

//============================================================================
string CFullyQualifiedCommandIntermediateResponseID::toStdString() const
{
    return toString().toStdString();
}
}  // namespace SiLA2
