/**
 ** This file is part of the sila_cpp project.
 ** Copyright (c) 2021 SiLA2
 **
 ** Permission is hereby granted, free of charge, to any person obtaining a copy
 ** of this software and associated documentation files (the "Software"), to deal
 ** in the Software without restriction, including without limitation the rights
 ** to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 ** copies of the Software, and to permit persons to whom the Software is
 ** furnished to do so, subject to the following conditions:
 **
 ** The above copyright notice and this permission notice shall be included in all
 ** copies or substantial portions of the Software.
 **
 ** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 ** IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 ** FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 ** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 ** LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 ** OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 ** SOFTWARE.
 **/

//============================================================================
/// \file   FullyQualifiedPropertyID.cpp
/// \author Florian Meinicke (florian.meinicke@cetoni.de)
/// \date   26.01.2021
/// \brief  Implementation of the CFullyQualifiedPropertyID class
//============================================================================

//============================================================================
//                                  INCLUDES
//============================================================================
#include <sila_cpp/common/FullyQualifiedPropertyID.h>
#include <sila_cpp/common/logging.h>

using namespace std;

namespace SiLA2
{
//============================================================================
CFullyQualifiedPropertyID::CFullyQualifiedPropertyID(
    const CFullyQualifiedFeatureID& FeatureID, string_view Identifier)
    : CFullyQualifiedFeatureID{FeatureID}, m_Identifier{Identifier.data()}
{}

//============================================================================
CFullyQualifiedPropertyID::CFullyQualifiedPropertyID(
    const CFullyQualifiedFeatureID& FeatureID, QStringView Identifier)
    : CFullyQualifiedFeatureID{FeatureID}, m_Identifier{Identifier.toString()}
{}

//============================================================================
CFullyQualifiedPropertyID CFullyQualifiedPropertyID::fromString(
    const QString& from)
{
    return fromStdString(from.toStdString());
}

//============================================================================
CFullyQualifiedPropertyID CFullyQualifiedPropertyID::fromStdString(
    const string& from)
{
    auto List = QString::fromStdString(from).split('/');
    if (List.size() != 6 || List.at(4).toLower() != "property")
    {
        qCWarning(sila_cpp_common).nospace()
            << "The given string \"" << from
            << "\" is not in the correct format of a "
               "Fully Qualified Property Identifier";
        return CFullyQualifiedPropertyID{};
    }
    return CFullyQualifiedPropertyID{
        {List.at(0), List.at(1), List.at(2), List.at(3)}, List.at(5)};
}

//============================================================================
bool CFullyQualifiedPropertyID::operator==(
    const CFullyQualifiedPropertyID& rhs) const
{
    return featureIdentifier() == rhs.featureIdentifier()
           && m_Identifier.toLower() == rhs.m_Identifier.toLower();
}

//============================================================================
QString CFullyQualifiedPropertyID::identifier() const
{
    return m_Identifier;
}

//============================================================================
bool CFullyQualifiedPropertyID::isValid() const
{
    return CFullyQualifiedFeatureID::isValid() && !m_Identifier.isEmpty();
}

//============================================================================
QString CFullyQualifiedPropertyID::toString() const
{
    return CFullyQualifiedFeatureID::toString() + "/Property/" + m_Identifier;
}

//============================================================================
string CFullyQualifiedPropertyID::toStdString() const
{
    return toString().toStdString();
}

//============================================================================
string CFullyQualifiedPropertyID::toMethodName(bool Observable) const
{
    return QStringList{
        "", QString::fromStdString(CFullyQualifiedFeatureID::toServiceName()),
        (Observable ? "Subscribe_" : "Get_") + m_Identifier}
        .join('/')
        .toStdString();
}

//============================================================================
string CFullyQualifiedPropertyID::parameterMessageName(bool Observable) const
{
    return toFullyQualifiedMessageName((Observable ? "Subscribe_" : "Get_")
                                       + identifier() + "_Parameters");
}

//============================================================================
string CFullyQualifiedPropertyID::responseMessageName(bool Observable) const
{
    return toFullyQualifiedMessageName((Observable ? "Subscribe_" : "Get_")
                                       + identifier() + "_Responses");
}

//============================================================================
CFullyQualifiedPropertyID::CFullyQualifiedPropertyID() = default;
}  // namespace SiLA2
