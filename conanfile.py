from conans import ConanFile, CMake, tools
from conans.errors import ConanException
import os

class SilacppConan(ConanFile):
    name = "sila_cpp"
    description = "Official SiLA 2 C++ reference implementation"
    topics = ("SiLA 2", "Lab Automation", "Development")
    author = "Florian Meinicke <florian.meinicke@cetoni.de>"
    url = "https://gitlab.com/SiLA2/sila_cpp"
    license = "MIT"
    exports_sources = "src/*", "third_party/*", "cmake/*", "CMakeLists.txt", "LICENSE"
    generators = "cmake", "cmake_find_package", "cmake_find_package_multi"

    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "with_catch2": [True, False],
        "with_grpc": [True, False],
        "with_protobuf": [True, False],
        "with_qt": [True, False],
        "with_xercesc": [True, False],
        "qt_install_prefix_path": "ANY"
    }
    default_options = {
        "shared": False,
        "with_catch2": True,
        "with_grpc": True,
        "with_protobuf": True,
        "with_qt": True,
        "with_xercesc": True,
        "qt_install_prefix_path": None
    }

    _cmake = None
    _qt_install_prefix_path = ""

    def set_version(self):
        git = tools.Git(folder=self.recipe_folder)
        self.version = git.run("tag -l").split('v')[-1]

    def requirements(self):
        self.requires("polymorphic_value/1.3.0")

        if self.options.with_catch2:
            self.requires("catch2/2.13.6")
        if self.options.with_grpc:
            self.requires("grpc/1.37.1")
        if self.options.with_protobuf:
            # 3.17.1 and 3.15.5 fail with 'error: temporary of non-literal type' in multiple places
            self.requires("protobuf/3.13.0", override=True)
        if self.options.with_qt:
            self.requires("qt/5.15.2")
            # self.requires("openssl/1.1.1k", override=True) # override with openssl version from gRPC
        if self.options.with_xercesc:
            self.requires("xerces-c/3.2.3")

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC
        if self.options.with_qt:
            self.options["qt"].gui = False
            self.options["qt"].widgets = False
        else:
            if not self.options.qt_install_prefix_path:
                self.output.warn(
                    "`qt_install_prefix_path` is not set and `with_qt` is set to false. "
                    "Expecting to find a Qt installation in PATH! If CMake fails to find "
                    "Qt you need to set `qt_install_prefix_path` to point to your Qt "
                    "installation directory or enable conan to install Qt for you by "
                    "setting the `with_qt` option to `True`!"
                )
            else:
                self._qt_install_prefix_path = str(self.options.qt_install_prefix_path)

        if self.options.with_xercesc:
            self.options["xerces-c"].network_accessor = "winsock" if self.settings.os == "Windows" else "curl"

    def _configure_cmake(self):
        if self._cmake is not None:
            return self._cmake

        self._cmake = CMake(self)
        self._cmake.definitions["CMAKE_PREFIX_PATH"] = self._qt_install_prefix_path
        self._cmake.definitions["SILA_CPP_BUILD_EXAMPLES"] = "OFF"
        self._cmake.definitions["VERSION_SHORT"] = self.version
        self._cmake.definitions["CMAKE_VERBOSE_MAKEFILE"] = "ON"
        self._cmake.configure()
        return self._cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        bindir = os.path.join(self.package_folder, "bin")
        self.output.info("Appending PATH environment variable: {}".format(bindir))
        self.env_info.PATH.append(bindir)

        self.cpp_info.names["cmake_find_package"] = "sila_cpp"
        self.cpp_info.names["cmake_find_package_multi"] = "sila_cpp"

        self.cpp_info.libs = ["sila_cpp"]
        if self.settings.os == "Windows":
            self.cpp_info.defines += ["_WIN32_WINNT=0x600"] # required by gRPC
        if not self.options.with_qt:
            self.cpp_info.cxxflags += ["-L{}".format(os.path.join(self._qt_install_prefix_path, "lib"))]
            self.cpp_info.cxxflags += ["-I{}".format(os.path.join(self._qt_install_prefix_path, "include"))]
            self.output.info("Setting CMAKE_PREFIX_PATH to Qt's install prefix path")
            self.env_info.CMAKE_PREFIX_PATH = self._qt_install_prefix_path
            qt_bindir = os.path.join(self._qt_install_prefix_path, "bin")
            self.output.info("Appending PATH environment variable with Qt's install prefix path: {}".format(qt_bindir))
            self.env_info.PATH.append(qt_bindir)
