cmake_minimum_required(VERSION 3.13)

project(HelloSiLA2Metadata LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Find sila_cpp library
if(NOT TARGET sila_cpp)
    # if we're building standalone
    find_package(sila_cpp CONFIG REQUIRED)
else()
    # if we're building as subproject
    include("${SILA_CPP_SOURCE_DIR}/cmake/sila_cppConfig.cmake")
    set(PROTOBUF_IMPORT_DIRS "${SILA_CPP_SOURCE_DIR}/src/lib/sila_base/protobuf")
endif()

add_library(${PROJECT_NAME}Features)
set_target_properties(${PROJECT_NAME}Features PROPERTIES DEBUG_POSTFIX "d")

set(PROTOS
    ${CMAKE_CURRENT_SOURCE_DIR}/meta/GreetingProvider.proto
    ${CMAKE_CURRENT_SOURCE_DIR}/meta/TemperatureController.proto
)

target_protoc_generate_cpp(${PROJECT_NAME}Features ${PROTOS})

target_link_libraries(${PROJECT_NAME}Features
    sila2::sila_cpp
)

foreach(_target
    ${PROJECT_NAME}Client ${PROJECT_NAME}Server)
    add_executable(${_target} "${_target}.cpp")
    target_link_libraries(${_target}
        ${PROJECT_NAME}Features
    )
    # copy the libs to the directory of the executables so that they can find them
    add_custom_command(TARGET ${_target} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
        $<TARGET_FILE:sila2::sila_cpp> $<TARGET_FILE:sila2::sila_cpp_codegen>
        $<TARGET_FILE:QtZeroConf> $<TARGET_FILE_DIR:${_target}>
    )
endforeach()

# The server needs the FDL files which are compiled into the binary using the resource file
target_sources(${PROJECT_NAME}Server PRIVATE ${PROJECT_NAME}.qrc)
